<?php
namespace App\Model;

use App\Lib\Database;
use App\Lib\Response;

class ScriptModel extends BaseModel
{
    protected $nombre;
    protected $codigo;
    protected $usuario_id;
    
    public function __CONSTRUCT()
    {
        $this->setTable('script');
        $this->setForArray(array('id', 'nombre', 'codigo', 'usuario_id'));
        parent::__CONSTRUCT();
    }


    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }


    public function getCodigo(){
        return $this->codigo;
    }

    public function setCodigo($codigo){
        $this->codigo = $codigo;
        return $this;
    }


    public function getUsuario_id(){
        return $this->usuario_id;
    }

    public function setUsuario_id($usuario_id){
        $this->usuario_id = $usuario_id;
        return $this;
    }

    public function insertOrUpdate()
    {
		try 
		{
            if($this->getId())
            {
                $sql = "UPDATE ".$this->getTable()." SET 
                            nombre          = ?,
                            codigo          = ?
                        WHERE id = ?";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->getNombre(),
                            $this->getCodigo(),
                            $this->getId()
                        )
                    );
            }
            else
            {
                $sql = "INSERT INTO ".$this->getTable()."
                            (nombre, codigo, usuario_id)
                            VALUES (?,?,?)";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->getNombre(),
                            $this->getCodigo(),
                            $this->getUsuario_id(),
                        )
                    );
                $this->setId($this->getDb()->lastInsertId()); 
            }
            
			
            return $this;
		}catch (Exception $e) 
		{
            return null;
		}
    }
    
    
}