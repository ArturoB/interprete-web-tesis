<?php
namespace App\Model;

use App\Lib\Database;
use App\Lib\Response;

class UserAulaModel extends BaseModel
{
    protected $usuario_id;
    protected $aula_id;
    //Nombre del usuario
    protected $alias;
    protected $cedula;
    
    public function __CONSTRUCT()
    {
        $this->setTable('usuario_aula');
        $this->setForArray(array('id', 'usuario_id', 'alias', 'aula_id', 'cedula'));
        parent::__CONSTRUCT();
        $this->borrado = true;
    }

    public function getUsuario_id(){
        return $this->usuario_id;
    }

    public function setUsuario_id($usuario_id){
        $this->usuario_id = $usuario_id;
        return $this;
    }

    public function getAula_id(){
        return $this->aula_id;
    }

    public function setAula_id($aula_id){
        $this->aula_id = $aula_id;
        return $this;
    }

    public function getAlias(){
        return $this->alias;
    }

    public function setAlias($alias){
        $this->alias = $alias;
        return $this;
    }

    public function getCedula(){
        return $this->cedula;
    }

    public function setCedula($cedula){
        $this->cedula = $cedula;
        return $this;
    }

    public function setBorrado($borrado){
        $this->borrado = $borrado;
        return $this;
    }

    public function get($id)
    {

        try
        {   $anexo = '';
            if($this->borrado)
                $anexo = ' AND useraula.borrado = 0 ';
            $result = array();

            $stm = $this->getDb()->prepare("SELECT useraula.id as id,
                                        useraula.usuario_id as usuario_id,
                                        useraula.aula_id as aula_id,
                                        user.alias as alias,
                                        user.cedula as cedula
                                         FROM ".$this->getTable()." useraula, usuario user 
                                         WHERE useraula.id = ? 
                                         AND user.id = useraula.usuario_id ".$anexo."limit 1");
            $stm->execute(array($id));

            
            $array = $stm->fetch();
        }
        catch(Exception $e)
        {

            $array = null;
        }

        //$array = parent::get($id);
        $name = ''.static::class;
        if($array){
            $rol = new $name();
            $rol->fromObject($array);
            return $rol;
            
        }

        return null;
    }

    /**
    $where: condición de la consulta
    $values: valores para la condición
    */   
    public function getAll($where = ' 1', $values = array())
    {
        $where1 = '';
        if($where !== ' 1'){
            $where1 = 'AND '.$where;
        }
        try
        {   $anexo = '';
            if($this->borrado)
                $anexo = ' AND useraula.borrado = 0 ';
            $result = array();

            $stm = $this->getDb()->prepare("SELECT useraula.id as id,
                                        useraula.usuario_id as usuario_id,
                                        useraula.aula_id as aula_id,
                                        user.alias as alias,
                                        user.cedula as cedula
                                         FROM ".$this->getTable()." useraula, usuario user 
                                         WHERE user.id = useraula.usuario_id ".$where1.$anexo);
            $stm->execute($values);
            
            $array = $stm->fetchAll();
        }
        catch(Exception $e)
        {
            $array = null;
        }

        //$array = parent::getAll($where = '1', $values = array());
        $name = ''.static::class;
        $all = array();
        if($array){
            foreach ($array as $value) {
                $rol = new $name;
                $rol->fromObject($value);
                $all[] = $rol;
            }
            return $all;

            
        }

        return null;
    }

    public function insertOrUpdate()
    {
        try 
        {
            if($this->getId())
            {
                $sql = "UPDATE ".$this->getTable()." SET 
                            aula_id          = ?,
                            usuario_id          = ?,
                        WHERE id = ?";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->aula_id,
                            $this->usuario_id,
                            $this->getId()
                        )
                    );
            }
            else
            {
                $sql = "INSERT INTO ".$this->getTable()."
                            (aula_id, usuario_id)
                            VALUES (?,?)";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->aula_id,
                            $this->usuario_id,
                        )
                    );
                $this->setId($this->getDb()->lastInsertId()); 
            }
            
            
            return $this;
        }catch (Exception $e) 
        {
            return null;
        }
    }

    public function delete()
    {

        $entrega = new EntregaModel();
        $entrega = $entrega->getAll('usuario_aula_id = ?', array($this->getId()));
        if(count($entrega) <= 0){
            return parent::delete();
        }
        try 
        {
            $sql = "UPDATE ".$this->getTable()." SET 
                            borrado          = 1,
                            fecha_borrado          = now()
                        WHERE id = ?";
            $stm = $this->getDb()
                        ->prepare($sql);                   

            $stm->execute(array($this->getId()));
            
            return true;
        } catch (Exception $e) 
        {
            return false;
        }
    }

    public function reCreate()
    {
        try 
        {
            $sql = "UPDATE ".$this->getTable()." SET 
                            borrado          = 0,
                            fecha_borrado          = null
                        WHERE id = ?";
            $stm = $this->getDb()
                        ->prepare($sql);                   

            $stm->execute(array($this->getId()));
            
            return true;
        } catch (Exception $e) 
        {
            return false;
        }
    }
}