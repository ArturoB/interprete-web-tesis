<?php
namespace App\Model;

use App\Lib\Database;
use App\Lib\Response;

class ParametroModel extends BaseModel
{
    protected $nombre;
    protected $descripcion;
    protected $valor;
    protected $tipo_valor;
    
    public function __CONSTRUCT()
    {
        $this->setTable('parametro');
        $this->setForArray(array('id', 'nombre', 'descripcion', 'valor', 'tipo_valor'));
        parent::__CONSTRUCT();
    }


    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }


    public function getDescripcion(){
        return $this->descripcion;
    }

    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
        return $this;
    }


    public function getValor(){
        return $this->valor;
    }

    public function setValor($valor){
        $this->valor = $valor;
        return $this;
    }


    public function getTipo_valor(){
        return $this->tipo_valor;
    }

    public function setTipo_valor($tipo_valor){
        $this->tipo_valor = $tipo_valor;
        return $this;
    }

    public function insertOrUpdate()
    {
		try 
		{
            if($this->getId())
            {
                $sql = "UPDATE ".$this->getTable()." SET 
                            nombre          = ?,
                            descripcion          = ?,
                            valor          = ?,
                            tipo_valor          = ?
                        WHERE id = ?";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->getNombre(),
                            $this->descripcion,
                            $this->valor,
                            $this->tipo_valor,
                            $this->getId()
                        )
                    );
            }
            else
            {
                $sql = "INSERT INTO ".$this->getTable()."
                            (nombre, descripcion, valor, tipo_valor)
                            VALUES (?,?,?,?)";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->getNombre(),
                            $this->descripcion,
                            $this->valor,
                            $this->tipo_valor,
                        )
                    );
                $this->setId($this->getDb()->lastInsertId()); 
            }
            
			
            return $this;
		}catch (Exception $e) 
		{
            return null;
		}
    }
    
    
}