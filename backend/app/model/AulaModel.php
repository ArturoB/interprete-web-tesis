<?php
namespace App\Model;

use App\Lib\Database;
use App\Lib\Response;

class AulaModel extends BaseModel
{
    protected $usuario_id;
    protected $nombre;
    protected $codigo;
    protected $semestre_id;
    protected $estudiantesObj;
    //El que llama esta clase en caso de ser un user estudiante
    protected $estudianteObj;
    protected $semestreObj;
    protected $owner;
    
    public function __CONSTRUCT()
    {
        $this->setTable('aula');
        $this->setForArray(array('id', 'usuario_id', 'nombre', 'semestre_id', 'codigo'));
        $this->owner = true;
        parent::__CONSTRUCT();
        $parametro = new ParametroModel();
        $parametro = $parametro->getAll('nombre = ?', 
            array('SEMESTRE_EN_CURSO'))[0];
        $semestre = (new SemestreModel())->get($parametro->getValor());
        if(!$semestre){
            $semestre = (new SemestreModel())->getAll()[0];
        }
        $this->setSemestre_id($semestre->getId());
        $this->borrado = true;
    }

    public static function getAulaForCode($codigo)
    {
        $aula = new AulaModel();
        $aula = $aula->getAll('codigo = BINARY ?'
            , array($codigo));
        if(count($aula) > 0){
            return $aula[0];
            
        }

        return null;
    }



    public function getCodigo(){
        return $this->codigo;
    }

    public function generateCodigo(){
        do{
            $token = bin2hex(openssl_random_pseudo_bytes(3));
        }while($this->getAulaForCode($token));
        $this->codigo = $token;
        return $this->codigo;
    }


    public function getUsuario_id(){
        return $this->usuario_id;
    }

    public function setUsuario_id($usuario_id){
        $this->usuario_id = $usuario_id;
        return $this;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }

    public function getSemestre_id(){
        return $this->semestre_id;
    }

    public function setSemestre_id($semestre_id){
        $this->semestre_id = $semestre_id;
        return $this;
    }

    public function getEstudiantesObj(){
        $usuario_aula = new UserAulaModel(); 
        return $usuario_aula->getAll('useraula.aula_id = ?', array($this->getId()));
    }

    public function setEstudiantesObj($estudiantesObj){
        $this->estudiantesObj = $estudiantesObj;
        return $this;
    }

    public function getEstudianteObj(){
        return $this->estudianteObj;
    }

    public function setEstudianteObj($estudianteObj){
        $this->estudianteObj = $estudianteObj;
        return $this;
    }

    public function setEstudianteObjByUsuario_id($usuario_id, $borrado = true){
        $usuario_aula = new UserAulaModel(); 
        $usuario_aula->setBorrado($borrado);
        $this->estudianteObj = $usuario_aula->getAll(
            'useraula.aula_id = ? AND useraula.usuario_id = ?'
            , array($this->getId(), $usuario_id));
        if(count($this->estudianteObj)>0)
            $this->estudianteObj = $this->estudianteObj[0]; 
        return $this;
    }

    public function getSemestreObj(){
        if($this->semestreObj)
            return $this->semestreObj;
        $this->semestreObj = new SemestreModel(); 
        $this->semestreObj = $this->semestreObj->get($this->getSemestre_id());
        return $this->semestreObj;
    }

    public function setSemestreObj($semestreObj){
        $this->semestreObj = $semestreObj;
        return $this;
    }

    public function getOwner(){
        return $this->owner;
    }

    public function setOwner($owner){
        $this->owner = $owner;
        return $this;
    }

    public function insertOrUpdate()
    {
        try 
        {
            if($this->getId())
            {
                $sql = "UPDATE ".$this->getTable()." SET 
                            usuario_id          = ?,
                            nombre          = ?,
                            codigo          = ?,
                            semestre_id = ?
                        WHERE id = ?";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->getUsuario_id(),
                            $this->getNombre(),
                            $this->getCodigo(),
                            $this->getSemestre_id(),
                            $this->getId()
                        )
                    );
            }
            else
            {
                $sql = "INSERT INTO ".$this->getTable()."
                            (nombre, codigo, usuario_id, semestre_id)
                            VALUES (?,?,?,?)";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->getNombre(),
                            $this->getCodigo(),
                            $this->getUsuario_id(),
                            $this->getSemestre_id()
                        )
                    );
                $this->setId($this->getDb()->lastInsertId()); 
            }
            
            
            return $this;
        }catch (Exception $e) 
        {
            return null;
        }
    }

    /*public function delete()
    {
        $proyecto = new ProyectoModel();
        $proyecto->setBorrado(false);
        $proyecto = $proyecto->getAll('aula_id = ?', array($this->getId()));
        if(count($proyecto) <= 0){
            return parent::delete();
        }

        try 
        {
            $sql = "UPDATE ".$this->getTable()." SET 
                            borrado          = 1,
                            fecha_borrado          = now()
                        WHERE id = ?";
            $stm = $this->getDb()
                        ->prepare($sql);                   

            $stm->execute(array($this->getId()));
            
            return true;
        } catch (Exception $e) 
        {
            return false;
        }
    }
    */

    public function getArray($loadAll = false){
        $array = parent::getArray();

        $usuario_aula = new UserAulaModel();
        $usuario_aula = $usuario_aula->getAll('useraula.aula_id = ?', array($this->getId()));
        $proyecto = new ProyectoModel();
        if($this->getOwner())
            $proyecto = $proyecto->getAll('aula_id = ?', array($this->getId()));
        else{
            $proyecto = $proyecto->getAll('aula_id = ? AND fecha_inicio <= now()', array($this->getId()));
            if($proyecto)
                foreach ($proyecto as $mproyecto) {
                    $mproyecto->setAulaObj($this);
                    $mproyecto->setForArray(array('id', 'aula_id', 'valor', 
            'nombre', 'enunciado', 'fecha_inicio', 'fecha_fin'));
                }
        }
        $semestre = new SemestreModel();
        $semestre = $semestre->get($this->getSemestre_id());
        if($usuario_aula && (!$this->getOwner()||$loadAll))
            $array['usuarios'] = UserModel::getArrayObjects($usuario_aula);
        else
            $array['usuarios'] = array();
        if($proyecto && (!$this->getOwner()||$loadAll))
            $array['proyectos'] = UserModel::getArrayObjects($proyecto);
        else
            $array['proyectos'] = array();
        if($semestre)
            $array['semestre'] = $semestre->getArray();
        else
            $array['semestre'] = array();
        return $array;
    }

    public function recover()
    {
        try 
        {
            $sql = "UPDATE ".$this->getTable()." SET 
                            borrado          = 0,
                            fecha_borrado          = null
                        WHERE id = ?";
            $stm = $this->getDb()
                        ->prepare($sql);                   

            $stm->execute(array($this->getId()));
            
            return true;
        } catch (Exception $e) 
        {
            return false;
        }
    }
}