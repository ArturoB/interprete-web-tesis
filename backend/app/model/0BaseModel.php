<?php
namespace App\Model;

use App\Lib\Database;

class BaseModel
{
    private $db;
    private $table;
    private $id = null;
    private $forArray;
    protected $borrado = false;
    
    public function __CONSTRUCT()
    {
        $this->db = Database::StartUp();
    }

    public function getTable(){
        return $this->table;
    }

    public function setTable($table){
        $this->table = $table;
        return $this;

    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
        return $this;
    }

    public function getDb(){
        return $this->db;
    }

    public function setDb($db){
        $this->db = $db;
        return $this;
    }

    public function setBorrado($borrado){
        $this->borrado = $borrado;
        return $this;
    }

    public function getForArray(){
        return $this->forArray;
    }

    public function setForArray($forArray){
        $this->forArray = array_merge(array(''), $forArray);
        return $this;
    }

    public function fromArray($array)
    {
     foreach(get_object_vars($this) as $attrName => $attrValue){
        if(array_key_exists($attrName, $array))
            $this->{$attrName} = $array[$attrName];
     }
    }

    public function fromObject($array)
    {
     /*foreach(get_object_vars($this) as $attrName => $attrValue){
        if(array_key_exists($attrName, $array))
            $this->{$attrName} = $array->{$attrName};//$array[$attrName];
     }*/
     foreach(get_object_vars($array) as $attrName => $attrValue){
        //if(array_key_exists($attrName, $array))
            $this->{$attrName} = $array->{$attrName};//$array[$attrName];
     }
    }

    public function getArray()
    {
        $prohibidos = array();
        $prohibidos[] = ''; // falla sin esto
        $prohibidos[] = 'table';
        $prohibidos[] = 'db';
        $prohibidos[] = 'response';
        $array = array();
        foreach(get_object_vars($this) as $attrName => $attrValue){
        if(array_search($attrName, $this->forArray))
            $array[$attrName] = $this->{$attrName};
        }
        return $array;
    }



    public static function getArrayObjects($objects)
    {
        $prohibidos = array();
        $prohibidos[] = ''; // falla sin esto
        $prohibidos[] = 'table';
        $prohibidos[] = 'db';
        $prohibidos[] = 'response';
        $objectsReturn = array();
        foreach ($objects as $value) {
            $objectsReturn[] = $value->getArray(); 
        }
        return $objectsReturn;
    }



    public function get($id)
    {

        try
        {   $anexo = '';
            if($this->borrado)
                $anexo = ' AND borrado = 0 ';
            $result = array();

            $stm = $this->db->prepare("SELECT * FROM ".$this->table." WHERE id = ? ".$anexo."limit 1");
            $stm->execute(array($id));

            
            $array = $stm->fetch();
        }
        catch(Exception $e)
        {

            $array = null;
        }

        //$array = parent::get($id);
        $name = ''.static::class;
        if($array){
            $rol = new $name();
            $rol->fromObject($array);
            return $rol;
            
        }

        return null;
    }

    /**
    $where: condición de la consulta
    $values: valores para la condición
    */   
    public function getAll($where = ' 1', $values = array())
    {
		try
		{   $anexo = '';
            if($this->borrado)
                $anexo = ' AND borrado = 0 ';
			$result = array();

			$stm = $this->db->prepare("SELECT * FROM ".$this->table." where ".$where.$anexo);
			$stm->execute($values);
            
            $array = $stm->fetchAll();
		}
		catch(Exception $e)
		{
            $array = null;
		}

        //$array = parent::getAll($where = '1', $values = array());
        $name = ''.static::class;
        $all = array();
        if($array){
            foreach ($array as $value) {
                $rol = new $name;
                $rol->fromObject($value);
                $all[] = $rol;
            }
            return $all;

            
        }

        return null;
    }
    
    
    
    public function insertOrUpdate($data)
    {/*
		try 
		{
            if(isset($data['id']))
            {
                $sql = "UPDATE $this->table SET 
                            Nombre          = ?, 
                            Apellido        = ?,
                            Correo          = ?,
                            Sexo            = ?,
                            Sueldo          = ?,
                            Profesion_id    = ?,
                            FechaNacimiento = ?
                        WHERE id = ?";
                
                $this->db->prepare($sql)
                     ->execute(
                        array(
                            $data['Nombre'], 
                            $data['Apellido'],
                            $data['Correo'],
                            $data['Sexo'],
                            $data['Sueldo'],
                            $data['Profesion_id'],
                            $data['FechaNacimiento'],
                            $data['id']
                        )
                    );
            }
            else
            {
                $sql = "INSERT INTO $this->table
                            (Nombre, Apellido, Correo, Sexo, Sueldo, Profesion_id, FechaNacimiento, FechaRegistro)
                            VALUES (?,?,?,?,?,?,?,?)";
                
                $this->db->prepare($sql)
                     ->execute(
                        array(
                            $data['Nombre'], 
                            $data['Apellido'],
                            $data['Correo'],
                            $data['Sexo'],
                            $data['Sueldo'],
                            $data['Profesion_id'],
                            $data['FechaNacimiento'],
                            date('Y-m-d')
                        )
                    ); 
            }
            
			$this->response->setResponse(true);
            return $this->response;
		}catch (Exception $e) 
		{
            $this->response->setResponse(false, $e->getMessage());
		}*/
    }
    
    public function delete()
    {
        try 
        {
            $stm = $this->db
                        ->prepare("DELETE FROM $this->table WHERE id = ?");                   

            $stm->execute(array($this->getId()));
            
            return true;
        } catch (Exception $e) 
        {
            return false;
        }
    }
}