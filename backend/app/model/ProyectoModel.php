<?php
namespace App\Model;

use App\Lib\Database;
use App\Lib\Response;

class ProyectoModel extends BaseModel
{
    protected $aula_id;
    protected $nombre;
    protected $valor;
    protected $enunciado;
    protected $fecha_inicio;
    protected $fecha_fin;
    protected $clave;
    protected $aulaObj;
    protected $validacion_ip;
    protected $ip_rango_inicial;
    protected $ip_rango_final;
    
    public function __CONSTRUCT()
    {
        $this->setTable('proyecto');
        $this->setForArray(array('id', 'aula_id', 'valor', 
            'nombre', 'enunciado', 'fecha_inicio', 'fecha_fin',
             'validacion_ip', 'ip_rango_inicial', 'ip_rango_final'));
        parent::__CONSTRUCT();
        $this->borrado = true;
    }

    public function getAula_id(){
        return $this->aula_id;
    }

    public function setAula_id($aula_id){
        $this->aula_id = $aula_id;
        return $this;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }

    public function getValor(){
        return $this->valor;
    }

    public function setValor($valor){
        $this->valor = $valor;
        return $this;
    }

    public function getEnunciado(){
        return $this->enunciado;
    }

    public function setEnunciado($enunciado){
        $this->enunciado = $enunciado;
        return $this;
    }

    public function getFecha_inicio(){
        return $this->fecha_inicio;
    }

    public function setFecha_inicio($fecha_inicio){
        $this->fecha_inicio = $fecha_inicio;
        return $this;
    }

    public function getFecha_fin(){
        return $this->fecha_fin;
    }

    public function setFecha_fin($fecha_fin){
        $this->fecha_fin = $fecha_fin;
        return $this;
    }

    public function getClave(){
        return $this->clave;
    }

    public function setClave($clave){
        $this->clave = $clave;
        return $this;
    }

    public function setBorrado($borrado){
        $this->borrado = $borrado;
        return $this;
    }

    public function getAulaObj(){
        if(!$this->aulaObj)
            return (new AulaModel())->get($this->getAula_id());
        return $this->aulaObj;
    }

    public function setAulaObj($aulaObj){
        $this->aulaObj = $aulaObj;
        return $this;
    }

    public function isIpPermitida($needle){
        if($this->validacion_ip == 0)
            return true;
        if((ip2long($needle) >= ip2long($this->ip_rango_inicial))
         && (ip2long($needle) <= ip2long($this->ip_rango_final))) 
            return true;
        return false;
    }

    public function hasClave(){
        if($this->getClave() != sha1(null) && $this->getClave())
            return true;
        return false;
    }

    public function insertOrUpdate()
    {
		try 
		{
            if($this->getId())
            {
                $sql = "UPDATE ".$this->getTable()." SET 
                            nombre          = ?,
                            aula_id          = ?,
                            valor          = ?,
                            enunciado          = ?,
                            fecha_inicio          = ?,
                            fecha_fin          = ?,
                            validacion_ip = ?,
                            ip_rango_inicial = ?,
                            ip_rango_final = ?,
                            clave = ?
                        WHERE id = ?";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->nombre,
                            $this->aula_id,
                            $this->valor,
                            $this->enunciado,
                            $this->fecha_inicio,
                            $this->fecha_fin,
                            $this->validacion_ip,
                            $this->ip_rango_inicial,
                            $this->ip_rango_final,
                            $this->clave,
                            $this->getId()
                        )
                    );
            }
            else
            {
                $sql = "INSERT INTO ".$this->getTable()."
                            (nombre, aula_id, valor, enunciado, fecha_inicio, fecha_fin,
                                validacion_ip, ip_rango_inicial, ip_rango_final, clave)
                            VALUES (?,?,?,?,?,?,?,?,?, sha(?))";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->nombre,
                            $this->aula_id,
                            $this->valor,
                            $this->enunciado,
                            $this->fecha_inicio,
                            $this->fecha_fin,
                            $this->validacion_ip,
                            $this->ip_rango_inicial,
                            $this->ip_rango_final,
                            $this->clave,
                        )
                    ); 
                $this->setId($this->getDb()->lastInsertId());
            }
            
			
            return $this;
		}catch (Exception $e) 
		{
            return null;
		}
    }

    /*public function delete()
    {
        $entrega = new EntregaModel();
        $entrega = $entrega->getAll('proyecto_id = ?', array($this->getId()));
        if(count($entrega) <= 0){
            return parent::delete();
        }
        try 
        {
            $sql = "UPDATE ".$this->getTable()." SET 
                            borrado          = 1,
                            fecha_borrado          = now()
                        WHERE id = ?";
            $stm = $this->getDb()
                        ->prepare($sql);                   

            $stm->execute(array($this->getId()));
            
            return true;
        } catch (Exception $e) 
        {
            return false;
        }
    }*/

    public function getArray(){
        $array = parent::getArray();

        $entrega = new EntregaModel();
        $aula = $this->getAulaObj(); 
        if($aula->getOwner())
            $entrega = $entrega->getAll('proyecto_id = ?', array($this->getId()));
        else{
            $estudiante = $aula->getEstudianteObj();
            $entrega = $entrega->getAll(
                'proyecto_id = ? and usuario_aula_id = ?'
                , array($this->getId(), $estudiante->getId()));
        }
        if($entrega && !$aula->getOwner())
            $array['entregas'] = UserModel::getArrayObjects($entrega);
        else
            $array['entregas'] = array();
        if($this->getClave() != sha1(null) && $this->getClave())
            $array['clave'] = true;
        else
            $array['clave'] = false;
        return $array;
    }

    public function getArrayO(){
        $array = parent::getArray();

        $entrega = new EntregaModel();
        $entrega = $entrega->getAll('proyecto_id = ?', array($this->getId()));
        if($entrega)
            $array['entregas'] = UserModel::getArrayObjects($entrega);
        else
            $array['entregas'] = array();
        return $array;
    }

    public function recover()
    {
        try 
        {
            $sql = "UPDATE ".$this->getTable()." SET 
                            borrado          = 0,
                            fecha_borrado          = null
                        WHERE id = ?";
            $stm = $this->getDb()
                        ->prepare($sql);                   

            $stm->execute(array($this->getId()));
            
            return true;
        } catch (Exception $e) 
        {
            return false;
        }
    }

    
}