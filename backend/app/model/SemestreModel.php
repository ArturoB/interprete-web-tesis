<?php
namespace App\Model;

use App\Lib\Database;
use App\Lib\Response;

class SemestreModel extends BaseModel
{
    protected $nombre;
    
    public function __CONSTRUCT()
    {
        $this->setTable('semestre');
        $this->setForArray(array('id', 'nombre'));
        parent::__CONSTRUCT();
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }

    public function insertOrUpdate()
    {
        try 
        {
            if($this->getId())
            {
                $sql = "UPDATE ".$this->getTable()." SET
                            nombre          = ?
                        WHERE id = ?";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->getNombre(),
                            $this->getId()
                        )
                    );
            }
            else
            {
                $sql = "INSERT INTO ".$this->getTable()."
                            (nombre)
                            VALUES (?)";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->getNombre()
                        )
                    );
                $this->setId($this->getDb()->lastInsertId()); 
            }
            
            
            return $this;
        }catch (Exception $e) 
        {
            return null;
        }
    }

    public function getArray(){
        $array = parent::getArray();
        return $array;
    }

}