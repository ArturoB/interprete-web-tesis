<?php
namespace App\Model;

use App\Lib\Database;
use App\Lib\Response;

class EntregaModel extends BaseModel
{
    protected $proyecto_id;
    protected $usuario_aula_id;
    protected $codigo;
    protected $nota;
    protected $comentarios;
    protected $ip;
    protected $crc;
    protected $proyectoObj;
    protected $usuario_aulaObj;
    
    public function __CONSTRUCT()
    {
        $this->setTable('entrega');
        $this->setForArray(array('id', 'proyecto_id', 'usuario_aula_id', 
            'codigo', 'nota', 'comentarios', 'crc'));
        parent::__CONSTRUCT();
    }

    public function getProyecto_id(){
        return $this->proyecto_id;
    }

    public function setProyecto_id($proyecto_id){
        $this->proyecto_id = $proyecto_id;
        return $this;
    }

    public function getUsuario_aula_id(){
        return $this->usuario_aula_id;
    }

    public function setUsuario_aula_id($usuario_aula_id){
        $this->usuario_aula_id = $usuario_aula_id;
        return $this;
    }

    public function getCodigo(){
        return $this->codigo;
    }

    public function setCodigo($codigo){
        $this->codigo = $codigo;
        return $this;
    }

    public function getNota(){
        return $this->nota;
    }

    public function setNota($nota){
        $this->nota = $nota;
        return $this;
    }

    public function getComentarios(){
        return $this->comentarios;
    }

    public function setComentarios($comentarios){
        $this->comentarios = $comentarios;
        return $this;
    }

    public function getIp(){
        return $this->ip;
    }

    public function setIp($ip){
        $this->ip = $ip;
        return $this;
    }

    public function getCrc(){
        return $this->crc;
    }

    public function setCrc($crc){
        $this->crc = $crc;
        return $this;
    }

    public function getProyectoObj(){

        return (new ProyectoModel())->get($this->getProyecto_id());
    }

    public function setProyectoObj($proyectoObj){
        $this->proyectoObj = $proyectoObj;
        return $this;
    }

    public function getUsuario_aulaObj(){
        //if($this->usuario_aulaObj)
        //    return $this->usuario_aulaObj;
        $usuario_aula = new UserAulaModel();
        $usuario_aula->setBorrado(false);
        $this->usuario_aulaObj = $usuario_aula->get($this->getUsuario_aula_id());

        return $this->usuario_aulaObj;
    }

    public function insertOrUpdate()
    {
		try 
		{
            if($this->getId())
            {
                $sql = "UPDATE ".$this->getTable()." SET 
                            proyecto_id          = ?,
                            usuario_aula_id          = ?,
                            codigo          = ?,
                            nota          = ?,
                            comentarios          = ?,
                            crc          = ?,
                            ip = ?
                        WHERE id = ?";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->proyecto_id,
                            $this->usuario_aula_id,
                            $this->codigo,
                            $this->nota,
                            $this->comentarios,
                            $this->crc,
                            $this->ip,
                            $this->getId()
                        )
                    );
            }
            else
            {
                $sql = "INSERT INTO ".$this->getTable()."
                            (proyecto_id, usuario_aula_id, codigo, nota, comentarios, ip, crc)
                            VALUES (?,?,?,?,?,?,?)";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->proyecto_id,
                            $this->usuario_aula_id,
                            $this->codigo,
                            $this->nota,
                            $this->comentarios,
                            $this->ip,
                            $this->crc,
                        )
                    ); 
                $this->setId($this->getDb()->lastInsertId());
            }
            
			
            return $this;
		}catch (Exception $e) 
		{
            return null;
		}
    }



    public function getArray(){
        $array = parent::getArray();
        $usuario_aula = $this->getUsuario_aulaObj();
        $array['usuario'] = $usuario_aula->getArray();
        return $array;
    }

    
}