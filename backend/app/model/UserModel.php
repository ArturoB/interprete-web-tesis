<?php
namespace App\Model;

use App\Lib\Database;
use App\Lib\Response;

class UserModel extends BaseModel
{
    protected $rol_id;
    protected $alias;
    protected $clave;
    protected $nombre;
    protected $cedula;
    protected $descripcion;
    protected $correo;
    protected $carrera_id;
    protected $semestre_id;
    protected $token_reseteo_clave;
    protected $rolObj;
    
    public function __CONSTRUCT()
    {
        $this->setTable('usuario');
        $this->setForArray(array('id', 'rol_id', 'alias', 'nombre', 'descripcion', 'correo',
            'carrera_id', 'semestre_id', 'cedula'));
        parent::__CONSTRUCT();
        $parametro = new ParametroModel();
        $parametro = $parametro->getAll('nombre = ?', 
            array('SEMESTRE_EN_CURSO'))[0];
        $semestre = (new SemestreModel())->get($parametro->getValor());
        if(!$semestre){
            $semestre = (new SemestreModel())->getAll()[0];
        }
        $this->setSemestre_id($semestre->getId());
    }


    public static function login($alias, $pass)
    {
        $user = new UserModel();
        $user = $user->getAll('(correo = ?) AND clave = BINARY sha(?)'
            , array($alias, $pass));
        if(count($user) > 0){
            if($user[0]->getToken_reseteo_clave()){
                try 
                {
                    $sql = "UPDATE ".$user[0]->getTable()." SET 
                                    token_reseteo_clave          = ?
                                WHERE id = ?";
                    $stm = $user[0]->getDb()
                                ->prepare($sql);                   

                    $stm->execute(array(null, $user[0]->getId()));
                } catch (Exception $e) 
                {
                }
            }
            
            return $user[0];

        }

        return null;
    }

    public static function search($buscar)
    {
        $user = new UserModel();
        $user = $user->getAll('correo = ? OR cedula = ?'
            , array($buscar, $buscar));
        if(count($user) > 0){
            return $user[0];
        }

        return null;
    }

    public static function searchByToken($token)
    {
        $user = new UserModel();
        $user = $user->getAll('token_reseteo_clave = ?'
            , array($token));
        if(count($user) > 0){
            return $user[0];
        }

        return null;
    }

    public function getRol_id(){
        return $this->rol_id;
    }

    public function setRol_id($rol_id){
        $this->rol_id = $rol_id;
        return $this;
    }

    public function getAlias(){
        return $this->alias;
    }

    public function setAlias($alias){
        $this->alias = $alias;
        return $this;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }

    public function getCedula(){
        return $this->cedula;
    }

    public function setCedula($cedula){
        $this->cedula = $cedula;
        return $this;
    }

    public function getClave(){
        return $this->clave;
    }

    public function setClave($clave){
        $this->clave = $clave;
        return $this;
    }

    public function getDescripcion(){
        return $this->descripcion;
    }

    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
        return $this;
    }

    public function getCorreo(){
        return $this->correo;
    }

    public function setCorreo($correo){
        $this->correo = $correo;
        return $this;
    }

    public function getCarrera_id(){
        return $this->carrera_id;
    }

    public function setCarrera_id($carrera_id){
        $this->carrera_id = $carrera_id;
        return $this;
    }

    public function getSemestre_id(){
        return $this->semestre_id;
    }

    public function setSemestre_id($semestre_id){
        $this->semestre_id = $semestre_id;
        return $this;
    }

    public function getToken_reseteo_clave(){
        return $this->token_reseteo_clave;
    }

    public function getRolObj(){

        return (new RolModel())->get($this->getRol_id());
    }

    public function setRolObj($rolObj){
        $this->rolObj = $rolObj;
        return $this;
    }

    public function insertOrUpdate()
    {
		try 
		{
            if($this->getId())
            {
                $sql = "UPDATE ".$this->getTable()." SET 
                            nombre          = ?,
                            rol_id          = ?,
                            clave          = ?,
                            correo          = ?,
                            alias          = ?,
                            descripcion          = ?,
                            cedula          = ?,
                            carrera_id          = ?,
                            semestre_id          = ?
                        WHERE id = ?";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->nombre,
                            $this->rol_id,
                            $this->clave,
                            $this->correo,
                            $this->alias,
                            $this->descripcion,
                            $this->cedula,
                            $this->carrera_id,
                            $this->semestre_id,
                            $this->getId()
                        )
                    );
            }
            else
            {
                $sql = "INSERT INTO ".$this->getTable()."
                            (nombre, rol_id, clave, correo, alias, descripcion,
                                cedula, carrera_id, semestre_id)
                            VALUES (?,?,sha(?),?,?,?,?,?,?)";
                
                $this->getDb()->prepare($sql)
                     ->execute(
                        array(
                            $this->nombre,
                            $this->rol_id,
                            $this->clave,
                            $this->correo,
                            $this->alias,
                            $this->descripcion,
                            $this->cedula,
                            $this->carrera_id,
                            $this->semestre_id,
                        )
                    );
                $this->setId($this->getDb()->lastInsertId()); 
            }
            
			
            return $this;
		}catch (Exception $e) 
		{
            return null;
		}
    }

    public function getArray(){
        $array = parent::getArray();

        $script = new ScriptModel();
        $script = $script->getAll('usuario_id = ?', array($this->getId()));//aulas profesor
        $roles = new RolModel();
        $roles = $roles->getAll();
        $parametros = new ParametroModel();
        $parametros = $parametros->getAll(' not nombre = ?', array('PERMITIR_REGISTRO_EXTERNO'));
        $semestreEnCurso = new ParametroModel();
        //se buscar semestre en curso y semestre anterior únicamente
        $semestreEnCurso = $semestreEnCurso->getAll('nombre = ?', 
            array('SEMESTRE_EN_CURSO'))[0];
        $semestreAnt = -1;
        if(!$semestreEnCurso){
            $semestreEnCurso = (new SemestreModel())->getAll()[0];
        }
        else{
            $semestres = (new SemestreModel())->getAll();
            for ($i=0; $i < count($semestres); $i++) { 
                if($semestres[$i]->getId() == $semestreEnCurso->getValor()){
                    if($i>0){
                        $semestreAnt = $semestres[$i-1]->getId();
                    }
                    break;
                }
            }
        }
        //aulas profesor
        $aula = new AulaModel();
        $aula = $aula->getAll('usuario_id = ? AND semestre_id in (?,?)', array($this->getId(),
                                                $semestreEnCurso->getValor(),
                                                $semestreAnt));
        $semestreEnCurso = (new SemestreModel())->get($semestreEnCurso->getValor());
        //aulas estudiante
        $miAula = new AulaModel();
        $miAula = $miAula->getAll('id IN 
            (SELECT usuaula.aula_id FROM usuario_aula usuaula 
                WHERE usuaula.usuario_id = ? AND usuaula.borrado = 0)'
            , array($this->getId()));
        //para restringir información a estudiante de aula
        if($miAula)
            foreach ($miAula as $maula) {
                $maula->setOwner(false);
                $maula->setEstudianteObjByUsuario_id($this->getId());
            }
        $rol = $this->getRolObj();
        /*if($script)
            $array['scripts'] = UserModel::getArrayObjects($script);
        else*/
            $array['scripts'] = array();
        if($aula)
            $array['aulas'] = UserModel::getArrayObjects($aula);
        else
            $array['aulas'] = array();
        if($miAula)
            $array['misAulas'] = UserModel::getArrayObjects($miAula);
        else
            $array['misAulas'] = array();
        if($rol->getNombre() == 'ADMINISTRADOR'){
            $array['roles'] = UserModel::getArrayObjects($roles);
            $array['parametros'] = UserModel::getArrayObjects($parametros);
        }
        else{
            $array['roles'] = array();
            $array['parametros'] = array();
        }
        $array['rol'] = $rol->getArray();
        $array['semestre'] = $semestreEnCurso->getArray();
        return $array;
    }

    public function generarToken_reseteo_clave()
    {
        $cadena = $this->getId().$this->getCorreo().rand(1,9999999).date('Y-m-d');
        $this->token_reseteo_clave = sha1($cadena);
        try 
        {
            $sql = "UPDATE ".$this->getTable()." SET 
                            token_reseteo_clave          = ?
                        WHERE id = ?";
            $stm = $this->getDb()
                        ->prepare($sql);                   

            $stm->execute(array($this->token_reseteo_clave, $this->getId()));
            
            return $this->token_reseteo_clave;
        } catch (Exception $e) 
        {
            return false;
        }
    }

    public function resetearToken_reseteo_clave()
    {
        try 
        {
            $sql = "UPDATE ".$this->getTable()." SET 
                            token_reseteo_clave          = ?,
                            clave = sha(?)
                        WHERE id = ?";
            $stm = $this->getDb()
                        ->prepare($sql);                   

            $stm->execute(array(null, $this->clave, $this->getId()));
            
            return true;
        } catch (Exception $e) 
        {
            return false;
        }
    }
}