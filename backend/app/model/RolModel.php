<?php
namespace App\Model;

use App\Lib\Database;
use App\Lib\Response;

class RolModel extends BaseModel
{
    protected $nombre;
    
    public function __CONSTRUCT()
    {
        $this->setTable('rol');
        $this->setForArray(array('id', 'nombre'));
        parent::__CONSTRUCT();
    }


    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }

    public function insertOrUpdate()
    {
		try 
		{
            if($this->id)
            {
                $sql = "UPDATE $this->table SET 
                            nombre          = ?
                        WHERE id = ?";
                
                $this->db->prepare($sql)
                     ->execute(
                        array(
                            $this->nombre
                        )
                    );
            }
            else
            {
                $sql = "INSERT INTO $this->table
                            (nombre)
                            VALUES (?,?,?,?,?,?,?,?)";
                
                $this->db->prepare($sql)
                     ->execute(
                        array(
                            $this->nombre
                        )
                    ); 
            }
            
			
            return $this;
		}catch (Exception $e) 
		{
            return null;
		}
    }
    
    
}