<?php

use App\Model\UserModel;
use App\Model\UserAulaModel;
use App\Model\AulaModel;
use App\Model\RolModel;

$app->group('/usuario_aula/', function ()use($app) {

    //Profesor añade estudiante a su aula
    $this->post('add/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      $data = $req->getParsedBody();
      if($user->getRolObj()->getNombre()=='EXTERNO'
          ||$user->getRolObj()->getNombre()=='ESTUDIANTE')
        return $res->withStatus(401);
      if(!array_key_exists('id', $data) || !array_key_exists('buscar', $data))
        return $res->withStatus(401);
      $aula = new AulaModel();
      $user_aula = new UserAulaModel();
      $estudiante = UserModel::search($data['buscar']);
      $aula = $aula->get($data['id']);
      if(!$aula || !$estudiante)
        return $res->withStatus(401);
      $aula->setEstudianteObjByUsuario_id($estudiante->getId());
      if($aula->getEstudianteObj())
        return $res->withStatus(401);
      $aula->setEstudianteObjByUsuario_id($estudiante->getId(), false);
      if($aula->getEstudianteObj()){
        $aula->getEstudianteObj()->reCreate();
        return $res->withStatus(200)
                      ->write(json_encode($aula->getEstudianteObj()->getArray()));
      }

      $user_aula->setAula_id($aula->getId());
      $user_aula->setUsuario_id($estudiante->getId());
      $user_aula->setAlias($estudiante->getAlias());
        if($user_aula)
            if($user_aula->insertOrUpdate())
              return $res->withStatus(200)
                      ->write(json_encode($user_aula->getArray()));
        return $res->withStatus(401);
    });

    //Estudiante busca aula y se une a la misma
    $this->post('save/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      //if($user->getRolObj()->getNombre()=='EXTERNO')
      //  return $res->withStatus(401);
      $aula = new AulaModel();
      $user_aula = new UserAulaModel();
      $data = $req->getParsedBody();
      if(!array_key_exists('codigo', $data))
        return $res->withStatus(401);
      $aula = AulaModel::getAulaForCode($data['codigo']);
      if(!$aula)
        return $res->withStatus(401);
      $aula->setEstudianteObjByUsuario_id($user->getId());
      if($aula->getEstudianteObj())
        return $res->withStatus(401);
      $aula->setEstudianteObjByUsuario_id($user->getId(), false);
      if($aula->getEstudianteObj()){
        $aula->getEstudianteObj()->reCreate();
        return $res->withStatus(200)
                      ->write(json_encode($user->getArray()));
      }
      $user_aula->setAula_id($aula->getId());
      $user_aula->setUsuario_id($user->getId());
        if($user_aula)
            if($user_aula->insertOrUpdate()){
              //cambio de rol en caso de ser externo
              if($user->getRolObj()->getNombre()=='EXTERNO'){
                $rol = new RolModel();
                $rol = $rol->getAll('nombre = ?',
                  array('ESTUDIANTE'))[0];
                $user->setRol_id($rol->getId());
                $user->insertOrUpdate();
              }
              return $res->withStatus(200)
                      ->write(json_encode($user->getArray()));
            }
        return $res->withStatus(401);
    });

    
    //Profesor saca estudiante de su aula
    $this->post('delete/{id}', function ($req, $res, $args)use($app) {
        $user_aula = (new UserAulaModel())->get($args['id']);
        $user = $app->user;
        $aula = (new AulaModel())->get($user_aula->getAula_id());
        if($aula->getUsuario_id() != $user->getId())
          return $res->withStatus(401);
        if($user_aula)
            if($user_aula->delete())
              return $res->withStatus(200);
        return $res->withStatus(401);
    });
    
    //Estudiante sale de un aula
    $this->post('back/{idAula}', function ($req, $res, $args)use($app) {
        $user = $app->user;
        $aula = (new AulaModel())->get($args['idAula']);
        $aula->setEstudianteObjByUsuario_id($user->getId());
        if(!$aula->getEstudianteObj())
          return $res->withStatus(401);
        if($aula->getEstudianteObj())
            if($aula->getEstudianteObj()->delete())
              return $res->withStatus(200);
        return $res->withStatus(401);
    });
    
});