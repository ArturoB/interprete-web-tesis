<?php

use App\Model\UserModel;
use App\Model\RolModel;
use App\Model\AulaModel;
use App\Model\UserAulaModel;

$app->group('/user/', function ()use($app) {
    
    $this->post('login', function ($req, $res, $args) {

        $data = $req->getParsedBody();
        $user = UserModel::login($data['alias'], $data['clave']);
        if(!$user){
          session_destroy();
          return $res->withStatus(401);
        }
        $_SESSION['user'] = $user->getId();
        return $res->getBody()
                   ->write(json_encode($user->getArray()));
    });
    
    $this->post('logout', function ($req, $res, $args) {
        session_destroy();
        return $res;
    });
    
    $this->post('info', function ($req, $res, $args)use($app) {
        $user = $app->user;
        if(!$user)
          return $res->withStatus(401);
        return $res->getBody()
                   ->write(json_encode($user->getArray()));
    });


    $this->post('save', function ($req, $res, $args)use($app) {
      $aula = new AulaModel();
      $user = new UserModel();
      $user_aula = new UserAulaModel();
      $data = $req->getParsedBody();
      if(!array_key_exists('codigo', $data))
        return $res->withStatus(401);
      if(strlen($data['codigo']) <= 0)
        return $res->withStatus(401);
      $aula = AulaModel::getAulaForCode($data['codigo']);
      if(!$aula)
        return $res->withStatus(401);

      $user->fromArray($data);
      $rol = new RolModel();
      $rol = $rol->getAll('nombre = ?',
              array('ESTUDIANTE'))[0];
      $user->setRol_id($rol->getId());
      $alias = explode('@', $data['correo']);
      $user->setAlias($alias[0]);
        if($user)
            if($user->insertOrUpdate()){
              $user_aula->setAula_id($aula->getId());
              $user_aula->setUsuario_id($user->getId());
              $user_aula->insertOrUpdate();
              $_SESSION['user'] = $user->getId();
              return $res->withStatus(200)
                      ->write(json_encode($user->getArray()));;
            }
        return $res->withStatus(401);
    });

    $this->post('saveE', function ($req, $res, $args)use($app) {
      $user = new UserModel();
      $data = $req->getParsedBody();
      if(!array_key_exists('nombre', $data) || !array_key_exists('clave', $data)
          || !array_key_exists('correo', $data)|| !array_key_exists('cedula', $data))
        return $res->withStatus(401);
      if(strlen($data['clave']) <= 0)
        return $res->withStatus(401);

      $user->setNombre($data['nombre']);
      $user->setClave($data['clave']);
      $user->setCorreo($data['correo']);
      $user->setCedula($data['cedula']);
      $rol = new RolModel();
      $rol = $rol->getAll('nombre = ?',
              array('EXTERNO'))[0];
      $user->setRol_id($rol->getId());
      $alias = explode('@', $data['correo']);
      $user->setAlias($alias[0]);
        if($user)
            if($user->insertOrUpdate()){
              $_SESSION['user'] = $user->getId();
              return $res->withStatus(200)
                      ->write(json_encode($user->getArray()));;
            }
        return $res->withStatus(401);
    });

    $this->post('search/', function ($req, $res, $args)use($app) {
        $user = $app->user;
        $data = $req->getParsedBody();
        if($user->getRolObj()->getNombre()!='ADMINISTRADOR' || !array_key_exists('buscar', $data))
          return $res->withStatus(401);
        $user = UserModel::search($data['buscar']);
        if(!$user){
          return $res->withStatus(401);
        }
        return $res->getBody()
                   ->write(json_encode($user->getArray()));
    });

    $this->post('saveA', function ($req, $res, $args)use($app) {
      $user = new UserModel();
      $userL = $app->user;
      $data = $req->getParsedBody();
      if($userL->getRolObj()->getNombre()!='ADMINISTRADOR')
        return $res->withStatus(401);

      if(array_key_exists('id', $data)){
        $user = (new UserModel())->get($data['id']);
        if(strlen($data['clave'])>0){
          $user->setClave(sha1($data['clave']));
        }
        if(!$user)
          $user = new UserModel();
      }
      else{
        if(strlen($data['clave'])<=0)
          return $res->withStatus(401);
        $user->setClave($data['clave']);
      }
      $user->setNombre($data['nombre']);
      $user->setCedula($data['cedula']);
      $user->setCorreo($data['correo']);
      $user->setCarrera_id($data['carrera_id']);
      $user->setRol_id($data['rol_id']);
      $alias = explode('@', $data['correo']);
      $user->setAlias($alias[0]);
        if($user)
            if($user->insertOrUpdate()){
              return $res->withStatus(200)
                      ->write(json_encode($user->getArray()));;
            }
        return $res->withStatus(401);
    });

    $this->post('forgotpassword', function ($req, $res, $args)use($app) {
    $data = $req->getParsedBody();
    if(!array_key_exists('correo', $data))
          return $res->withStatus(401);
    $user = UserModel::search($data['correo']);
    if(!$user)
          return $res->withStatus(401);
    if(!$user->getToken_reseteo_clave())
      $param =  $user->generarToken_reseteo_clave();
    else
      $param =  $user->getToken_reseteo_clave();

        $url = (isset($_SERVER['HTTPS'])?'https://':'http://');
        $url .= $_SERVER["SERVER_NAME"].str_replace('public/user/forgotpassword', '', $_SERVER['REQUEST_URI']).'templates/resetPass.html?token='.$param.'&url='.$url.$_SERVER["SERVER_NAME"].str_replace('forgotpassword', '', $_SERVER['REQUEST_URI']);
        $mensaje = '<html>
          
         <head>
            <title>Restablece tu contraseña</title>
         </head>
         <body>
           <p>Hemos recibido una petición para restablecer la contraseña de tu cuenta.</p>
           <p>Si hiciste esta petición, haz clic en el siguiente enlace, si no hiciste esta petición puedes ignorar este correo.</p>
           <p>
             <strong>Enlace para restablecer tu contraseña</strong><br>
             <a  href="'.$url.'"> Restablecer contraseña </a>
           </p>
         </body>
        </html>';

       $cabeceras = 'MIME-Version: 1.0' . "\r\n";
       $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
       $cabeceras .= 'From: ñnumeral <nnumeral@nnumeral.com>' . "\r\n";
       mail($user->getCorreo(), "Recuperar Contraseña", $mensaje, $cabeceras);  
    /*$mail = new PHPMailer;

    $mail->setFrom('nnumeral@nnumeral.com', 'ñnumeral');
    $mail->addAddress($user->getCorreo());
    $mail->addReplyTo('no-reply@nnumeral.com', 'ñnumeral');

    $mail->isHTML(true);
//$_SERVER["SERVER_NAME"]
    $mail->Subject = 'Instrucciones para reestablecer tu clave del intérprete ñ#';
    $mail->Body    = "
        <p>Hola,</p>
        <p>            
        Hemos recibido una petición para reestablecer la clave de tu cuenta del intérprete ñ# asociada a este correo.
        </p>
        <p>
        Para confirmar y cambiar tu clave, presiona <a href=\"".$url."\">aquí</a>.  Si tu no solicitaste este correo, por favor ignóralo.
        </p>
        <p>
        Saludos.
        <br>
        El equipo de ñ#
        </p>";

    if(!$mail->send()) {
        return $res->withStatus(401);
    }     */

        return $res->withStatus(200);
  });

    $this->post('resetpassword', function ($req, $res, $args)use($app) {
      $user = new UserModel();
      $data = $req->getParsedBody();

      if(!array_key_exists('token', $data)){
        return $res->withStatus(401);
      }
      if(strlen($data['token'])<=0)
          return $res->withStatus(401);
      $user = UserModel::searchByToken($data['token']);
      if(!$user){
        return $res->withStatus(401);
      }

      if(strlen($data['clave'])<=0)
          return $res->withStatus(401);
        $user->setClave($data['clave']);
        if($user)
            if($user->resetearToken_reseteo_clave()){
              return $res->withStatus(200)
                      ->write(json_encode($user->getArray()));;
            }
        return $res->withStatus(401);
    });
    
});