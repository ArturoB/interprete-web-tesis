<?php

use App\Model\UserModel;
use App\Model\RolModel;
use App\Model\AulaModel;

$app->group('/aula/', function ()use($app) {
    
    $this->post('save/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      $data = $req->getParsedBody();
      if($user->getRolObj()->getNombre()=='ESTUDIANTE'
        ||$user->getRolObj()->getNombre()=='EXTERNO'
        || array_key_exists('codigo', $data))
        return $res->withStatus(401);
      $aula = new AulaModel();
      if(array_key_exists('id', $data)){
        $aula = (new AulaModel())->get($data['id']);
        if(!$aula)
          $aula = new AulaModel();
      }
      else{
        $aula->setUsuario_id($user->getId());
        $aula->generateCodigo();
      }
      $aula->setNombre($data['nombre']);
        if($aula)
          if($aula->getUsuario_id() == $user->getId()){
            if($aula->insertOrUpdate())
              return $res->withStatus(200)
                      ->write(json_encode($aula->getArray()));
          }
        return $res->withStatus(401);
    });
    
    $this->post('delete/{id}', function ($req, $res, $args)use($app) {
        $aula = (new AulaModel())->get($args['id']);
        $user = $app->user;
        if($aula)
          if($aula->getUsuario_id() == $user->getId()){
            if($aula->delete())
              return $res->withStatus(200);
          }
        return $res->withStatus(401);
    });

    $this->post('get/{id}', function ($req, $res, $args)use($app) {
        $aula = (new AulaModel())->get($args['id']);
        $user = $app->user;
        if($aula)
          if($aula->getUsuario_id() == $user->getId()){
            return $res->withStatus(200)
                  ->write(json_encode($aula->getArray(true)));
          }
        return $res->withStatus(401);
    });

    /*$this->post('searchInTrash/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      if($user->getRolObj()->getNombre()=='ESTUDIANTE'
        ||$user->getRolObj()->getNombre()=='EXTERNO')
        return $res->withStatus(401);
      $aula = new AulaModel();
      $data = $req->getParsedBody();
      if(!array_key_exists('fecha_inicio', $data)){
        return $res->withStatus(401);
      }
      $aula->setBorrado(false);
      $aula = $aula->getAll('borrado = 1 AND usuario_id = ? AND fecha_borrado >= ?',
       array($user->getId(), $data['fecha_inicio']));
        if($aula){
              return $res->withStatus(200)
                      ->write(json_encode(UserModel::getArrayObjects($aula)));
        }
        return $res->withStatus(401);
    });

    $this->post('recover/{id}', function ($req, $res, $args)use($app) {
        $aula = new AulaModel();
        $aula->setBorrado(false);
        $aula = $aula->get($args['id']);
        $user = $app->user;
        if($aula)
          if($aula->getUsuario_id() == $user->getId()){
            $aula->recover();
            return $res->withStatus(200)
                  ->write(json_encode($user->getArray()));
          }
        return $res->withStatus(401);
    });
    */

    
});