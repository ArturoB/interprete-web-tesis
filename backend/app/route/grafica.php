<?php

use App\Model\UserModel;
use App\Model\RolModel;
use App\Model\AulaModel;
use App\Model\ProyectoModel;

$app->group('/grafica/', function ()use($app) {
    
    $this->post('{semestre}/{tipo}', function ($req, $res, $args)use($app) {
      /* 
        0 Usuarios registrados
        1 Aulas creadas
        2 Proyectos creados
      */

      //primer dato siempre en la primera columna
      //cada data debe tener la cantidad de labels, para que queden ordenados por columnas
      $user = $app->user;
      if($user->getRolObj()->getNombre()!='ADMINISTRADOR')
        return $res->withStatus(401);
      $labels = array();
      $datasets = array();
      if($args['tipo'] == 0){
        $roles = (new RolModel())->getAll();
        array_push($labels, 'roles');
        foreach ($roles as $rol) {
          $data = array();
          array_push($data, count(
            (new UserModel())->getAll('rol_id = ? AND semestre_id = ?', 
              array($rol->getId(), $args['semestre']))
          ));
          array_push($datasets, array( 'label' => $rol->getNombre(),
                    'borderWidth' => 1,
                    'data' => $data,
                  )
          );
        }
      }else if($args['tipo'] == 1){
        $data = array();
        array_push($labels, 'Aulas');
        array_push($data, count(
          (new AulaModel())->getAll('semestre_id = ?', 
            array($args['semestre']))
        ));
        array_push($datasets, array( 'label' => 'Aulas',
                    'borderWidth' => 1,
                    'data' => $data,
                  )
        );
      }else if($args['tipo'] == 2){
        $data = array();
        array_push($labels, 'Proyectos');
        $aulas = (new AulaModel())->getAll('semestre_id = ?', 
            array($args['semestre']));
        $evaluados = 0;
        $noEvaluados = 0;
        foreach ($aulas as $aula) {
          $cantidad = count(
            (new ProyectoModel())->getAll('aula_id = ? AND valor > 0',
             array($aula->getId()))
          );
          if(is_numeric($cantidad))
            $evaluados += $cantidad;
          $cantidad = count(
            (new ProyectoModel())->getAll('aula_id = ? AND valor <= 0',
             array($aula->getId()))
          );
          if(is_numeric($cantidad))
            $noEvaluados += $cantidad;
        }
        array_push($data, $evaluados);
        array_push($datasets, array( 'label' => 'Evaluados',
                    'borderWidth' => 1,
                    'data' => $data,
                  )
        );
        $data = array();
        array_push($data, $noEvaluados);
        array_push($datasets, array( 'label' => 'No evaluados',
                    'borderWidth' => 1,
                    'data' => $data,
                  )
        );
      }

      $retorno = array(
        'labels' => $labels,
        'datasets' => $datasets,
      );
      return $res->withStatus(200)
                  ->write(json_encode($retorno));
    });

});

/*
[{
          label: 'Dataset 1',
          backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
         borderColor: ,
          borderWidth: 1,
          data: [
            1, 2, 3, 4, 5, 6, 7
          ]
        }]
*/