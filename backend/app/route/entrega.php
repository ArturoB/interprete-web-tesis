<?php

use App\Model\UserModel;
use App\Model\RolModel;
use App\Model\AulaModel;
use App\Model\ProyectoModel;
use App\Model\EntregaModel;
use App\Model\ParametroModel;

$app->group('/entrega/', function ()use($app) {
    
    $this->post('save/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      $entrega = new EntregaModel();
      $data = $req->getParsedBody();
      if(array_key_exists('id', $data)){
        $entrega = (new EntregaModel())->get($data['id']);
        if(!$entrega)
          $entrega = new EntregaModel();
      }
      else{
        $entrega->setProyecto_id($data['proyecto_id']);
        $entrega->setIp(getIp());

      }
      $estudiantes = $entrega->getProyectoObj()->getAulaObj()->getEstudiantesObj();
      $estudiante = null;
      foreach ($estudiantes as $estu) {
        if($estu->getUsuario_id() == $user->getId()){
          $estudiante = $estu;
          break;
        }
      }
      $proyecto = $entrega->getProyectoObj();
      if(!$proyecto)
        return $res->withStatus(401);
      if($proyecto->hasClave()){
        if(!array_key_exists('clave', $data))
          return $res->withStatus(401);
        if($proyecto->getClave() != sha1($data['clave']))
          return $res->withStatus(401);
      }
      $entrega->setCodigo($data['codigo']);
      $crcCodigo = sprintf("%u",crc32($data['codigo']));
      $entrega->setCrc($crcCodigo);
      $parametro = new ParametroModel();
      $parametro = $parametro->getAll('nombre = ?', 
        array('TAMAÑO_MAXIMO_SCRIPT_KB'))[0];
      //se divide entre 2 para promediar por ser UTF-8
      if(strlen($entrega->getCodigo()) > 1024*$parametro->getValor()/2)
        $entrega = null;
      if($entrega && $estudiante){
        $entrega->setUsuario_aula_id($estudiante->getId());
        $cantidad = count((new EntregaModel())->getAll('usuario_aula_id = ? AND proyecto_id = ?',
              array($estudiante->getId(), $entrega->getProyecto_id())
            ));
        $fechaI = date($entrega->getProyectoObj()->getFecha_inicio());
        $fechaF = date($entrega->getProyectoObj()->getFecha_fin());
        $fecha = date('Y-m-d H:i:s');
        $ipPermitida = $entrega->getProyectoObj()->isIpPermitida(getIp());
        //$ipPermitida = $entrega->getProyectoObj()->isIpPermitida('10.0.255.255');
        //$ipPermitida = true;
        if(($cantidad < 1 || array_key_exists('id', $data))
            && $fechaI <= $fecha && $fechaF >= $fecha && $ipPermitida)
          if($entrega->insertOrUpdate())
            return $res->withStatus(200)
                    ->write(json_encode($entrega->getArray()));
      }
        return $res->withStatus(401);
    });

    $this->post('evaluate/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      $data = $req->getParsedBody();
      if($user->getRolObj()->getNombre()=='ESTUDIANTE' || !array_key_exists('id', $data))
        return $res->withStatus(401);
      $entrega = (new EntregaModel())->get($data['id']);
      $entrega->setNota($data['nota']);
      $entrega->setComentarios($data['comentarios']);
        if($entrega)
          if($entrega->getProyectoObj()->getAulaObj()->getUsuario_id() == $user->getId()){
            if($entrega->insertOrUpdate())
              return $res->withStatus(200)
                      ->write(json_encode($entrega->getArray()));;
          }
        return $res->withStatus(401);
    });
    
    /*$this->post('delete/{id}', function ($req, $res, $args)use($app) {
        $proyecto = (new ProyectoModel())->get($args['id']);
        $user = $app->user;
        if($proyecto)
          if($proyecto->getAulaObj()->getUsuario_id() == $user->getId()){
            if($proyecto->delete())
              return $res->withStatus(200);
          }
        return $res->withStatus(401);
    });*/
    
});


function getIp(){
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    $ip = $_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}



/**
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}**////

/*


$_SERVER['REMOTE_ADDR'] may not actually contain real client IP addresses, as it will give you a proxy address for clients connected through a proxy, for example. That may well be what you really want, though, depending what your doing with the IPs. Someone's private RFC1918 address may not do you any good if you're say, trying to see where your traffic is originating from, or remembering what IP the user last connected from, where the public IP of the proxy or NAT gateway might be the more appropriate to store.

There are several HTTP headers like X-Forwarded-For which may or may not be set by various proxies. The problem is that those are merely HTTP headers which can be set by anyone. There's no guarantee about their content. $_SERVER['REMOTE_ADDR'] is the actual physical IP address that the web server received the connection from and that the response will be sent to. Anything else is just arbitrary and voluntary information. There's only one scenario in which you can trust this information: you are controlling the proxy that sets this header. Meaning only if you know 100% where and how the header was set should you heed it for anything of importance.

Having said that, here's some sample code:

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
Editor's note: Using the above code has security implications. The client can set all HTTP header information (ie. $_SERVER['HTTP_...) to any arbitrary value it wants. As such it's far more reliable to use $_SERVER['REMOTE_ADDR'], as this cannot be set by the user.
  **/



/*

1
down vote
Since you are dealing with ip addresses, it's probably easier to treat them as integers instead of strings. You can use the ip2long() function to do this.

Here's a working function to help you out:

function ipbetweenrange($needle, $start, $end) {
  if((ip2long($needle) >= ip2long($start)) && (ip2long($needle) <= ip2long($end))) {
    return true;
  }
  return false;
}

$ipstart = '70.42.249.1';
$ipend = '70.42.249.255';

echo ipbetweenrange('70.42.249.1', $ipstart, $ipend); // true
echo ipbetweenrange('70.42.249.255', $ipstart, $ipend); // true
echo ipbetweenrange('70.42.249.128', $ipstart, $ipend); // true
echo ipbetweenrange('192.168.0.1', $ipstart, $ipend); // false
*/


/*
Example
Check if the variable $ip is a valid IP address:

<?php
$ip = "127.0.0.1";

if (filter_var($ip, FILTER_VALIDATE_IP)) {
    echo("$ip is a valid IP address");
} else {
    echo("$ip is not a valid IP address");
}
?>
Definition and Usage
The FILTER_VALIDATE_IP filter validates an IP address.

Possible flags:

FILTER_FLAG_IPV4 - The value must be a valid IPv4 address
FILTER_FLAG_IPV6 - The value must be a valid IPv6 address
FILTER_FLAG_NO_PRIV_RANGE - The value must not be within a private range
FILTER_FLAG_NO_RES_RANGE - The value must not be within a reserved range
More Examples
Example 1
Check if the variable $ip is a valid IPv6 address:

<?php
$ip = "2001:0db8:85a3:08d3:1319:8a2e:0370:7334";

if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
    echo("$ip is a valid IPv6 address");
} else {
    echo("$ip is not a valid IPv6 address");
}
?>

*/



/*


3
down vote
This may help you:-
I have 4 input fields, each of its border was hided. Only the parent of the input field has the border, which will make it to look like a single text box. After that, allow the user to enter only 3 characters by using maxLength, once the user reached the maximum length, focus on to next field.
The code follows:- Link

HTML

<div>    
<input type="text" maxlength="3" class="onlythree" />.    
<input type="text" maxlength="3" class="onlythree" />.    
<input type="text" maxlength="3" class="onlythree" />.    
<input type="text" maxlength="3" class="onlythree" />    
</div> 
CSS

.onlythree{    
width: 50px;    
border: 0;    
}    
div{    
border: 1px solid gray;    
display: inline-block;    
}  
JS

$(".onlythree").keyup(function () {    
if (this.value.length == this.maxLength) {    
$(this).next('.onlythree').focus();    
}    
});  
*/




/**







strlen(cad) <= 1024*100
para menor a 100 kb









**/



/**

$app->get('/forgotpassword/:id', function($id) use ($app) {
    $param = "secret-password-reset-code";

    $mail = new PHPMailer;

    $mail->setFrom('admin@badger-dating.com', 'BadgerDating.com');
    $mail->addAddress($id);
    $mail->addReplyTo('no-reply@badger-dating.com', 'BadgerDating.com');

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = 'Instructions for resetting the password for your account with BadgerDating.com';
    $mail->Body    = "
        <p>Hi,</p>
        <p>            
        Thanks for choosing BadgerDating.com!  We have received a request for a password reset on the account associated with this email address.
        </p>
        <p>
        To confirm and reset your password, please click <a href=\"http://badger-dating.com/resetpassword/$id/$param\">here</a>.  If you did not initiate this request,
        please disregard this message.
        </p>
        <p>
        If you have any questions about this email, you may contact us at support@badger-dating.com.
        </p>
        <p>
        With regards,
        <br>
        The BadgerDating.com Team
        </p>";

    if(!$mail->send()) {
        $app->flash("error", "We're having trouble with our mail servers at the moment.  Please try again later, or contact us directly by phone.");
        error_log('Mailer Error: ' . $mail->errorMessage());
        $app->halt(500);
    }     
}

*/