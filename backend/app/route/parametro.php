<?php

use App\Model\UserModel;
use App\Model\RolModel;
use App\Model\ParametroModel;
use App\Model\SemestreModel;

$app->group('/parametro/', function ()use($app) {
    
    $this->post('update/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      $data = $req->getParsedBody();
      if($user->getRolObj()->getNombre()!='ADMINISTRADOR' 
        || !array_key_exists('id', $data) 
        || !array_key_exists('valor', $data))
        return $res->withStatus(401);
      $parametro = new ParametroModel();
      $parametro = $parametro->get($data['id']);
      if($parametro->getNombre()=='SEMESTRE_EN_CURSO'){
        $semestre = (new SemestreModel())->get($data['valor']);
        if(!$semestre){
          return $res->withStatus(401);
        }

      }
      $parametro->setValor($data['valor']);
        if($parametro)
            if($parametro->insertOrUpdate()){
              $parametros = $parametro->getAll(' not nombre = ?', array('PERMITIR_REGISTRO_EXTERNO'));
              return $res->withStatus(200)
                      ->write(json_encode(UserModel::getArrayObjects($parametros)));
            }
        return $res->withStatus(401);
    });
    
});