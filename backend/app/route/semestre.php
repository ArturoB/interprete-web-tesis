<?php

use App\Model\UserModel;
use App\Model\SemestreModel;

$app->group('/semestre/', function ()use($app) {
    
    $this->post('getall', function ($req, $res, $args) {

      $semestre = new SemestreModel();
      $semestre = $semestre->getAll();
        return $res->getBody()
                   ->write(json_encode(UserModel::getArrayObjects($semestre)));
    });

    $this->post('save/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      if($user->getRolObj()->getNombre()!='ADMINISTRADOR')
        return $res->withStatus(401);
      $semestre = new SemestreModel();
      $data = $req->getParsedBody();
      if(array_key_exists('id', $data)){
        $semestre = (new SemestreModel())->get($data['id']);
        if(!$semestre)
          $semestre = new SemestreModel();
      }

      $semestre->setNombre($data['nombre']);
        if($semestre)
            if($semestre->insertOrUpdate())
              return $res->withStatus(200)
                      ->write(json_encode($semestre->getArray()));
        return $res->withStatus(401);
    });
    
    $this->post('delete/{id}', function ($req, $res, $args)use($app) {
        $semestre = (new SemestreModel())->get($args['id']);
        $user = $app->user;
        if($user->getRolObj()->getNombre()!='ADMINISTRADOR')
          return $res->withStatus(401);
        if($semestre)
            if($semestre->delete())
              return $res->withStatus(200);
        return $res->withStatus(401);
    });
    
});