<?php

use App\Model\UserModel;
use App\Model\RolModel;
use App\Model\ScriptModel;
use App\Model\ParametroModel;

$app->group('/script/', function ()use($app) {

    $this->post('getall/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      $scripts = (new ScriptModel())->getAll('usuario_id = ?', array($user->getId()));
      
        if($scripts)
          return $res->withStatus(200)
                  ->write(json_encode(UserModel::getArrayObjects($scripts)));
        return $res->withStatus(200)
                  ->write(json_encode(array()));
    });
    
    $this->post('save/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      $script = new ScriptModel();
      $data = $req->getParsedBody();
      if(array_key_exists('id', $data)){
        $script = (new ScriptModel())->get($data['id']);
        if(!$script)
          $script = new ScriptModel();
      }
      else{
        $script->setUsuario_id($user->getId());
      }
      $script->fromArray($data);
      if(!array_key_exists('id', $data)){
        $cantidad = count((new ScriptModel())->getAll('usuario_id = ?', array($user->getId())));
        $parametro = new ParametroModel();
        $parametro = $parametro->getAll('nombre = ?', 
            array('CANTIDAD_MAXIMA_SCRIPTS'))[0];
        if($cantidad >= $parametro->getValor())
          $script = null;
      }
      $parametro = new ParametroModel();
      $parametro = $parametro->getAll('nombre = ?', 
        array('TAMAÑO_MAXIMO_SCRIPT_KB'))[0];
      //se divide entre 2 para promediar por ser UTF-8
      if(strlen($script->getCodigo()) > 1024*$parametro->getValor()/2)
        $script = null;
        if($script)
          if($script->getUsuario_id() == $user->getId()){
            if($script->insertOrUpdate())
              return $res->withStatus(200)
                      ->write(json_encode($script->getArray()));;
          }
        return $res->withStatus(401);
    });
    
    $this->post('delete/{id}', function ($req, $res, $args)use($app) {
        $script = (new ScriptModel())->get($args['id']);
        $user = $app->user;
        if($script)
          if($script->getUsuario_id() == $user->getId()){
            if($script->delete())
              return $res->withStatus(200);
          }
        return $res->withStatus(401);
    });
    
});