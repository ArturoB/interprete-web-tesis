<?php

use App\Model\UserModel;
use App\Model\RolModel;
use App\Model\AulaModel;
use App\Model\ProyectoModel;

$app->group('/proyecto/', function ()use($app) {
    
    $this->post('save/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      if($user->getRolObj()->getNombre()=='ESTUDIANTE'||
          $user->getRolObj()->getNombre()=='EXTERNO')
        return $res->withStatus(401);
      $proyecto = new ProyectoModel();
      $data = $req->getParsedBody();
      $aula = (new AulaModel())->get($data['aula_id']);
      if(array_key_exists('id', $data)){
        $proyecto = (new ProyectoModel())->get($data['id']);
        if(!$proyecto)
          $proyecto = new ProyectoModel();
      }
      else{
        $proyecto->setAula_id($aula->getId());
      }

      $proyecto->fromArray($data);
      if(strlen($data['clave'])>0){
        $proyecto->setClave(sha1($data['clave']));
      }
        if($proyecto)
          if($aula->getUsuario_id() == $user->getId()){
            if($proyecto->insertOrUpdate())
              return $res->withStatus(200)
                      ->write(json_encode($proyecto->getArray()));;
          }
        return $res->withStatus(401);
    });
    
    $this->post('delete/{id}', function ($req, $res, $args)use($app) {
        $proyecto = (new ProyectoModel())->get($args['id']);
        $user = $app->user;
        if($proyecto)
          if($proyecto->getAulaObj()->getUsuario_id() == $user->getId()){
            if($proyecto->delete())
              return $res->withStatus(200);
          }
        return $res->withStatus(401);
    });

    $this->post('get_by_project/{idProject}', function ($req, $res, $args)use($app) {
        $proyecto = (new ProyectoModel())->get($args['idProject']);
        $user = $app->user;
        if($proyecto)
          if($proyecto->getAulaObj()->getUsuario_id() == $user->getId()){
            return $res->withStatus(200)
                  ->write(json_encode($proyecto->getArrayO()));
          }
        return $res->withStatus(401);
    });

    $this->post('searchInTrash/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      if($user->getRolObj()->getNombre()=='ESTUDIANTE'
        ||$user->getRolObj()->getNombre()=='EXTERNO')
        return $res->withStatus(401);
      $proyecto = new ProyectoModel();
      $data = $req->getParsedBody();
      if(!array_key_exists('fecha_inicio', $data) || !array_key_exists('idAula', $data)){
        return $res->withStatus(401);
      }
      $aula = new AulaModel();
      $aula = $aula->get($data['idAula']);
      if(!$aula)
        return $res->withStatus(401);
      if($aula->getUsuario_id() != $user->getId())
        return $res->withStatus(401);
      $proyecto->setBorrado(false);
      $proyecto = $proyecto->getAll('borrado = 1 AND aula_id = ? AND fecha_borrado >= ?',
       array($aula->getId(), $data['fecha_inicio']));
        if($proyecto){
              return $res->withStatus(200)
                      ->write(json_encode(UserModel::getArrayObjects($proyecto)));
        }
        return $res->withStatus(401);
    });

    $this->post('recover/{id}', function ($req, $res, $args)use($app) {
        $proyecto = new ProyectoModel();
        $proyecto->setBorrado(false);
        $proyecto = $proyecto->get($args['id']);
        $user = $app->user;
        if($proyecto)
          if($proyecto->getAulaObj()->getUsuario_id() == $user->getId()){
            $proyecto->recover();
            return $res->withStatus(200)
                  ->write(json_encode($user->getArray()));
          }
        return $res->withStatus(401);
    });
    
});