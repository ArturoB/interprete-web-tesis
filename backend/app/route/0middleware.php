<?php

namespace App\Route;
use App\Model\UserModel;


class Middleware
{
    private $app = null;


    public function __construct($app) {
        $this->app = $app;
    }

    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        if($request->getUri()->getPath() != 'user/login' 
            && !isset($_SESSION['user'])
            && $request->getUri()->getPath() != 'user/logout'
            && $request->getUri()->getPath() != 'user/save'
            && $request->getUri()->getPath() != 'user/saveE'
            && $request->getUri()->getPath() != 'semestre/getall'
            && $request->getUri()->getPath() != 'carrera/getall'
            && $request->getUri()->getPath() != 'user/forgotpassword'
            && $request->getUri()->getPath() != 'user/resetpassword')
            return $response->withStatus(401);

        if(isset($_SESSION['user'])){
            $user = (new UserModel())->get($_SESSION['user']);
            $this->app->user = $user;
        }
        $response = $next($request, $response);
        return $response;
    }
}