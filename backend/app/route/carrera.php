<?php

use App\Model\UserModel;
use App\Model\CarreraModel;

$app->group('/carrera/', function ()use($app) {
    
    $this->post('getall', function ($req, $res, $args) {

      $carrera = new CarreraModel();
      $carrera = $carrera->getAll();
        return $res->getBody()
                   ->write(json_encode(UserModel::getArrayObjects($carrera)));
    });

    $this->post('save/', function ($req, $res, $args)use($app) {
      $user = $app->user;
      if($user->getRolObj()->getNombre()!='ADMINISTRADOR')
        return $res->withStatus(401);
      $carrera = new CarreraModel();
      $data = $req->getParsedBody();
      if(array_key_exists('id', $data)){
        $carrera = (new CarreraModel())->get($data['id']);
        if(!$carrera)
          $carrera = new CarreraModel();
      }

      $carrera->setNombre($data['nombre']);
        if($carrera)
            if($carrera->insertOrUpdate())
              return $res->withStatus(200)
                      ->write(json_encode($carrera->getArray()));
        return $res->withStatus(401);
    });
    
    $this->post('delete/{id}', function ($req, $res, $args)use($app) {
        $carrera = (new CarreraModel())->get($args['id']);
        $user = $app->user;
        if($user->getRolObj()->getNombre()!='ADMINISTRADOR')
          return $res->withStatus(401);
        if($carrera)
            if($carrera->delete())
              return $res->withStatus(200);
        return $res->withStatus(401);
    });
    
});