<?php
namespace App\Lib;

use PDO;

class Database
{
	public static $db = null;
	public static $dbName = 'interprete_db';
	public static $dbHost = 'localhost';
	public static $dbUser = 'root';
	public static $dbUserPass = '';

    public static function StartUp()
    {

    	if(self::$db)
    		return self::$db;

        $pdo = new PDO('mysql:host='.self::$dbHost.';dbname='.self::$dbName.';charset=utf8', self::$dbUser, self::$dbUserPass);
        
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        //$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        
        self::$db = $pdo;
        
        return $pdo;
    }
}