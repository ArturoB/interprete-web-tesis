/**
Información de arbol en cuanto a que hay en cada índice
1-scripts
2-aulas
  21+posAula-estudiantes
  22+posAula-proyectos
    22+'-'+1+posAula-proyectos no evaluados
    22+'-'+2+posAula-proyectos evaluados
  23+posAula+'-'+posProyecto-Modificar Entregas
  24+semestreId- Semestre donde hay aulas
  25 buscar aula y recuperar
  26+posAula buscar proyecto y recuperar
3-Aministración
  31-Estudiantes
  32-Semestres
  33-Sistema
  34-Estadísticas
**/
function Usuario(datos = null, editores = null){
  this.alias;
  this.scripts;
  this.scriptsCargados = false;
  this.id;
  this.rol;
  this.aulas;
  this.misAulas;
  this.roles;
  this.parametros;
  this.semestre; //para semestre en curso
  this.tree = 'esasyui_tree';
  this.editores = editores;
  this.iniciar(datos);

}


Usuario.prototype.iniciar = function(datos){
  var self = this;
  if(datos){
    this.id =  datos['id'];
    this.alias = datos['alias'];
    this.scripts =  datos['scripts'];
    this.rol =  datos['rol'];
    this.aulas =  datos['aulas'];
    this.misAulas =  datos['misAulas'];
    this.roles =  datos['roles'];
    this.parametros =  datos['parametros'];
    this.semestre =  datos['semestre'];
    this.setSelectRol();
    this.setParametrosModal();

  }
  $('#semestreEnCurso').html('Semestre '+this.semestre.nombre);
  $('#'+this.tree).tree({
    data: this.getTree()
  });
  $('#'+this.tree).tree({
    onClick: function(node){
      if(node.attributes){
        if(node.attributes.tipo == 'script'){
          editores.nuevoTab(self.scripts[node.attributes.pos]['nombre'],
          self.scripts[node.attributes.pos]['codigo'],
          self.scripts[node.attributes.pos]['id']);
        }
        else if(node.attributes.tipo == 'usuarios-buscar'){
          $('#buscarUsuarioModal').modal('show');
        }
        else if(node.attributes.tipo == 'aula-estudiante-buscar'){
          $('#buscarUsuarioAModal').data('id', node.attributes.idAula);
          $('#buscarUsuarioAModal').data('pos', node.attributes.posAula);
          $('#buscarBuscarUsuarioAModal').val('');
          $('#buscarUsuarioAModal').modal('show');
        }
        else if(node.attributes.tipo == 'parametros'){
          self.setParametrosModal();
          $('#parametrosModal').modal('show');
        }
        else if(node.attributes.tipo == 'estadisticas'){
          $('#semestreEstadisticasModal').html(self.setSelectSemestres(-1));
          $('#estadisticasModal').modal('show');
        }
        else if(node.attributes.tipo == 'usuarios-crear'){
          $("#nombreRegistroAModal").val('');
          $("#correoRegistroAModal").val('');
          $("#cedulaRegistroAModal").val('');
          $("#claveRegistroAModal").val('');
          $("#clave1RegistroAModal").val('');
          $("#registroAModal").data('id', -1);
          $('#registroAModal').modal('show');
        }
        else if(node.attributes.tipo == 'aula-proyecto-estudiante'){
            $('#nombreNuevoProyectoModal').prop('disabled',true);
            $('#valorNuevoProyectoModal').prop('disabled',true);
            $('#enunciadoNuevoProyectoModal').prop('disabled',true);
            $('#fechaInicioNuevoProyectoModal').prop('disabled',true);
            $('#fechaFinNuevoProyectoModal').prop('disabled',true);
            $('#claveNuevoProyectoModal').hide();
            $('#clave1NuevoProyectoModal').hide();
            $('#nuevoProyectoModalForm  button').hide();
            
            $('#'+self.tree+'_menu_proyecto_padre').data('aulaid', node.attributes.idAula);
            var proyecto = self.getProyectoPosForId(node.attributes.idProyecto,
                                               node.attributes.posAula, self.misAulas);
            var aula = node.attributes.posAula;
            var fechaI = new Date(self.misAulas[aula].proyectos[proyecto].fecha_inicio);
            fechaI = fechaI.format('yyyy-mm-dd')+'T'+fechaI.format('HH:MM');
            var fechaF = new Date(self.misAulas[aula].proyectos[proyecto].fecha_fin);
            fechaF = fechaF.format('yyyy-mm-dd')+'T'+fechaF.format('HH:MM');
            $('#nuevoProyectoModal').data('proyecto', node.attributes.idProyecto);
            $('#nombreNuevoProyectoModal').val(self.misAulas[aula].proyectos[proyecto].nombre);
            $('#valorNuevoProyectoModal').val(self.misAulas[aula].proyectos[proyecto].valor);
            $('#enunciadoNuevoProyectoModal').val(self.misAulas[aula].proyectos[proyecto].enunciado);
            $('#fechaInicioNuevoProyectoModal').val(fechaI);
            $('#fechaFinNuevoProyectoModal').val(fechaF);
            $(".ipContainer").hide();
            $('#divValidarIpNuevoProyectoModal').hide();
            $('#nuevoProyectoModal').modal('show');
          }
          else if(node.attributes.tipo == 'estudiante-aula-buscar'){//rol estudiante
            $('#buscarAulaModal').modal('show');
            $('#codigoBuscarAulaModal').val('');
            $('#claveBuscarAulaModal').val('');
          }
          else if(node.attributes.tipo == 'aula-recuperar'){//rol docente
            $('#recuperarAulaModalBody').html('');
            var fechaI = new Date();
            fechaI.setDate(fechaI.getDate()-7);
            fechaI = fechaI.format('yyyy-mm-dd')+'T'+fechaI.format('HH:MM');
            $('#fechaInicioRecuperarAulaModal').val(fechaI);
            $('#recuperarAulaModal').modal('show');
          }
          else if(node.attributes.tipo == 'proyecto-recuperar'){//rol docente
            $('#recuperarProyectoModalBody').html('');
            var fechaI = new Date();
            fechaI.setDate(fechaI.getDate()-7);
            fechaI = fechaI.format('yyyy-mm-dd')+'T'+fechaI.format('HH:MM');
            $('#fechaInicioRecuperarProyectoModal').val(fechaI);
            $('#recuperarProyectoModal').data('idAula', node.attributes.idAula);
            $('#recuperarProyectoModal').modal('show');
          }
      }
    },
    onExpand: function(node){
      if(node.attributes){
        if(node.attributes.tipo == 'scripts' && !self.scriptsCargados){
          var nodeE = $(this).tree('find','scripts');
            $(this).tree('remove', nodeE.target);
          self.loadScripts();
        }else if(node.attributes.tipo == 'aula'){
          if(node.attributes.cargada)
            return;
          node.attributes.cargada = true;
          self.loadAula(node.attributes.id);
        }else if(node.attributes.tipo == 'aula-proyecto-padre'){
          var nodeE = $(this).tree('find','proyecto'+node.attributes.idAula);
          if(nodeE)
            $(this).tree('remove', nodeE.target);
        }else if(node.attributes.tipo == 'aula-proyecto'){
          if(node.attributes.cargada)
            return;
          node.attributes.cargada = true;
          self.loadProyecto(node.attributes.idProyecto, node.attributes.idAula);
          var nodeE = $(this).tree('find','entrega'+node.attributes.idAula+
                ''+node.attributes.idProyecto);
          if(nodeE){
            $(this).tree('remove', nodeE.target);
            var pos = self.getAulaPosForId(node.attributes.idAula);
            nodeE = $(this).tree('find',22+''+pos);
            if(nodeE){
              if(nodeE.children.length <= 1){
                $(this).tree('update', {
                  target: nodeE.target,
                  state:'closed'
                });
                //alert();
              }
            }
          } 
        }
      }
    }

  });
  
  $('#'+this.tree).tree({
    onContextMenu: function(e, node){
          e.preventDefault();
          $(this).tree('select',node.target);
          if(!node.attributes)
            return;
          if(node.attributes.tipo == 'script'){
            $('#'+self.tree+'_menu_script').menu('show',{
              left: e.pageX,
              top: e.pageY
            });
          }
          else if(node.attributes.tipo == 'semestres'){
             $('#nuevoSemestreModal').data('semestre', -1);
             $('#nombreNuevoSemestreModal').val('');
             $('#'+self.tree+'_menu_semestres').data('pos', -1);
            $('#'+self.tree+'_menu_semestres').menu('show',{
              left: e.pageX,
              top: e.pageY
            });
          }
          else if(node.attributes.tipo == 'semestre'){
             $('#nuevoSemestreModal').data('semestre', node.attributes.id);
             $('#nombreNuevoSemestreModal').val(Usuario.semestres[node.attributes.pos].nombre);
             $('#'+self.tree+'_menu_semestres').data('pos', node.attributes.pos);
            $('#'+self.tree+'_menu_semestre').menu('show',{
              left: e.pageX,
              top: e.pageY
            });
          }
          else if(node.attributes.tipo == 'aula'){
             $('#nuevaAulaModal').data('aula', self.aulas[node.attributes.pos].id);
             $('#claveNuevaAulaModal').val('');
             $('#claveNuevaAulaModal1').val('');
             $('#nombreNuevaAulaModal').val(self.aulas[node.attributes.pos].nombre);
             $('#divCodigoNuevaAulaModal').show();
             $('#codigoNuevaAulaModal').html(self.aulas[node.attributes.pos].codigo);
            $('#'+self.tree+'_menu_aula').menu('show',{
              left: e.pageX,
              top: e.pageY
            });
          }
          else if(node.attributes.tipo == 'aula-padre'){
             $('#nombreNuevaAulaModal').val('');
             $('#claveNuevaAulaModal').val('');
             $('#claveNuevaAulaModal1').val('');
             $('#divCodigoNuevaAulaModal').hide();
            $('#nuevaAulaModal').data('aula', -1);
            $('#'+self.tree+'_menu_aula_padre').menu('show',{
              left: e.pageX,
              top: e.pageY
            });
          }
          else if(node.attributes.tipo == 'aula-estudiante'){
            $('#'+self.tree+'_menu_aula_estudiante').menu('show',{
              left: e.pageX,
              top: e.pageY
            });
          }
          else if(node.attributes.tipo == 'estudiante-aula'){//rol estudiante
            $('#'+self.tree+'_menu_aula_estudiante_salir').menu('show',{
              left: e.pageX,
              top: e.pageY
            });
          }
          else if(node.attributes.tipo == 'aula-proyecto-padre'){
            $('#nombreNuevoProyectoModal').prop('disabled',false);
            $('#valorNuevoProyectoModal').prop('disabled',false);
            $('#enunciadoNuevoProyectoModal').prop('disabled',false);
            $('#fechaInicioNuevoProyectoModal').prop('disabled',false);
            $('#fechaFinNuevoProyectoModal').prop('disabled',false);
            $('#nuevoProyectoModalForm button').show();
            $('#claveNuevoProyectoModal').show();
            $('#clave1NuevoProyectoModal').show();
            var fechaI = new Date();
            fechaI = fechaI.format('yyyy-mm-dd')+'T'+fechaI.format('HH:MM');
            var fechaF = new Date();
            fechaF = fechaF.format('yyyy-mm-dd')+'T'+fechaF.format('HH:MM');
            $('#nuevoProyectoModal').data('proyecto', -1);
            $('#nombreNuevoProyectoModal').val('');
            $('#valorNuevoProyectoModal').val(0);
            $('#enunciadoNuevoProyectoModal').val('');
            $('#fechaInicioNuevoProyectoModal').val(fechaI);
            $('#fechaFinNuevoProyectoModal').val(fechaF);
            setIpInModal('0.0.0.0', '255.255.255.255');
            $(".ipContainer").hide();
            $('#validarIpNuevoProyectoModal').prop('checked', false);
            $('#divValidarIpNuevoProyectoModal').show();
            $('#'+self.tree+'_menu_proyecto_padre').data('aulaid', node.attributes.idAula);
            $('#'+self.tree+'_menu_proyecto_padre').menu('show',{
              left: e.pageX,
              top: e.pageY
            });
          }
          else if(node.attributes.tipo == 'aula-proyecto'){
            $('#nombreNuevoProyectoModal').prop('disabled',false);
            $('#valorNuevoProyectoModal').prop('disabled',false);
            $('#enunciadoNuevoProyectoModal').prop('disabled',false);
            $('#fechaInicioNuevoProyectoModal').prop('disabled',false);
            $('#fechaFinNuevoProyectoModal').prop('disabled',false);
            $('#claveNuevoProyectoModal').show();
            $('#clave1NuevoProyectoModal').show();
            $('#nuevoProyectoModalForm  button').show();
            $('#'+self.tree+'_menu_proyecto_padre').data('aulaid', node.attributes.idAula);
            var proyecto = self.getProyectoPosForId(node.attributes.idProyecto,
                                               node.attributes.posAula);
            var aula = node.attributes.posAula;
            var fechaI = new Date(self.aulas[aula].proyectos[proyecto].fecha_inicio);
            fechaI = fechaI.format('yyyy-mm-dd')+'T'+fechaI.format('HH:MM');
            var fechaF = new Date(self.aulas[aula].proyectos[proyecto].fecha_fin);
            fechaF = fechaF.format('yyyy-mm-dd')+'T'+fechaF.format('HH:MM');
            $('#nuevoProyectoModal').data('proyecto', node.attributes.idProyecto);
            $('#nombreNuevoProyectoModal').val(self.aulas[aula].proyectos[proyecto].nombre);
            $('#valorNuevoProyectoModal').val(self.aulas[aula].proyectos[proyecto].valor);
            $('#enunciadoNuevoProyectoModal').val(self.aulas[aula].proyectos[proyecto].enunciado);
            $('#fechaInicioNuevoProyectoModal').val(fechaI);
            $('#fechaFinNuevoProyectoModal').val(fechaF);
            setIpInModal(self.aulas[aula].proyectos[proyecto].ip_rango_inicial,
              self.aulas[aula].proyectos[proyecto].ip_rango_final);
            $(".ipContainer").hide();
            $('#divValidarIpNuevoProyectoModal').show();
            $('#validarIpNuevoProyectoModal').prop('checked', false);
            if(self.aulas[aula].proyectos[proyecto].validacion_ip != 0){
            $(".ipContainer").show();
            $('#validarIpNuevoProyectoModal').prop('checked', true);
            }
            else{
              setIpInModal('0.0.0.0', '255.255.255.255');
            }
            $('#'+self.tree+'_menu_proyecto_hijo').menu('show',{
              left: e.pageX,
              top: e.pageY
            });
          }
          else if(node.attributes.tipo == 'aula-proyecto-entrega'){
            $('#'+self.tree+'_menu_entrega_evaluar').data('posaula', node.attributes.posAula);
            $('#'+self.tree+'_menu_entrega_evaluar').data('posproyecto', node.attributes.posProyecto);
            $('#'+self.tree+'_menu_entrega_evaluar').data('pos', node.attributes.pos);
            $('#notaNuevaEntregaModal').prop('disabled',false);
            $('#codigoNuevaEntregaModal').prop('disabled',true);
            $('#comentariosNuevaEntregaModal').prop('disabled',false);
            $('#crcsNuevaEntregaModal').show();
            $('#copiarNuevaEntregaModal').hide();
            $('#nuevaEntregaModal').data('tipo', 0); //evaluar entrega profesor
            $('#nuevaEntregaModal').data('id', node.attributes.id);
            $('#nuevaEntregaModal').data('proyectoid', node.attributes.idProyecto);
            var proyecto = self.getProyectoPosForId(node.attributes.idProyecto,
                                               node.attributes.posAula);
            var aula = node.attributes.posAula;
            var proyectoP = node.attributes.posProyecto;
            var pos = node.attributes.pos;
            $('#notaNuevaEntregaModal').val(self.aulas[aula].proyectos[proyectoP].entregas[pos].nota);
            $('#codigoNuevaEntregaModal').val(self.aulas[aula].proyectos[proyectoP].entregas[pos].codigo);
            $('#comentariosNuevaEntregaModal').val(
              self.aulas[aula].proyectos[proyectoP].entregas[pos].comentarios);
            $('#crc1NuevaEntregaModal').val(self.aulas[aula].proyectos[proyectoP].entregas[pos].crc);
            $('#crcNuevaEntregaModal').val(null);
            $('#'+self.tree+'_menu_entrega_evaluar').menu('show',{
              left: e.pageX,
              top: e.pageY
            });
          }
          else if(node.attributes.tipo == 'aula-proyecto-estudiante'){
            $('#notaNuevaEntregaModal').prop('disabled',true);
            $('#codigoNuevaEntregaModal').prop('disabled',false);
            $('#comentariosNuevaEntregaModal').prop('disabled',true);
            $('#crcsNuevaEntregaModal').hide();
            $('#copiarNuevaEntregaModal').show();
            $('#nuevaEntregaModal').data('id', -1);
            $('#nuevaEntregaModal').data('proyectoid', node.attributes.idProyecto);
            $('#'+self.tree+'_menu_entrega_entregar').data('posaula', node.attributes.posAula);
            $('#'+self.tree+'_menu_entrega_entregar').data('posproyecto', node.attributes.posProyecto);
            $('#notaNuevaEntregaModal').val('');
            $('#codigoNuevaEntregaModal').val('');
            $('#comentariosNuevaEntregaModal').val('');
            var aula = node.attributes.posAula;
            var proyectoP = node.attributes.posProyecto;
            if(self.misAulas[aula].proyectos[proyectoP].entregas.length < 1)
              $('#'+self.tree+'_menu_entrega_entregar').menu('show',{
                left: e.pageX,
                top: e.pageY
              });
          }
          else if(node.attributes.tipo == 'aula-proyecto-entrega-estudiante'){
            $('#notaNuevaEntregaModal').prop('disabled',true);
            $('#codigoNuevaEntregaModal').prop('disabled',false);
            $('#comentariosNuevaEntregaModal').prop('disabled',true);
            $('#crcsNuevaEntregaModal').hide();
            $('#copiarNuevaEntregaModal').show();
            $('#nuevaEntregaModal').data('tipo', 1); //modificar entrega usuario
            $('#nuevaEntregaModal').data('id', node.attributes.id);
            $('#'+self.tree+'_menu_entrega_entregar').data('pos', node.attributes.pos);
            $('#nuevaEntregaModal').data('proyectoid', node.attributes.idProyecto);
            $('#'+self.tree+'_menu_entrega_entregar').data('posaula', node.attributes.posAula);
            $('#'+self.tree+'_menu_entrega_entregar').data('posproyecto', node.attributes.posProyecto);
            var aula = node.attributes.posAula;
            var proyectoP = node.attributes.posProyecto;
            var pos = node.attributes.pos;
            $('#notaNuevaEntregaModal').val(self.misAulas[aula].proyectos[proyectoP].entregas[pos].nota);
            $('#codigoNuevaEntregaModal').val(
              self.misAulas[aula].proyectos[proyectoP].entregas[pos].codigo);
            $('#comentariosNuevaEntregaModal').val(
              self.misAulas[aula].proyectos[proyectoP].entregas[pos].comentarios);
              $('#'+self.tree+'_menu_entrega_modificar').menu('show',{
                left: e.pageX,
                top: e.pageY
              });
          }
    }
  });


}


Usuario.prototype.setSelectRol = function(){
  var select = '';
  for (var i = 0; i < this.roles.length; i++) {
    select += '<option value="'+this.roles[i].id+'">'+this.roles[i].nombre+'</option>';
  }
  $('#rolRegistroAModal').html(select);
}

Usuario.prototype.setParametrosModal = function(){
  var select = '';
  for (var i = 0; i < this.parametros.length; i++) {
    if(this.parametros[i].nombre=='SEMESTRE_EN_CURSO'){
      select += '<div class="form-group">'
                +'<label class="form-check-label">'
                +this.parametros[i].nombre+'</label>'
                +'<select id="'+this.parametros[i].nombre+'" value="'+this.parametros[i].valor
                +'" class="form-control" data-id="'
                +this.parametros[i].id+'">'
                +this.setSelectSemestres(this.parametros[i].valor)+'</select>'
                '</div>';
    }else {
      select += '<div class="form-group">'
                +'<label class="form-check-label">'
                +this.parametros[i].nombre+'</label>'
                +'<input id="'+this.parametros[i].nombre
                +'" type="number" value="'+this.parametros[i].valor+'" class="form-control" data-id="'
                +this.parametros[i].id+'">'
                '</div>';
    }
  }
  $('#parametrosModalBody').html(select);
}

Usuario.prototype.setSelectSemestres = function(valor){
  var select = '';
  for (var i = 0; i < Usuario.semestres.length; i++) {
    if(Usuario.semestres[i].id == valor){
      select += '<option selected value="'
            +Usuario.semestres[i].id+'">'+Usuario.semestres[i].nombre+'</option>';
    }
    else
      select += '<option value="'+Usuario.semestres[i].id+'">'+Usuario.semestres[i].nombre+'</option>';
  }
  return select;
}


//Multiuso (ya no solo sripts)
Usuario.prototype.eliminarScript = function(){
  var node = $('#'+this.tree).tree('getSelected');
  var resp = confirm(this.getDeleteMessage(node.attributes.tipo));
  if(!resp)
    return;
  if(node.attributes.tipo == 'script'){
    this.editores.setIdNull(this.scripts[node.attributes.pos]['id']);
    this.llamadaAjax('script/delete/'+this.scripts[node.attributes.pos]['id']);
  }else if(node.attributes.tipo == 'aula'){
    var aux = prompt('Ingrese la palabra aula :','');
    if(aux != 'aula')
      return;
    this.llamadaAjax('aula/delete/'+this.aulas[node.attributes.pos]['id']);
  }else if(node.attributes.tipo == 'aula-proyecto'){
    var aux = prompt('Ingrese la palabra proyecto :','');
    if(aux != 'proyecto')
      return;
    this.llamadaAjax('proyecto/delete/'+
      this.aulas[node.attributes.posAula].proyectos[node.attributes.pos]['id']);
  }else if(node.attributes.tipo == 'semestre'){
    this.llamadaAjax('semestre/delete/'+node.attributes.id);
  }else if(node.attributes.tipo == 'aula-estudiante'){
    this.llamadaAjax('usuario_aula/delete/'+node.attributes.id);
  }else if(node.attributes.tipo == 'estudiante-aula'){
    this.llamadaAjax('usuario_aula/back/'+node.attributes.idAula);
  }
  $('#'+this.tree).tree('remove', node.target);
}

Usuario.prototype.getDeleteMessage = function(tipo){
  switch(tipo){
    case 'script':
      return '¿Borrar este script?';
    case 'aula':
      return '¿Borrar esta aula?';
    case 'aula-proyecto':
      return '¿Borrar este proyecto?';
    case 'semestre':
      return '¿Borrar este semestre?';
    case 'aula-estudiante':
      return '¿Remover este estudiante del aula?';
    case 'estudiante-aula':
      return '¿Salir del aula?';
  }
  return '¿Borrar esto?';
}



/**
  Añade un script.
  data: vector de array de objeto a añadir. 
  Ejemplo [{text:'titulo'}]
*/
Usuario.prototype.appendScript = function(data, pos = 1){
  var node = $('#'+this.tree).tree('find',pos);
  $('#'+this.tree).tree('append', {
        parent: (node?node.target:null),
        data: data
      });
}

/*
  Actualiza el título del nodo
*/
Usuario.prototype.updateNode = function(text, toFind = null){
  var node = $('#'+this.tree).tree('getSelected');
  if(toFind){
    node = $('#'+this.tree).tree('find',toFind);
  }
  if (node){
    $('#'+this.tree).tree('update', {
      target: node.target,
      text: text
    });
  }
}




//Scripts
Usuario.prototype.saveScript = function(){
  var self = this;
  /**
  A enviar
  id, nombre, codigo, usuario_id
  */
  var datos = {};
  var editor = this.editores.getEditorActual();
  datos.codigo = editor.editor.getValue();
  datos.nombre = editor.nombre;
  if(editor.id){
    datos.id = editor.id;
  }

  this.llamadaAjax('script/save/', datos, function(res){
    if(!self.scriptsCargados){
      editor.id = res.id;
      return;
    }
    var data = [];
    if(editor.id){
      pos = self.getScriptPosForId(res.id);
      self.scripts[pos] = {
        id: res.id,
        nombre: res.nombre,
        codigo: res.codigo
      };
      return;
    }
    editor.id = res.id;
    data.push({
        text: datos.nombre,
        attributes: {
          pos: self.scripts.length,
          tipo: 'script'
        }
      });
    self.scripts.push({
      id: res.id,
      nombre: datos.nombre,
      codigo: datos.codigo
    });
    self.appendScript(data);
  });
}

Usuario.prototype.getScriptPosForId = function(id, array = this.scripts){
  for (var i = 0; i < array.length; i++) {
    if(array['id'] == id)
      return i;
  }
  return null;
}

/**************************
      Aulas
**************************/
Usuario.prototype.saveAula = function(nombre , id = null){
  var self = this;
  /**
  A enviar
  id, nombre, clave, usuario_id
  */
  var datos = {};
  datos.nombre = nombre;
  if(id){
    datos.id = id;
  }
  

  this.llamadaAjax('aula/save/', datos, function(res){
    var data = [];
    if(id){
      pos = self.getAulaPosForId(res.id);
      self.aulas[pos] = {
        id: res.id,
        nombre: res.nombre,
        semestre: res.semestre,
        proyectos: self.aulas[pos].proyectos,
        usuarios: res.usuarios,
        codigo: res.codigo
      };
      self.updateNode(res.nombre);
      alert('Código: '+res.codigo+'\nNota: también puede ver este código al modificar el aula');
      return;
    }
    var nuevoId = res.id;
    var array = [];
    //usuarios
    var childrens = [];
    childrens.push({
      text: 'Unir usuario',
      iconCls: "icon-search",
      attributes: {
        tipo: 'aula-estudiante-buscar',
        idAula: res.id,
        posAula: self.aulas.length,
      }
    });
    array[0] = {
      id: 21+''+self.aulas.length,
      text: 'Estudiantes',
      state: 'closed',//closed
      iconCls: "icon-man",
      children: childrens
    };


    //proyectos
    /*var proyectoR = [];
    proyectoR.push({
      text: 'Buscar y recuperar proyecto',
      iconCls: "icon-trash",
      attributes: {
        tipo: 'proyecto-recuperar',
        idAula: res.id,
      }
    });*/

    var tiposProyectos = [];
          tiposProyectos.push({
            id: 22+'-1'+self.aulas.length,
            text: 'Evaluaciones',
            children: [],
            state: 'closed',
          });
          tiposProyectos.push({
            id: 22+'-2'+self.aulas.length,
            text: 'No evaluados',
            children: [],
            state: 'closed',
          });

    array[1] = {
      id: 22+''+self.aulas.length,
      text: 'Proyectos',
      state: 'closed',//closed
      iconCls: "icon-save",
      children: tiposProyectos,//[{text:'dfdf', id:'proyecto'+res.id}],
      attributes: {
        pos: self.aulas.length,
        tipo: 'aula-proyecto-padre',
        idAula: res.id,
      }
    };
    data.push({
        text: datos.nombre,
        children: array,
        state: 'closed',
        attributes: {
          pos: self.aulas.length,
          tipo: 'aula',
          cargada: true,
          id: res.id
        }
      });

    self.aulas.push({
      id: res.id,
      nombre: datos.nombre,
      proyectos: [],
      usuarios:[],
      semestre: res.semestre,
      codigo: res.codigo
    });
    self.appendScript(data, 24+''+res.semestre.id);
    var cant = 0;
    for (var i = 0; i < self.aulas.length; i++) {
      if(self.aulas[i].semestre.id == res.semestre.id)
        cant++;
    }
    self.updateNode(res.semestre.nombre+' ('+(cant)+')', 24+''+res.semestre.id);
    alert('Código: '+res.codigo+'\nNota: también puede ver este código al modificar el aula');
  },
  function(res){
    $('#loadingModal').modal('hide');
    $('#nuevaAulaModal').modal('hide');
  }
  );
}

Usuario.prototype.buscarParaRecuperarAula = function(fecha_inicio){
  var self = this;
  /**
  A enviar
  fecha
  */
  var datos = {};
  datos.fecha_inicio = fecha_inicio;
  $('#recuperarAulaModalBody').html('');
  

  this.llamadaAjax('aula/searchInTrash/', datos, function(res){
    var tbody = '';
    for (var i = 0; i < res.length; i++) {
      tbody+=
              '<tr id="tablaaula'+res[i].id+'">'+
                '<td >'+res[i].nombre+'</td>'+
                '<td ><a class="btn btn-info " href="#"'+
                  ' onclick="Usuario.recuperarAula('+res[i].id+')">Recuperar</a></td>'+
              '</tr>';

    }

    var tabla = '<table style="width:100%;height:70%; margin-top:20px;">'+
      '<thead>'+
        '<tr>'+
          '<th >Nombre</th>'+
          '<th ></th>'+
        '</tr>'+
      '</thead>'+
      '<tbody>'+
        tbody
      '</tbody>'+
    '</table>';

      $('#recuperarAulaModalBody').append('<div>'+tabla+'<dv>');
  },
  function(res){
    $('#loadingModal').modal('hide');
  }
  );
}

Usuario.prototype.getAulaPosForId = function(id, array = this.aulas){
  for (var i = 0; i < array.length; i++) {
    if(array[i]['id'] == id)
      return i;
  }
  return null;
}

/**************************
      Proyectos
**************************/
Usuario.prototype.saveProyecto = function(nombre , valor, 
  enunciado, fechaInicio, fechaFin, validacion_ip,
  ip_rango_inicial, ip_rango_final, clave,  id = null){
  var self = this;
  /**
  A enviar
  id, nombre, valor, enunciado,
  aula_id, fecha_inicio, fecha_fin
  validar_ip, ip_rango_inicial, ip_rango_final
  */
  var datos = {};
  datos.nombre = nombre;
  datos.valor = valor;
  datos.enunciado = enunciado;
  datos.fecha_inicio = fechaInicio;
  datos.fecha_fin = fechaFin;
  datos.validacion_ip = validacion_ip;
  datos.ip_rango_inicial = ip_rango_inicial;
  datos.ip_rango_final = ip_rango_final;
  datos.clave = clave;
  datos.aula_id = $('#'+self.tree+'_menu_proyecto_padre').data('aulaid');
  var aulaId = $('#'+self.tree+'_menu_proyecto_padre').data('aulaid');
  var posAula = self.getAulaPosForId(aulaId);
  if(id){
    datos.id = id;
  }
  

  this.llamadaAjax('proyecto/save/', datos, function(res){
    var data = [];
    if(id){
      var pos = self.getProyectoPosForId(res.id, posAula);
      res.entregas = self.aulas[posAula].proyectos[pos].entregas;
      self.aulas[posAula].proyectos[pos] = res;
      self.updateNode(res.nombre);
      return;
    }
    var nuevoId = res.id;
    data.push({
        text: datos.nombre,
        state: 'closed',
        children: [],
        attributes: {
          pos: self.aulas[posAula].proyectos.length,
          tipo: 'aula-proyecto',
          cargada: true,
          idAula: self.aulas[posAula].id,
          idProyecto: res.id,
          posAula: posAula
        }
      });
    self.aulas[posAula].proyectos.push({
      id: res.id,
      nombre: res.nombre,
      valor: res.valor,
      enunciado: res.enunciado,
      aula_id: res.aula_id,
      fecha_fin: res.fecha_fin,
      fecha_inicio: res.fecha_inicio,
      validacion_ip: res.validacion_ip,
      ip_rango_inicial: res.ip_rango_inicial,
      ip_rango_final: res.ip_rango_final,
      entregas: []
      });
    var evaluado = '-1';
    if(res.valor <= 0)
      evaluado = '-2';
    self.appendScript(data, 22+evaluado+(posAula));
  },
  function(res){
    $('#loadingModal').modal('hide');
    $('#nuevoProyectoModal').modal('hide');
  }
  );
}

Usuario.prototype.buscarParaRecuperarProyecto = function(fecha_inicio, idAula){
  var self = this;
  /**
  A enviar
  fecha
  */
  var datos = {};
  datos.fecha_inicio = fecha_inicio;
  datos.idAula = idAula;
  $('#recuperarProyectoModalBody').html('');
  

  this.llamadaAjax('proyecto/searchInTrash/', datos, function(res){
    var tbody = '';
    for (var i = 0; i < res.length; i++) {
      tbody+=
              '<tr id="tablaproyecto'+res[i].id+'">'+
                '<td >'+res[i].nombre+'</td>'+
                '<td ><a class="btn btn-info " href="#"'+
                  ' onclick="Usuario.recuperarProyecto('+res[i].id+')">Recuperar</a></td>'+
              '</tr>';

    }

    var tabla = '<table style="width:100%;height:70%; margin-top:20px;">'+
      '<thead>'+
        '<tr>'+
          '<th >Nombre</th>'+
          '<th ></th>'+
        '</tr>'+
      '</thead>'+
      '<tbody>'+
        tbody
      '</tbody>'+
    '</table>';

      $('#recuperarProyectoModalBody').append('<div>'+tabla+'<dv>');
  },
  function(res){
    $('#loadingModal').modal('hide');
  }
  );
}

Usuario.prototype.getProyectoPosForId = function(id, aulaPos, array=this.aulas){
  for (var j = 0; j < array[aulaPos].proyectos.length; j++) {
      if(array[aulaPos].proyectos[j]['id'] == id){
        return j;
      }
  }
  return null;
}

Usuario.prototype.getUsuarioAulaPosForId = function(id, aulaPos, array = this.aulas){
  for (var j = 0; j < array[aulaPos].usuarios.length; j++) {
      if(array[aulaPos].usuarios[j]['id'] == id){
        return j;
      }
  }
  return null;
}


/**************************
      Entregas Evaluar
**************************/
Usuario.prototype.evaluarEntrega = function(id, nota, comentarios){
  var self = this;
  /**
  A enviar
  id, nota, comentarios
  */
  var datos = {};
  datos.id = id;
  datos.nota = nota;
  datos.comentarios = comentarios;
  var posAula = $('#'+self.tree+'_menu_entrega_evaluar').data('posaula');
  var posProyecto = $('#'+self.tree+'_menu_entrega_evaluar').data('posproyecto');
  var pos = $('#'+self.tree+'_menu_entrega_evaluar').data('pos');
  

  this.llamadaAjax('entrega/evaluate/', datos, function(res){
    var data = [];

    if(id){
      self.aulas[posAula].proyectos[posProyecto].entregas[pos] = res;

      return;
    }
  },
  function(res){
    $('#loadingModal').modal('hide');
    $('#nuevaEntregaModal').modal('hide');
  }
  );
}

/****************************
      Entregas Abrir Código
****************************/
Usuario.prototype.abrirEntrega = function(){
  var posAula = $('#'+this.tree+'_menu_entrega_evaluar').data('posaula');
  var posProyecto = $('#'+this.tree+'_menu_entrega_evaluar').data('posproyecto');
  var pos = $('#'+this.tree+'_menu_entrega_evaluar').data('pos');
  editores.nuevoTab('cédula: '+this.aulas[posAula].proyectos[posProyecto].entregas[pos].usuario.cedula,
          this.aulas[posAula].proyectos[posProyecto].entregas[pos].codigo);
}

/**************************
      Entregas Realizar
**************************/
Usuario.prototype.realizarEntrega = function(codigo, proyecto_id, id = null){
  var self = this;
  /**
  A enviar
  id, nota, comentarios
  */
  var datos = {};
  datos.codigo = codigo;
  datos.proyecto_id = proyecto_id;
  datos.clave = '';
  var posAula = $('#'+self.tree+'_menu_entrega_entregar').data('posaula');
  var posProyecto = $('#'+self.tree+'_menu_entrega_entregar').data('posproyecto');
  if(this.misAulas[posAula].proyectos[posProyecto].clave){
    var aux = prompt('Ingrese clave del proyecto :','');
    datos.clave = aux;
  }
  if(id)
    datos.id = id;
  

  this.llamadaAjax('entrega/save/', datos, function(res){
    var data = [];

    if(id){
      var pos = $('#'+self.tree+'_menu_entrega_entregar').data('pos');
      self.misAulas[posAula].proyectos[posProyecto].entregas[pos] = {
        id: res.id,
        proyecto_id: res.proyecto_id,
        usuario_aula_id: res.usuario_aula_id,
        codigo: res.codigo,
        nota: res.nota,
        comentarios: res.comentarios
      };

      return;
    }
    var nuevoId = res.id;
    data.push({
        text: res.usuario.alias
                +' ('+Number(res.usuario.cedula).toLocaleString()+')',
        attributes: {
          pos: self.misAulas[posAula].proyectos[posProyecto].entregas.length,
          id: res.id,
          tipo: 'aula-proyecto-entrega-estudiante',
          idAula: self.misAulas[posAula].id,
          idProyecto: self.misAulas[posAula].proyectos[posProyecto]['id'],
          idUsuarioAula: res.usuario.id,
          posAula: posAula,
          posProyecto: posProyecto
        }
      });
    self.misAulas[posAula].proyectos[posProyecto].entregas.push({
        id: res.id,
        proyecto_id: res.proyecto_id,
        usuario_aula_id: res.usuario_aula_id,
        codigo: res.codigo,
        nota: res.nota,
        comentarios: res.comentarios
      });
    self.appendScript(data, 23+''+posAula+'-'+posProyecto);
  },
  function(res){
    $('#loadingModal').modal('hide');
    $('#nuevaEntregaModal').modal('hide');
  }
  );
}


/**************************
      Semestres
**************************/
Usuario.prototype.saveSemestre = function(nombre, id = null){
  var self = this;
  /**
  A enviar
  id, nombre
  */
  var datos = {};
  datos.nombre = nombre;
  var pos = $('#'+self.tree+'_menu_semestres').data('pos');
  if(id){
    datos.id = id;
  }
  

  this.llamadaAjax('semestre/save/', datos, function(res){
    var data = [];
    if(id){
      Usuario.semestres[pos] = {
        id: res.id,
        nombre: res.nombre,
      };
      self.updateNode(res.nombre);
      return;
    }
    var nuevoId = res.id;
    data.push({
        text: datos.nombre,
        iconCls: "icon-remove",
        attributes: {
          pos: Usuario.semestres.length,
          tipo: 'semestre',
          id: res.id
        }
      });
    Usuario.semestres.push({
      id: res.id,
      nombre: res.nombre
      });
    self.appendScript(data, 32);
  },
  function(res){
    $('#loadingModal').modal('hide');
    $('#nuevoSemestreModal').modal('hide');
  }
  );
}


/**************************
      Usuarios
**************************/
Usuario.prototype.searchUsuario = function(buscar){
  var self = this;
  /**
  cédula, email o alias
  */
  var datos = {};
  datos.buscar = buscar;
  

  this.llamadaAjax('user/search/', datos, function(res){
    var data = [];
    $('#loadingModal').modal('hide');
    $('#buscarUsuarioModal').modal('hide');
    $("#nombreRegistroAModal").val(res.nombre);
    $("#correoRegistroAModal").val(res.correo);
    $("#cedulaRegistroAModal").val(res.cedula);
    $("#carreraRegistroAModal").val(res.carrera_id);
    $("#rolRegistroAModal").val(res.rol_id);
    $("#claveRegistroAModal").val('');
    $("#clave1RegistroAModal").val('');
    $("#registroAModal").data('id', res.id);
    setTimeout(function(){ 
        $('#registroAModal').modal('show');
      }, 500);

  },
  function(res){
    $('#loadingModal').modal('hide');
    $('#buscarUsuarioModal').modal('hide');
  }
  );
}

Usuario.prototype.saveUsuario = function(nombre, clave, correo, 
  cedula, rol_id, carrera_id, id = null){
  var self = this;
  /**
  A enviar
  id, nombre, clave, correo, cedula, rol_id, carrera_id
  */
  var datos = {};
  datos.nombre = nombre;
  datos.clave = clave;
  datos.correo = correo;
  datos.cedula = cedula;
  datos.rol_id = rol_id;
  datos.carrera_id = carrera_id;
  if(id){
    datos.id = id;
  }
  

  this.llamadaAjax('user/saveA', datos, function(res){
    alert('Cambios guardados con éxito');
    
  },
  function(res){
    $('#loadingModal').modal('hide');
    $('#registroAModal').modal('hide');
  }
  );
}


/***************************************
      Usuarios a buscar para unir a aula
id: es id de aula
*******************************************/
Usuario.prototype.joinUsuarioToAula = function(buscar, id){
  var self = this;
  /**
  buscar, id
  */
  var datos = {};
  datos.buscar = buscar;
  datos.id = id;
  

  this.llamadaAjax('usuario_aula/add/', datos, function(res){
    var data = [];
    var pos = $('#buscarUsuarioAModal').data('pos');
    $('#buscarUsuarioAModal').modal('hide');
    var nuevoId = res.id;
    data.push({
        text: res.alias+
        ' ('+Number(res.cedula).toLocaleString()+')',
        iconCls: "icon-man",
        attributes: {
          pos: self.aulas[pos].usuarios.length,
          tipo: 'aula-estudiante',
          idAula: self.aulas[pos].id,
          id: res.id,
          posAula: pos
        }
      });
    self.aulas[pos].usuarios.push({
      res
      });
    self.appendScript(data, 21+''+(pos));

  },
  function(res){
    $('#loadingModal').modal('hide');
  }
  );
}

/**************************
      Parámetros
      Solo para rol ADMINISTRADOR
**************************/
Usuario.prototype.updateParametro = function(id, valor){
  var self = this;
  /**
  A enviar
  id, valor
  */
  var datos = {};
  datos.id = id;
  datos.valor = valor;
  

  this.llamadaAjax('parametro/update/', datos, function(res){
    self.parametros = res;
    
  },
  function(res){
    $('#loadingModal').modal('hide');
    $('#parametrosModal').modal('hide');
  }
  );
}


/**************************
      Unirse a Aula
**************************/
Usuario.prototype.joinAula = function(codigo, clave){
  var self = this;
  /**
  A enviar
  id, nombre, clave, correo, cedula, rol_id, carrera_id
  */
  var datos = {};
  datos.codigo = codigo;
  

  this.llamadaAjax('usuario_aula/save/', datos, function(res){
    alert('¡Bienvenido al aula!');
    window.usuario = new Usuario(res, editores);
  },
  function(res){
    $('#loadingModal').modal('hide');
    $('#buscarAulaModal').modal('hide');
  }
  );
}


/**************************
      Arbol
**************************/
Usuario.prototype.getTree = function(){
  var array = [];/*[{
      text: 'Item1',
      state: 'closed',
      children: [{
        text: 'Item11'
      },{
        text: 'Item12'
      }]
    },{
      text: 'Item2'
    }];*/
    var childrens = [];
    array[0] = {
      id: 1,
      text: 'Scripts',
      state: 'closed',//opened
      iconCls: "icon-save",
      children: [{text:'df', id:'scripts'}],
      attributes: {
        tipo: 'scripts'
      }
    };

    //Aulas de un profesor
    if(this.rol.nombre != 'ESTUDIANTE' && this.rol.nombre != 'EXTERNO'){
        array[1] = {
          id: 2,
          text: 'Aulas',
          state: 'closed',//closed
          attributes: {
            tipo: 'aula-padre'
          },
          iconCls: "icon-large-smartart",
          children: this.getSemestresTree()
        };
    }
    //Aulas de un estudiante
    else{
      array[1] = {
        id: 2,
        text: 'Aulas',
        state: 'closed',//closed
        attributes: {
          //tipo: 'aula-padre'
        },
        iconCls: "icon-large-smartart",
        children: this.getAulasEstudianteTree()
      };
    }

    //Zona de Administración
    if(this.rol.nombre == 'ADMINISTRADOR'){
        array[2] = {
          id: 3,
          text: 'Administración',
          state: 'closed',//closed
          attributes: {
            tipo: 'administracion'
          },
          iconCls: "icon-large-smartart",
          children: this.getAdministracionTree()
        };
    }

  return array;

}

/*****
  Cargar Scripts de un usuario primera vez
  Via ajax
******/

Usuario.prototype.loadScripts = function(){
  var self = this;
  $('#loadingModal').modal('show');
  this.llamadaAjax('script/getall/', null, function(res){
    self.scriptsCargados = true;
    self.scripts = res;
    for (var i = 0; i < self.scripts.length; i++) {
      self.appendScript({
        text: self.scripts[i]['nombre'],
        attributes: {
          pos: i,
          tipo: 'script'
        }
      },1);
    }
  }, function(){
    $('#loadingModal').modal('hide');
  });
}

/*****
  Cargar estructura de un aula de un profesor por primera vez
  Via ajax
******/

Usuario.prototype.loadAula = function(id){
  var self = this;
  $('#loadingModal').modal('show');
  this.llamadaAjax('aula/get/'+id, null, function(res){
    var pos = self.getAulaPosForId(id);
    var childrens = [];
    self.aulas[pos].usuarios = res.usuarios;
    for (var j = 0; j < self.aulas[pos].usuarios.length; j++) {
      childrens.push({
        text: self.aulas[pos].usuarios[j]['alias']
              +' ('+Number(self.aulas[pos].usuarios[j].cedula).toLocaleString()+')',
        iconCls: "icon-man",
        attributes: {
          pos: j,
          tipo: 'aula-estudiante',
          idAula: self.aulas[pos].id,
          posAula: pos,
          id:self.aulas[pos].usuarios[j]['id']
        }
      });
    }
    self.appendScript(childrens,21+''+pos);

    childrensE = [];
    childrensNE = [];
    self.aulas[pos].proyectos = res.proyectos;
        for (var j = 0; j < self.aulas[pos].proyectos.length; j++) {

          if(self.aulas[pos].proyectos[j].valor > 0)
            childrensE.push({
              id: 23+''+pos+'-'+j,
              text: self.aulas[pos].proyectos[j]['nombre'],
              children: [{text:'df', id:'entrega'
                    +self.aulas[pos].id+''+self.aulas[pos].proyectos[j]['id']}],
              state: 'closed',//closed
              attributes: {
                pos: j,
                tipo: 'aula-proyecto',
                cargada: false,
                idAula: self.aulas[pos].id,
                idProyecto: self.aulas[pos].proyectos[j]['id'],
                posAula: pos
              }
            });
          else
            childrensNE.push({
              id: 23+''+pos+'-'+j,
              text: self.aulas[pos].proyectos[j]['nombre'],
              children: [{text:'df', id:'entrega'
                    +self.aulas[pos].id+''+self.aulas[pos].proyectos[j]['id']}],
              state: 'closed',//closed
              attributes: {
                pos: j,
                tipo: 'aula-proyecto',
                cargada: false,
                idAula: self.aulas[pos].id,
                idProyecto: self.aulas[pos].proyectos[j]['id'],
                posAula: pos
              }
            });
        }
          var tiposProyectos = [];
          tiposProyectos.push({
            id: 22+'-1'+pos,
            text: 'Evaluaciones',
            children: childrensE,
            state: 'closed',
          });
          tiposProyectos.push({
            id: 22+'-2'+pos,
            text: 'No evaluados',
            children: childrensNE,
            state: 'closed',
          });
        self.appendScript(tiposProyectos,22+''+pos);

  }, function(){
    $('#loadingModal').modal('hide');
  });
}

/*****
  Cargar estructura de un proyecto de un aula de un profesor por primera vez
  Via ajax
******/

Usuario.prototype.loadProyecto = function(idProyecto, idAula){
  var self = this;
  $('#loadingModal').modal('show');
  this.llamadaAjax('proyecto/get_by_project/'+idProyecto, null, function(res){
    var pos = self.getAulaPosForId(idAula);
    var posProyecto = self.getProyectoPosForId(idProyecto, pos);

        self.aulas[pos].proyectos[posProyecto] = res;
          //entregas
          var entregas = [];
          for (var k = 0; k < self.aulas[pos].proyectos[posProyecto].entregas.length; k++) {
            entregas.push({
              text: self.aulas[pos].proyectos[posProyecto].entregas[k].usuario.alias
                  +' ('
                  +Number(self.aulas[pos].proyectos[posProyecto].
                    entregas[k].usuario.cedula).toLocaleString()+')',
              attributes: {
                pos: k,
                id: self.aulas[pos].proyectos[posProyecto].entregas[k].id,
                tipo: 'aula-proyecto-entrega',
                idAula: self.aulas[pos].id,
                idProyecto: self.aulas[pos].proyectos[posProyecto]['id'],
                idUsuarioAula: self.aulas[pos].proyectos[posProyecto].entregas[k].usuario.id,
                posAula: pos,
                posProyecto: posProyecto
              }
            });
          }
        self.appendScript(entregas,23+''+pos+'-'+posProyecto);

  }, function(){
    $('#loadingModal').modal('hide');
  });
}




/**
  SEMESTRES PARA UBICAR AULAS
  
**/
Usuario.prototype.getSemestresTree = function(){
  var array = [];
  /*array.push({id: 25,
          text: 'Buscar y recuperar aula',
          iconCls: "icon-trash",
          attributes: {
            tipo: 'aula-recuperar',
          }
        })
        */

  for (var i = 0; i < Usuario.semestres.length; i++) {
    if(Usuario.semestres[i].id == this.semestre.id){//valor por ser parámetro
      if(i>0)
        array.push(this.newSemestreTreeStruct(Usuario.semestres[i-1], i-1));
      array.push(this.newSemestreTreeStruct(Usuario.semestres[i], i));
      break;
    }
  }
  return array;
}

Usuario.prototype.newSemestreTreeStruct = function(semestre, i){
  var  aulas = this.getAulasTree(semestre.id);
  return {id: 24+''+semestre.id,
          text: semestre.nombre+' ('+(aulas.length)+')',
          state: 'closed',//closed
          iconCls: "icon-filter",
          attributes: {
            tipo: 'semestre-aula',
            id: semestre.id,
            pos: i,
          },
          children: aulas
        };
}

/**
  ARBOL DE PROFESORES
**/
Usuario.prototype.getAulasTree = function(idSemestre){
    //Aulas de un profesor
      var aulas = [];
      for (var i = 0; i < this.aulas.length; i++) {
        if(this.aulas[i].semestre.id != idSemestre)
          continue;
        var array = [];

        var childrens = [];
        childrens.push({
            text: 'Unir usuario',
            iconCls: "icon-search",
            attributes: {
              tipo: 'aula-estudiante-buscar',
              idAula: this.aulas[i].id,
              posAula: i,
            }
        });
        for (var j = 0; j < this.aulas[i].usuarios.length; j++) {
          childrens.push({
            text: this.aulas[i].usuarios[j]['alias']
                  +' ('+Number(this.aulas[i].usuarios[j].cedula).toLocaleString()+')',
            iconCls: "icon-man",
            attributes: {
              pos: j,
              tipo: 'aula-estudiante',
              idAula: this.aulas[i].id,
              posAula: i,
              id:this.aulas[i].usuarios[j]['id']
            }
          });
        }
        //usuarios
        array[0] = {
          id: 21+''+i,
          text: 'Estudiantes',
          state: 'closed',//closed
          iconCls: "icon-man",
          children: childrens
        };

        childrens = [];
        for (var j = 0; j < this.aulas[i].proyectos.length; j++) {
          //entregas
          var entregas = [];
          for (var k = 0; k < this.aulas[i].proyectos[j].entregas.length; k++) {
            var posUsuario = this.getUsuarioAulaPosForId(
              this.aulas[i].proyectos[j].entregas[k].usuario_aula_id, i);
            entregas.push({
              text: this.aulas[i].usuarios[posUsuario].alias
                    +' ('+Number(this.aulas[i].usuarios[posUsuario].cedula).toLocaleString()+')',
              attributes: {
                pos: k,
                id: this.aulas[i].proyectos[j].entregas[k].id,
                tipo: 'aula-proyecto-entrega',
                idAula: this.aulas[i].id,
                idProyecto: this.aulas[i].proyectos[j]['id'],
                idUsuarioAula: this.aulas[i].usuarios[posUsuario].id,
                posAula: i,
                posProyecto: j
              }
            });
          }
          childrens.push({
            id: 23+''+i+'-'+j,
            text: this.aulas[i].proyectos[j]['nombre'],
            children: entregas,
            state: 'closed',//closed
            attributes: {
              pos: j,
              tipo: 'aula-proyecto',
              idAula: this.aulas[i].id,
              idProyecto: this.aulas[i].proyectos[j]['id'],
              posAula: i
            }
          });
        }
        //Proyectos
        /*var proyectoR = [];
        proyectoR.push({
          text: 'Buscar y recuperar proyecto',
          iconCls: "icon-trash",
          attributes: {
            tipo: 'proyecto-recuperar',
            idAula: this.aulas[i].id,
          }
        });*/
        array[1] = {
          id: 22+''+i,
          text: 'Proyectos',
          iconCls: "icon-save",
          children: [{text:'dfdf', id:'proyecto'+this.aulas[i].id}],
          state: 'closed',//closed
          attributes: {
            pos: i,
            tipo: 'aula-proyecto-padre',
            idAula: this.aulas[i].id,
            }
        };

        aulas.push({
          text: this.aulas[i]['nombre'],
          children: array,
          state: 'closed',//closed
          attributes: {
            pos: i,
            tipo: 'aula',
            cargada: false,
            id: this.aulas[i]['id']
          }
        });
      }

  return aulas;
}

/**
  ACTUALIZAR AULAS DE UN PROFESOR
**/
/*Usuario.prototype.UpdateAulasTree = function(data = this.getAulasTree(), pos = 2){
  var datos = data;
  var node = $('#'+this.tree).tree('find',pos);
  $('#'+this.tree).tree('update', {
      target: node.target,
      children: datos
    });
}*/


/**
  ARBOL DE ADMINISTRACIÓN
**/
Usuario.prototype.getAdministracionTree = function(){
      var administracion = [];
      var semestres = [];
      for (var i = 0; i < Usuario.semestres.length; i++) {
        //semestre
        semestres.push({
          text: Usuario.semestres[i].nombre,
          iconCls: "icon-remove",
          attributes: {
            pos: i,
            tipo: 'semestre',
            id: Usuario.semestres[i].id,
            }
        });

      }

      var childrens = [];
      childrens.push({
        text: 'Buscar usuario',
        iconCls: "icon-search",
        attributes: {
          tipo: 'usuarios-buscar'
        }
      });
      childrens.push({
        text: 'Crear usuario',
        iconCls: "icon-add",
        attributes: {
          tipo: 'usuarios-crear'
        }
      });
      administracion.push({
        id:31,
        text: 'Usuarios',
        children: childrens,
        state: 'closed',//closed
        attributes: {
          tipo: 'usuarios'
        }
      });

      administracion.push({
        id:32,
        text: 'semestres',
        children: semestres,
        state: 'closed',//closed
        attributes: {
          tipo: 'semestres'
        }
      });

      administracion.push({
        id:33,
        text: 'sistema',
        iconCls: "icon-filter",
        attributes: {
          tipo: 'parametros'
        }
      });

      administracion.push({
        id:34,
        text: 'Estadísticas',
        iconCls: "icon-large-chart",
        attributes: {
          tipo: 'estadisticas'
        }
      });
  return administracion;
}

/**
  ARBOL DE ESTUDIANTES
**/

Usuario.prototype.getAulasEstudianteTree = function(){
    //Aulas de un estudiante
      var aulas = [];
      aulas.push({
        text: 'Unirse a una aula',
        iconCls: "icon-search",
        attributes: {
          tipo: 'estudiante-aula-buscar'
        }
      });
      for (var i = 0; i < this.misAulas.length; i++) {
        var array = [];
        var childrens = [];
        array[0] = {
          id: 22+''+i,
          text: 'Proyectos',
          state: 'closed',
          iconCls: "icon-save",
          children: this.newProyectoTreeStruct(i),
          attributes: {
            pos: i,
            tipo: 'aula-proyecto-padre-estudiante',
            idAula: this.misAulas[i].id,
            }
        };
        

        aulas.push({
          text: this.misAulas[i]['nombre'],
          children: array,
          state: 'closed',
          attributes: {
            pos: i,
            tipo: 'estudiante-aula',
            idAula: this.misAulas[i].id
          }
        });
      }

  return aulas;
}

Usuario.prototype.newProyectoTreeStruct = function(i){
  var tiposProyectos = [];

  var childrensE = [];//evaluados
  var childrensNE = [];//no evaludados
  for (var j = 0; j < this.misAulas[i].proyectos.length; j++) {
    var entregas = [];
    for (var k = 0; k < this.misAulas[i].proyectos[j].entregas.length; k++) {
      var posUsuario = this.getUsuarioAulaPosForId(
        this.misAulas[i].proyectos[j].entregas[k].usuario_aula_id, i, this.misAulas);
            entregas.push({
              text: this.misAulas[i].usuarios[posUsuario].alias
                    +' ('+Number(this.misAulas[i].usuarios[posUsuario].cedula).toLocaleString()+')',
              attributes: {
                pos: k,
                id: this.misAulas[i].proyectos[j].entregas[k].id,
                tipo: 'aula-proyecto-entrega-estudiante',
                idAula: this.misAulas[i].id,
                idProyecto: this.misAulas[i].proyectos[j]['id'],
                idUsuarioAula: this.misAulas[i].usuarios[posUsuario].id,
                posAula: i,
                posProyecto: j
              }
            });
          }
          if(this.misAulas[i].proyectos[j]['valor']>0)
              childrensE.push({
                id: 23+''+i+'-'+j,
                text: this.misAulas[i].proyectos[j]['nombre'],
                children: entregas,
                state: 'closed',
                attributes: {
                  pos: j,
                  tipo: 'aula-proyecto-estudiante',
                  idAula: this.misAulas[i].id,
                  idProyecto: this.misAulas[i].proyectos[j]['id'],
                  posAula: i,
                  posProyecto: j
                }
              });
            else
              childrensNE.push({
                id: 23+''+i+'-'+j,
                text: this.misAulas[i].proyectos[j]['nombre'],
                children: entregas,
                state: 'closed',
                attributes: {
                  pos: j,
                  tipo: 'aula-proyecto-estudiante',
                  idAula: this.misAulas[i].id,
                  idProyecto: this.misAulas[i].proyectos[j]['id'],
                  posAula: i,
                  posProyecto: j
                }
              });
        }


  tiposProyectos.push({
            id: 22+'-1'+i,
            text: 'Evaluaciones',
            children: childrensE,
            state: 'closed',
          });
  tiposProyectos.push({
            id: 22+'-2'+i,
            text: 'No evaluados',
            children: childrensNE,
            state: 'closed',
          });
        return tiposProyectos;

  
}


/*****************************************
                Estadísticas

*****************************************/
Usuario.prototype.graficar = function(nombreHtml, semestre, tipoGrafico){
  var self = this;
  /* 0 Usuarios registrados
    1 Aulas creadas
    2 Proyectos creados
  */
  var titulo = "Cantidad de usuarios registrados";
  switch(Number(tipoGrafico)){
    case 1:
      titulo = 'Cantidad de aulas creadas';
      break;
    case 2:
      titulo = 'Cantidad de proyectos creados';
      break;
  }

  $('#loadingModal').modal('show');

  

  this.llamadaAjax('grafica/'+semestre+'/'+tipoGrafico, null, function(res){
    $('#'+nombreHtml).html('');
    var barChartData = {
        labels: res.labels,
        datasets: res.datasets,

      };
      if(window.myBar)
        window.myBar.destroy();

        var ctx = document.getElementById(nombreHtml).getContext('2d');
        window.myBar = new Chart(ctx, {
          type: 'bar',
          data: barChartData,
          options: {
            responsive: true,
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: titulo
            },
            scales: {
              yAxes: [{
                ticks: {
                  //beginAtZero: true,
                  min: 0,
                  //stepSize: 1
                }
              }]
            }
          }
        });
    

  }, function(){
    $('#loadingModal').modal('hide');
  });
}


Usuario.prototype.llamadaAjax = function(url, data= null, functionSuccess = null, functionAlways = null){

  $.ajax({
        url: window.urlBase+url,
        method: "POST",
        dataType: "json",
        /*headers: {
            Authorization: tokenString
        },*/
        data: data,
        statusCode: {
            404: function (res) {
                //If the endpoint is not found, we'll end up in here
                $.toaster({ priority : 'warning', title : 'URL', message : 'No existe la url'});
            },
            200: function (res) {
                //Ok, everything worked as expected
                //console.log("Correcto");
                if(functionSuccess)
                  functionSuccess(res);
            },
            401: function (res) {
                $.toaster({ priority : 'warning', title : 'No autorizado', message : 'No está autorizado'});
                //Our token is either expired, invalid or doesn't exist
                //alert("");
            }
        }
      }).always(function(res){
          if(functionAlways)
            functionAlways(res);
        });
}



Usuario.login = function(alias, clave){
  $.ajax({
        url: window.urlBase+"user/login",
        method: "POST",
        dataType: "json",
        /*headers: {
            Authorization: tokenString
        },*/
        data: {alias: alias, clave: clave},
        statusCode: {
            404: function (res) {
                //If the endpoint is not found, we'll end up in here
                $.toaster({ priority : 'warning', title : 'URL', message : 'No existe la url'});
            },
            200: function (res) {
                //Ok, everything worked as expected
                //console.log("Correcto");
                $('#loginModal').modal('hide');
                $('#navbar_userLogin').hide();
                $('#navbar_userName').html(res['alias']);
                $('#navbar_user').show();
                $('#save_script').show();
                window.usuario = new Usuario(res, editores);

            },
            401: function (res) {
                $.toaster({ priority : 'warning', title : 'No autorizado', message : 'No está autorizado'});
                //Our token is either expired, invalid or doesn't exist
                //alert("");
            }
        }
         /*url: "jquery-ajax", //request page
        }
    cache: false,       // prevent to get cached response
    data: {todo:"ajaxexample1"}, //data sent to request page
    success: functionSucceed,   //this will be called if request succeeds
    error: functionFailed,       //this will be called if request fails
    statusCode: {    //statusCode handling
      404: function() {
        alert("page not found");
      }
    }   */
      }).always(function(res){
          $('#loadingModal').modal('hide');
        });
}

Usuario.reingresar = function(){
  $.ajax({
        url: window.urlBase+"user/info",
        method: "POST",
        dataType: "json",
        statusCode: {
            404: function (res) {
            },
            200: function (res) {
                $('#navbar_userLogin').hide();
                $('#navbar_userName').html(res['alias']);
                $('#navbar_user').show();
                $('#save_script').show();
                window.usuario = new Usuario(res, editores);

            },
            401: function (res) {
            }
        }
      }).always(function(res){
        });
}

Usuario.salir = function(){
  $.ajax({
        url: window.urlBase+"user/logout",
        method: "POST",
        dataType: "json",
        statusCode: {
            404: function (res) {
            },
            200: function (res) {
                $('#navbar_user').hide();
                $('#navbar_userLogin').show();
                $('#navbar_userName').html('');
                $('#esasyui_tree').html('');
                $('#save_script').hide();
                window.usuario = null;

            },
            401: function (res) {
            }
        }
      }).always(function(res){
        });
}



Usuario.register = function(codigo, clave, nombre, correo, 
  cedula, carrera_id, descripcion = ""){
  $.ajax({
        url: window.urlBase+"user/save",
        method: "POST",
        dataType: "json",
        /*headers: {
            Authorization: tokenString
        },*/
        data: {codigo: codigo,
          clave: clave, nombre: nombre, correo: correo,
          cedula:cedula, carrera_id:carrera_id, descripcion:descripcion},
        statusCode: {
            404: function (res) {
                //If the endpoint is not found, we'll end up in here
                $.toaster({ priority : 'warning', title : 'URL', message : 'No existe la url'});
            },
            200: function (res) {
                $('#loginModal').modal('hide');
                $('#navbar_userLogin').hide();
                $('#navbar_userName').html(res['alias']);
                $('#navbar_user').show();
                $('#save_script').show();
                window.usuario = new Usuario(res, editores);

            },
            401: function (res) {
                $.toaster({ priority : 'warning', title : 'No autorizado', message : 'No está autorizado'});

            }
        }
      }).always(function(res){
          $('#loadingModal').modal('hide');
          $('#registroModal').modal('hide');
        });
}
//Para rol EXTERNO
Usuario.registerE = function(clave, nombre, correo, 
  cedula){
  $.ajax({
        url: window.urlBase+"user/saveE",
        method: "POST",
        dataType: "json",
        data: {clave: clave, nombre: nombre, correo: correo, cedula:cedula},
        statusCode: {
            404: function (res) {
                //If the endpoint is not found, we'll end up in here
                $.toaster({ priority : 'warning', title : 'URL', message : 'No existe la url'});
            },
            200: function (res) {
                $('#loginModal').modal('hide');
                $('#navbar_userLogin').hide();
                $('#navbar_userName').html(res['alias']);
                $('#navbar_user').show();
                $('#save_script').show();
                window.usuario = new Usuario(res, editores);

            },
            401: function (res) {
                $.toaster({ priority : 'warning', title : 'No autorizado', message : 'No está autorizado'});

            }
        }
      }).always(function(res){
          $('#loadingModal').modal('hide');
          $('#registroEModal').modal('hide');
        });
}

Usuario.semestres = [];

Usuario.getSemestres = function(){
  $.ajax({
        url: window.urlBase+"semestre/getall",
        method: "POST",
        dataType: "json",
        statusCode: {
            404: function (res) {
                //If the endpoint is not found, we'll end up in here
                //$.toaster({ priority : 'warning', title : 'URL', message : 'No existe la url'});
            },
            200: function (res) {
                Usuario.semestres = res;

            },
            401: function (res) {
                $.toaster({ priority : 'warning', title : 'No autorizado', message : 'No está autorizado'});

            }
        }
      }).always(function(res){
        });
}

Usuario.carreras = [];

Usuario.getCarreras = function(){
  $.ajax({
        url: window.urlBase+"carrera/getall",
        method: "POST",
        dataType: "json",
        statusCode: {
            404: function (res) {
                //If the endpoint is not found, we'll end up in here
                //$.toaster({ priority : 'warning', title : 'URL', message : 'No existe la url'});
            },
            200: function (res) {
                Usuario.carreras = res;
                var select = '';
                for (var i = 0; i < res.length; i++) {
                  select += '<option value="'+res[i].id+'">'+res[i].nombre+'</option>';
                }
                $('#carreraRegistroModal').html(select);
                $('#carreraRegistroAModal').html(select);

            },
            401: function (res) {
                $.toaster({ priority : 'warning', title : 'No autorizado', message : 'No está autorizado'});

            }
        }
      }).always(function(res){
        });
}


Usuario.recuperarAula = function(id){
  $.ajax({
        url: window.urlBase+"aula/recover/"+id,
        method: "POST",
        dataType: "json",
        statusCode: {
            404: function (res) {
            },
            200: function (res) {
                $("#tablaaula"+id).remove();
                window.usuario = new Usuario(res, editores);

            },
            401: function (res) {
                $.toaster({ priority : 'warning', title : 'No autorizado', message : 'No está autorizado'});

            }
        }
      }).always(function(res){
        });
}

Usuario.recuperarProyecto = function(id){
  $.ajax({
        url: window.urlBase+"proyecto/recover/"+id,
        method: "POST",
        dataType: "json",
        statusCode: {
            404: function (res) {
            },
            200: function (res) {
                $("#tablaproyecto"+id).remove();
                window.usuario = new Usuario(res, editores);

            },
            401: function (res) {
                $.toaster({ priority : 'warning', title : 'No autorizado', message : 'No está autorizado'});

            }
        }
      }).always(function(res){
        });
}


Usuario.recuperarClave = function(correo){
  $.ajax({
        url: window.urlBase+"user/forgotpassword",
        method: "POST",
        dataType: "json",
        /*headers: {
            Authorization: tokenString
        },*/
        data: {correo: correo},
        statusCode: {
            404: function (res) {
                //If the endpoint is not found, we'll end up in here
                $.toaster({ priority : 'warning', title : 'URL', message : 'No existe la url'});
            },
            200: function (res) {
                //Ok, everything worked as expected
                //console.log("Correcto");
                alert('¡Revisa tu bandeja de entrada!');

            },
            401: function (res) {
                $.toaster({ priority : 'warning', title : 'No autorizado', message : 'No está autorizado'});
                //Our token is either expired, invalid or doesn't exist
                //alert("");
            }
        }
         /*url: "jquery-ajax", //request page
        }
    cache: false,       // prevent to get cached response
    data: {todo:"ajaxexample1"}, //data sent to request page
    success: functionSucceed,   //this will be called if request succeeds
    error: functionFailed,       //this will be called if request fails
    statusCode: {    //statusCode handling
      404: function() {
        alert("page not found");
      }
    }   */
      }).always(function(res){
          $('#loadingModal').modal('hide');
        });
}