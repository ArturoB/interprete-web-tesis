window.urlBase = 'http://localhost/tesis/backend/public/';
//window.urlBase = 'http://192.168.23.1/tesis/backend/public/';

Usuario.getSemestres();
Usuario.getCarreras();


$("#add_script").on('click', function(){
    $('#nameNewScriptModal').val('');
    $('#newScriptModal').modal('show');
    
});

$('#newScriptModal').on('shown.bs.modal', function() {
  $("#nameNewScriptModal").focus();
});




$("#save_script").on('click', function(){
    usuario.saveScript();
});

$("#usuario_cerrar_sesion").on('click', function(){
    Usuario.salir();
});



$("#calcular").on('click', function(){
  deshabilitarBotonesEjecucion();
  /**********************
  PRUEBA DE EFICIENCIA
  ****************/
  /*ejecucion.setValue('');
  for (var i = 0; i < 10000; i++) {
    ejecucion.setValue(ejecucion.getValue()+
        '\nERROR \n');
  }
  
  alert('listo');
  return;*/
    removePanel();
    if(ast.isRunning){
      //ast.isRunning = false;
      ast.lastInstruction = 0;
      //return;
    }
    else{
      ast.isRunning = true;
    }
    ast.debug = false;
    ast.debugAutomatic = true;
    entradaDatosActivo =false;
    //alert(editor.getValue());
    /*function proceso () {

    }
    Concurrent.Thread.create(proceso);*/
    editores.ejecutar();
    //


  });

$("#detener").on('click', function(){
  habilitarBotonesEjecucion();
    removePanel();
    ast.debugAutomatic = false;
    entradaDatosActivo =false;
    ast.lastInstruction = 0;
    ast.debug = false;
    ast.stop = true;
    if(ast.isRunning)
      ejecucion.setValue(ejecucion.getValue()+
        '\nEjecución finalizada... \n');
    ast.isRunning = false;
    entradaDatos();
  });


$("#step_to_step").on('click', function(){
  deshabilitarBotonesEjecucion();
      removePanel();
    if(ast.isRunning){
      //ast.isRunning = false;
      ast.lastInstruction = 0;
      //return;
    }
    else{
      ast.isRunning = true;
    }
  ast.debug = true;
  ast.debugAutomatic = false;
  entradaDatosActivo =false;
  //if(ast.lastInstruction == 0){
    addPanel();
    editores.ejecutar();
  //}
  /*else{

    if(!entradaDatosActivo)
      editores.continuar();
  }*/
});

function deshabilitarBotonesEjecucion(){

    $('#calcular').prop('disabled',true);
    $('#step_to_step').prop('disabled',true);
    $('#detener').prop('disabled',false);
    ejecucionIniciar();
}

function habilitarBotonesEjecucion(){

    $('#calcular').prop('disabled',false);
    $('#step_to_step').prop('disabled',false);
    $('#detener').prop('disabled',true);
    ejecucionFrenar();
}

/** Al recargar página el botón detener estará deshabilitado */
//habilitarBotonesEjecucion();







function copiarEditorActual(){
  if($('#nuevaEntregaModal').data('tipo')==0)
    return;
  $('#codigoNuevaEntregaModal').val(editores.getEditorActual().editor.getValue());
  
}



$("#save_script_pc").on('click', function(){
  var elem = $(this)[0];
  elem.download = editores.getEditorActual().nombre+".nnumeral";
  elem.href = "data:application/octet-stream," 
                     + encodeURIComponent(editores.getEditorActual().editor.getValue());
  //$.toaster({ priority : 'info', title : 'Desarrollo', message : 'función en desarrolo'});
    
});

$("#formNewScriptModal").submit(function(e){
    editores.nuevoTab($('#nameNewScriptModal').val());
    $('#newScriptModal').modal('hide');
    return false;
  });


$("#estadisticasModalForm").submit(function(e){
    usuario.graficar("estadisticasModalBody",
      $("#semestreEstadisticasModal").val(),
      $("#graficoEstadisticasModal").val(),
      );
    return false;
  });








function removeit(){
  usuario.eliminarScript();
}

function modificarAula(){
  $('#nuevaAulaModal').modal('show');
}

function modificarProyecto(){
  $('#nuevoProyectoModal').modal('show');
}

/**
  Para tratar ip del modal de proyecto
**/  
function setIpInModal(ipI, ipF){
  var ip = '';
  if(ipI){
    ip = ipI.split('.');
    $('#ip11').val(ip[0]);
    $('#ip12').val(ip[1]);
    $('#ip13').val(ip[2]);
    $('#ip14').val(ip[3]);
  }
  if(ipF){
    ip = ipF.split('.');
    $('#ip21').val(ip[0]);
    $('#ip22').val(ip[1]);
    $('#ip23').val(ip[2]);
    $('#ip24').val(ip[3]);
  }
} 
function getIp1OfModal(){
  return Number($('#ip11').val())+'.'+Number($('#ip12').val())
  +'.'+Number($('#ip13').val())+'.'+Number($('#ip14').val());
}   
function getIp2OfModal(){
  return Number($('#ip21').val())+'.'+Number($('#ip22').val())
  +'.'+Number($('#ip23').val())+'.'+Number($('#ip24').val());
}   

function evaluarEntrega(){
  $('#nuevaEntregaModal').modal('show');
}
function abrirCodigoEntrega(){
  usuario.abrirEntrega();
}


$(function(){
    var includes = $('[data-include]');
    jQuery.each(includes, function(){
      var version = 'v=201805101128';
      //window.location.href.replace('index.html', '')+
      var file = 'modales/' + $(this).data('include') + '.html?'+version;
      $(this).load(file);
    });
  });


/*****************************************
      Login Modal
*****************************************/

$("#loginModalIngresar").submit(function(e){
    $('#loadingModal').modal('show');
    Usuario.login($('#usernameLoginModal').val(), $('#passwordLoginModal').val());
    return false;
  });

/*****************************************
      Registro Usuario Modal
*****************************************/
$("#registroModalForm").submit(function(e){
  if($("#claveRegistroModal").val() != $("#clave1RegistroModal").val()){
    alert('Ambas claves deben coincidir');
    return false;
  }
    $('#loadingModal').modal('show');
    Usuario.register($("#codigoRegistroModal").val(),
      $("#claveRegistroModal").val(),
      $("#nombreRegistroModal").val(),
      $("#correoRegistroModal").val(),
      $("#cedulaRegistroModal").val(),
      $("#carreraRegistroModal").val());
    return false;
});

/*****************************************
      Olvido Clave Modal
*****************************************/
$("#olvidoClaveModalForm").submit(function(e){
    $('#loadingModal').modal('show');
    Usuario.recuperarClave($("#correoOlvidoClaveModal").val());
    return false;
});



/*****************************************
      Registro Usuario Externo Modal
*****************************************/
$("#registroEModalForm").submit(function(e){
  if($("#claveRegistroEModal").val() != $("#clave1RegistroEModal").val()){
    alert('Ambas claves deben coincidir');
    return false;
  }
    $('#loadingModal').modal('show');
    Usuario.registerE($("#claveRegistroEModal").val(),
      $("#nombreRegistroEModal").val(),
      $("#correoRegistroEModal").val(),
      $("#cedulaRegistroEModal").val());
    return false;
});

/*****************************************
      Registro Usuario por Administrador Modal
*****************************************/
$("#registroAModalForm").submit(function(e){
  if($("#claveRegistroAModal").val() != $("#clave1RegistroAModal").val()){
    alert('Ambas claves deben coincidir');
    return false;
  }
  $('#loadingModal').modal('show');
  var id = null;
  if($('#registroAModal').data('id') != -1)
    id = $('#registroAModal').data('id');
    usuario.saveUsuario($("#nombreRegistroAModal").val(),
      $("#claveRegistroAModal").val(),
      $("#correoRegistroAModal").val(),
      $("#cedulaRegistroAModal").val(),
      $("#rolRegistroAModal").val(),
      $("#carreraRegistroAModal").val(),
      id);
    return false;
});

/*****************************************
      Parámetros Modal
*****************************************/
$("#parametrosModalForm").submit(function(e){
  $("#parametrosModalBody select").each(function(){
    usuario.updateParametro($(this).data('id'), $(this).val());
  });
  $("#parametrosModalBody input").each(function(){
    usuario.updateParametro($(this).data('id'), $(this).val());
  });
    return false;
});

/*****************************************
      Unirse a Aula Modal
*****************************************/
$("#buscarAulaModalForm").submit(function(e){
  $('#loadingModal').modal('show');
    usuario.joinAula($("#codigoBuscarAulaModal").val());
    return false;
});


/*****************************************
      Nuevo Semestre Modal
*****************************************/
$("#nuevoSemestreModalForm").submit(function(e){
  var id = null;
  if($('#nuevoSemestreModal').data('semestre') != -1)
    id = $('#nuevoSemestreModal').data('semestre');
    $('#loadingModal').modal('show');
    //Usuario.login($('#usernameLoginModal').val(), $('#passwordLoginModal').val());
    usuario.saveSemestre($("#nombreNuevoSemestreModal").val(), id);
    return false;
});


/*****************************************
      Nueva Aula Modal
*****************************************/
$("#nuevaAulaModalForm").submit(function(e){
  var id = null;
  if($('#nuevaAulaModal').data('aula') != -1)
    id = $('#nuevaAulaModal').data('aula');
    $('#loadingModal').modal('show');
    //Usuario.login($('#usernameLoginModal').val(), $('#passwordLoginModal').val());
    usuario.saveAula($("#nombreNuevaAulaModal").val(),
      id);
    return false;
});

/*****************************************
      Recuperar Aula Modal
*****************************************/
$("#recuperarAulaModalForm").submit(function(e){
    $('#loadingModal').modal('show');
    usuario.buscarParaRecuperarAula($("#fechaInicioRecuperarAulaModal").val()
      );
    return false;
});

/*****************************************
      Recuperar Proyecto Modal
*****************************************/
$("#recuperarProyectoModalForm").submit(function(e){
    $('#loadingModal').modal('show');
    usuario.buscarParaRecuperarProyecto($("#fechaInicioRecuperarProyectoModal").val(),
      $("#recuperarProyectoModal").data('idAula')
      );
    return false;
});

/*****************************************
      Nuevo Proyecto Modal
*****************************************/
$("#nuevoProyectoModalForm").submit(function(e){
  if($("#claveNuevoProyectoModal").val() != $("#clave1NuevoProyectoModal").val()){
    alert('Ambas claves deben coincidir');
    //$.toaster({ priority : 'warning', title : 'Clave', message : 'Ambas claves deben coincidir'});
    return false;
  }

    $('#loadingModal').modal('show');
    var id = null;
    if($('#nuevoProyectoModal').data('proyecto') != -1)
      id = $('#nuevoProyectoModal').data('proyecto');
    var validarIp = 0;
    if($('#validarIpNuevoProyectoModal').prop('checked') == true)
      validarIp = 1;
    usuario.saveProyecto($("#nombreNuevoProyectoModal").val(),
      $("#valorNuevoProyectoModal").val(),
      $("#enunciadoNuevoProyectoModal").val(),
      $("#fechaInicioNuevoProyectoModal").val(),
      $("#fechaFinNuevoProyectoModal").val(),
      validarIp,
      getIp1OfModal(),
      getIp2OfModal(),
      $("#claveNuevoProyectoModal").val(),
      id
      );
    return false;
});

/*****************************************
      Evaluar y Realizar Entrega Modal
*****************************************/
$("#nuevaEntregaModalForm").submit(function(e){

    $('#loadingModal').modal('show');
    if($('#nuevaEntregaModal').data('id') != -1){
      if($('#nuevaEntregaModal').data('tipo') == 0){
        usuario.evaluarEntrega(
          $('#nuevaEntregaModal').data('id'),
          $("#notaNuevaEntregaModal").val(),
          $("#comentariosNuevaEntregaModal").val()
        );
      }else{
      usuario.realizarEntrega(
        $("#codigoNuevaEntregaModal").val(),
        $('#nuevaEntregaModal').data('proyectoid'),
        $('#nuevaEntregaModal').data('id')
      );
      }

    }
    else
      usuario.realizarEntrega(
        $("#codigoNuevaEntregaModal").val(),
        $('#nuevaEntregaModal').data('proyectoid')
      );
    return false;

});

/*****************************************
      Buscar Usuario Modal
*****************************************/

$("#buscarUsuarioModalForm").submit(function(e){
    $('#loadingModal').modal('show');
    usuario.searchUsuario($("#buscarBuscarUsuarioModal").val());
    return false;
  });

/*****************************************
      Buscar Usuario Para Unir a Aula Modal
*****************************************/

$("#buscarUsuarioAModalForm").submit(function(e){
    $('#loadingModal').modal('show');
    usuario.joinUsuarioToAula($("#buscarBuscarUsuarioAModal").val(),
      $("#buscarUsuarioAModal").data('id'));
    return false;
  });




/*****************************************
      Panel de ejecución paso a paso
*****************************************/
function addPanel(){
      var region = 'east';
      var options = {
        region: region
      };
      if (region=='north' || region=='south'){
        options.height = 50;
      } else {
        options.width = 200;
        options.split = true;
        options.title = 'Paso a Paso';
        options.content = getTablaEjecucion();
      }
      $('#principal-layout').layout('add', options);

      $("#step_to_step_continue").on('click', function(){
        $("#step_to_step_continue").html('Siguiente punto de quiebre');

        if(!entradaDatosActivo && ast.isRunning){
          editores.continuar();
        }
        if(entradaDatosActivo){
          $.toaster({ priority : 'info', title : 'Leer', message : 'Ingrese valor'});
          entradaDatos();
        }
      });

      editores.ajustarDimensionesAncho();

      var aceEditor = editores.getEditorActual().editor;

    vaciarBreakPoints();
    editores.setBreakPoints([]);
    aceEditor.on("guttermousedown", function(e){ 
      var target = e.domEvent.target; 
      if (target.className.indexOf("ace_gutter-cell") == -1) 
        return; 
      if (!aceEditor.isFocused()) 
        return; 
      if (e.clientX > 25 + target.getBoundingClientRect().left) 
        return; 

      var row = e.getDocumentPosition().row;

      var breakPoints = editores.getBreakPoints();
      var existe = false;
      for (var i = 0; i < breakPoints.length; i++) {
        if(breakPoints[i].row == row){
          breakPoints.splice(i, 1);
          existe = true;
          break;
        }
      }
      if(!existe){
        e.editor.session.setBreakpoint(row);
        breakPoints.push({row: row, activado: false});
      }
      else{
        e.editor.session.clearBreakpoint(row);
      }
      e.stop() 
  }); 

function vaciarBreakPoints(){
  var aceEditor = editores.getEditorActual().editor;
  var breakPoints = editores.getBreakPoints();
  var existe = false;
  for (var i = 0; i < breakPoints.length; i++) {
    aceEditor.session.clearBreakpoint(breakPoints[i].row);
  }

}



/*
      aceEditor.on("click", function (e) {
        alert();
                var breakpointsArray = aceEditor.session.getBreakpoints();
                if(Object.keys(aceEditor.session.getBreakpoints()).length>0){
                    if(e.lines.length>1){
                        var breakpoint = parseInt(Object.keys(breakpointsArray)[0]);
                        var lines = e.lines.length -1;
                        var start = e.start.row;
                        var end = e.end.row;
                        if(e.action==='insert'){
                            console.log('new lines',breakpoint, start , end );
                            if(breakpoint>start ){
                                console.log('breakpoint forward');
                                aceEditor.session.clearBreakpoint(breakpoint);
                                aceEditor.session.setBreakpoint(breakpoint + lines);
                            }
                        } else if(e.action==='remove'){
                            console.log('removed lines',breakpoint, start , end);
                            if(breakpoint>start && breakpoint<end ){
                                console.log('breakpoint remove');
                                aceEditor.session.clearBreakpoint(breakpoint);
                            }
                            if(breakpoint>=end ){
                                console.log('breakpoint behind');
                                aceEditor.session.clearBreakpoint(breakpoint);
                                aceEditor.session.setBreakpoint(breakpoint - lines);
                            }
                        }
                    }
                }
            });*/
}


//Eventos de cambio de tamaño en el layout
$('#principal-layout').layout({
    onExpand: function(region){
        editores.ajustarDimensionesAncho();
    },
    onCollapse: function(region){
        editores.ajustarDimensionesAncho();
    },
});


function removePanel(){
  $('#principal-layout').layout('remove', 'east');
  editores.ajustarDimensionesAncho();
}


/*****************************************
    Tabla en panel de ejecución paso a paso
*****************************************/

function getTablaEjecucion(){
  var tabla = '<table id="tabla_ejecucion" class="easyui-datagrid" title="Variables" style="width:100%;height:70%"'+
      'data-options="singleSelect:true,collapsible:true,method:\'get\'">'+
    '<thead>'+
      '<tr>'+
        '<th data-options="field:\'varName\',width:80">Nombre</th>'+
        '<th data-options="field:\'varValue\',width:100">Valor</th>'+
      '</tr>'+
    '</thead>'+
  '</table>';
  var boton = '<div style="width:90%; height:20%;"><div class="row justify-content-md-center align-items-end" '
    +'style="height:100%;" >'+

    '<div style="margin-left:10%" class="alert-primary text-center">'+
    '<strong>Seleccione puntos de quiebre</strong></div>'+

    '<button id="step_to_step_continue" '+
    'class="btn btn-info btn-sm btn-xs" '+ 
    'style="margin-left:10%"><i class="fa fa-play"></i>Iniciar</button></div></div>';

  return tabla+boton;
}

function appendInTablaEjecucion(nombre, value){
  $('#tabla_ejecucion').datagrid('appendRow',{
    varName: nombre,
    varValue: value
  });
}

function updateInTablaEjecucion(index, value){
  $('#tabla_ejecucion').datagrid('updateRow',{
    index: index,
    row: {
      varValue: value
    }
  });
}


var makeCRCTable = function(){
    var c;
    var crcTable = [];
    for(var n =0; n < 256; n++){
        c = n;
        for(var k =0; k < 8; k++){
            c = ((c&1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
        }
        crcTable[n] = c;
    }
    return crcTable;
}

var crc32 = function(str) {
    str = Utf8Encode(str);
    var crcTable = window.crcTable || (window.crcTable = makeCRCTable());
    var crc = 0 ^ (-1);

    for (var i = 0; i < str.length; i++ ) {
        crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
    }

    return (crc ^ (-1)) >>> 0;
};

function Utf8Encode(string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";

    for (var n = 0; n < string.length; n++) {
        var c = string.charCodeAt(n);
        if (c < 128) {
            utftext += String.fromCharCode(c);
        } else if((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
        } else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
        }
    }
    return utftext;
};

var generarCrc32 = function(str) {
    $('#crcNuevaEntregaModal').val(crc32($('#codigoNuevaEntregaModal').val()));
};

var generarPdf = function(str) {
  //var doc = new jsPDF();
  //doc.setFontSize(12);
  var pdf = new jsPDF('p','in','letter')
, sizes = [12]
, fonts = [['Times','Roman']/*,['Helvetica',''], ['Times','Italic']*/]
, font, size, lines
, verticalOffset = 0.5 // inches on a 8.5 x 11 inch sheet.
, loremipsum = $("#enunciadoNuevoProyectoModal").val();

for (var i in fonts){
    if (fonts.hasOwnProperty(i)) {
        font = fonts[i]
        size = sizes[i]

        lines = pdf.setFont(font[0], font[1])
                    .setFontSize(size)
                    .splitTextToSize(loremipsum, 7.5);
        var vector = [];
//alert(lines.toString().split('\n').length);
//alert(lines);
        if(lines.toString().split('\n').length>60){
          var cant = 0;
          var cadena = '';
          var lineas = lines.split('\n');
          for (var i = 0; i < lineas.length; i++) {
            if(i%60==0 && i!=0){
                vector.push(cadena);
                cadena = '';
            }
            else{
              cadena += lineas[i]+'\n';
            }
            
          }

        }
        else{
          vector.push(lines);
        }
        // Don't want to preset font, size to calculate the lines?
        // .splitTextToSize(text, maxsize, options)
        // allows you to pass an object with any of the following:
        // {
        //  'fontSize': 12
        //  , 'fontStyle': 'Italic'
        //  , 'fontName': 'Times'
        // }
        // Without these, .splitTextToSize will use current / default
        // font Family, Style, Size.
        for (var i = 0; i < vector.length; i++) {
          if(i!=0)
            pdf.addPage();
          pdf.text(0.5, verticalOffset + size / 72, vector[i]);
        }

        

        verticalOffset += (lines.length + 0.5) * size / 72
    }
}

pdf.save('Test.pdf');
//  doc.text($("#enunciadoNuevoProyectoModal").val(), 10, 10)
 // doc.save('enunciado.pdf');
};



$('#temaEditor').on('change',function(){
  editores.cambiarTema();
});




/*

var editIndex = undefined;
    function endEditing(){
      if (editIndex == undefined){return true}
      if ($('#dg').datagrid('validateRow', editIndex)){
        $('#dg').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
      } else {
        return false;
      }
    }
    function onClickCell(index, field){
      if (editIndex != index){
        if (endEditing()){
          $('#dg').datagrid('selectRow', index)
              .datagrid('beginEdit', index);
          var ed = $('#dg').datagrid('getEditor', {index:index,field:field});
          if (ed){
            ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
          }
          editIndex = index;
        } else {
          setTimeout(function(){
            $('#dg').datagrid('selectRow', editIndex);
          },0);
        }
      }
    }
    function onEndEdit(index, row){
      var ed = $(this).datagrid('getEditor', {
        index: index,
        field: 'productid'
      });
      row.productname = $(ed.target).combobox('getText');
    }
    function append(){
      if (endEditing()){
        $('#dg').datagrid('appendRow',{status:'P'});
        editIndex = $('#dg').datagrid('getRows').length-1;
        $('#dg').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
      }
    }
    function removeit(){
      if (editIndex == undefined){return}
      $('#dg').datagrid('cancelEdit', editIndex)
          .datagrid('deleteRow', editIndex);
      editIndex = undefined;
    }
    function accept(){
      if (endEditing()){
        $('#dg').datagrid('acceptChanges');
      }
    }
    function reject(){
      $('#dg').datagrid('rejectChanges');
      editIndex = undefined;
    }
    function getChanges(){
      var rows = $('#dg').datagrid('getChanges');
      alert(rows.length+' rows are changed!');
    }

*/


/*
$.toaster({ priority : 'success', title : 'Title', message : 'Your message here'});

$.toaster({ priority : 'info', title : 'Title', message : 'Your message here'});

$.toaster({ priority : 'warning', title : 'Title', message : 'Your message here'});

$.toaster({ priority : 'danger', title : 'Title', message : 'Your message here'});
*/