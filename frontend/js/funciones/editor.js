

/*

Autocomplete is now an official part of the API. Enabling it takes 3 lines of code:

ace.require("ace/ext/language_tools");
var editor = ace.edit("editor");
editor.setOptions({
    enableBasicAutocompletion: true
});
Depending on your setup with require-js, you may also need to include an additional javascript file
 in the html for your page:
 <script src="ace/ext-language_tools.js"></script>
 */

function Editor(nombreTabs){
  this.editoresCant = 0;
  /**
  Cada editor tiene:
  id: id del script en bd (si es que existe), default: null
  editor: instancia de ace edit donde se encuentra el código
  */
  this.editores = [];
  this.cxt = {};
  this.breakPoints = [];
  this.nombreTabs = nombreTabs;
  this.inicio = 0; //Línea donde está la palabra ALGORITMO
  this.program = null;
  this.iniciar();

}

Editor.prototype.iniciar = function(){
    var self = this; 
    /*ace.require("ace/ext/language_tools");
    var editor = this.getPlantillaEditor(0);
    this.editores.push({id: null, editor: editor, nombre: 'Tab1'});*/

  $('#'+this.nombreTabs).tabs({
    border:false,
    onSelect:function(title, index){

    },
    onBeforeClose: function(title,index){
      var target = this;
      var opts = $(target).tabs('options');
      var bc = opts.onBeforeClose;
      opts.onBeforeClose = function(){};  // allowed to close now
      //reescribe arriba para poder cerrar sin llamar la función
      $(target).tabs('close',index);
      $('#editor'+index).remove();
      self.editores.splice(index, 1);
      opts.onBeforeClose = bc;  // restore the event function
      /*$.messager.confirm('Confirm','Are you sure you want to close '+title,function(r){
    if (r){
      var opts = $(target).tabs('options');
      var bc = opts.onBeforeClose;
      opts.onBeforeClose = function(){};  // allowed to close now
      $(target).tabs('close',index);
      opts.onBeforeClose = bc;  // restore the event function
    }
  });*/
  return false; // prevent from closing
  }
  });


    this.nuevoTab('programa');

  
}

Editor.prototype.getEditores = function(){

      return this.editores;
}

Editor.prototype.setIdNull = function(id){
  for (var i = 0; i < this.editores.length; i++) {
    if(this.editores[i].id){
      this.editores[i].id = null;
    }
  }
}

Editor.prototype.setCxt = function(cxt){
  this.cxt = cxt;
}

Editor.prototype.nuevoTab = function(nombre, codigo = null, id = null){
  $('#'+this.nombreTabs).tabs('add',{
    title: nombre,
    index: this.editoresCant,
    content: this.getPreBasico(codigo),
    closable: true,
  });
  var editor = this.getPlantillaEditor();
  this.editores.push({id: id, editor: editor, nombre: nombre});
  this.editoresCant++;
  editor.focus();
  if(this.editoresCant != 1)
    $.toaster({ priority : 'success', title : 'Nuevo script', message : 'Creado script '+nombre});
  $(window).resize();
}

Editor.prototype.getPreBasico = function(codigo = null){
  var programa = 'ALGORITMO miPrograma\n    INICIO\n'+
                  '        escribirnl "Hola mundo"\n'+
                  '    FIN';
          
  return '<pre id="editor'+this.editoresCant+'" class="editor">'+(codigo?codigo:programa)+'</pre>';

}

Editor.prototype.getEditorActual = function(){
  var editor = this.editores[0];
      var tab = $('#'+this.nombreTabs).tabs('getSelected');
      if (tab){
        var index = $('#'+this.nombreTabs).tabs('getTabIndex', tab);
        editor = this.editores[index];
      }
          
  return editor;

}

Editor.prototype.getEditorActualPos = function(){
  var pos = 0;
      var tab = $('#'+this.nombreTabs).tabs('getSelected');
      if (tab){
        var index = $('#'+this.nombreTabs).tabs('getTabIndex', tab);
        pos = index;
      }
  return pos;
}

Editor.prototype.getPlantillaEditor = function(i = this.editoresCant){
  var editor = ace.edit("editor"+i);
  //editor.setTheme("ace/theme/twilight");
  editor.setTheme("ace/theme/"+$("#temaEditor").val());
  editor.session.setMode("ace/mode/nnumeral");
  editor.setOptions({
      enableBasicAutocompletion: true,
      enableSnippets: true,
      enableLiveAutocompletion: true
    });
  return editor;

}

Editor.prototype.cambiarTema = function(){
  for (var i = 0; i < this.editores.length; i++) {
    this.editores[i].editor.setTheme("ace/theme/"+$("#temaEditor").val());
  }
}

Editor.prototype.ejecutar = function(){
  var self = this; 
  this.cxt = {};
  ast.stop = false;
  ast.position = [];
  ast.position[''] = 0;
  ast.stringp = [];
  ast.stringp.push('');
  ast.actualInstruction = 0;
  ast.lastInstruction = 0;
  ast.declaratedVariables = false;
  var editor = this.getEditorActual().editor;
  lexer.setInput(editor.getValue());
  //parser.parse(lexer);
  program = parser.parse(lexer);
  
  //ejecucion.setValue('Ejecución:\n');
  ejecucion.setValue('');
  program.eval(this.cxt);

  if(ast.debugAutomatic && ast.isRunning && !entradaDatosActivo){
    setTimeout(function(){ 
      ast.debug = false;
        self.continuar();
      }, 0);
  }

}

Editor.prototype.continuar= function(){
  var self = this; 
  if(!ast.isRunning)
    return;
  ast.stop = false;
  ast.stringp = [];
  ast.stringp.push('');
  ast.actualInstruction = 0;
  ast.breakPointsStop = false;
  var editor = this.getEditorActual().editor;
  program.eval(this.cxt);

  if(ast.debugAutomatic && ast.isRunning && !entradaDatosActivo){
    setTimeout(function(){ 
      ast.debug = false;
        self.continuar();
      }, 0);
  }
  
  //para paso a paso fluído
  if(ast.debug && ast.isRunning && !entradaDatosActivo && !ast.breakPointsStop){
    setTimeout(function(){ 
        self.continuar();
      }, 0);
  }


}

Editor.prototype.marcarLinea = function(numero){
  var editor = this.getEditorActual().editor;
  session = editor.getSession();
  editor.gotoLine(numero, session.getLine(numero-1).length);
  editor.selection.moveCursorToPosition({row: numero, column: 0});
  editor.selection.selectLine();
}

Editor.prototype.getBreakPoints = function(){
  return this.breakPoints;
}

Editor.prototype.setBreakPoints = function(breakPoints){
  this.breakPoints = breakPoints;
}

Editor.prototype.ajustarDimensiones = function(alto, dimensionTotal){
  var editor = alto;
  var firstPanel = dimensionTotal;
  $('.editor').css('height', (firstPanel*editor)-20+'px', 'important');
  $('.editor').css('max-height', (firstPanel*editor)-20+'px', 'important');
  //ancho automático se actualiza al final y eso daña diseño
  //return;
  var cantReal = 0;
  //esta función
  //editor.resize()
  for (var i = 0; i < this.editores.length; i++) {
    this.editores[i].editor.resize();
  }
  /*for (var i = 0; i < this.editoresCant; i++) {
    if(!$('#editor'+i).length)
      continue;
    var editor = this.getPlantillaEditor(i);
    this.editores[cantReal++].editor = editor;
  }*/
}

Editor.prototype.ajustarDimensionesAncho = function(){
  for (var i = 0; i < this.editores.length; i++) {
    this.editores[i].editor.resize();
  }
}



