ace.define("ace/snippets/nnumeral",[], function(require, exports, module) {
"use strict";

exports.snippetText = "# raiz\n\
snippet raiz\n\
	raiz(numero)\n\
# aleatorio\n\
snippet aleatorio\n\
	aleatorio(numero)\n\
# log\n\
snippet log\n\
	log(numero)\n\
# ln\n\
snippet ln\n\
	ln(numero)\n\
# exp\n\
snippet exp\n\
	exp(numero)\n\
# abs\n\
snippet abs\n\
	abs(numero)\n\
# sen\n\
snippet sen\n\
	sen(numero)\n\
# asen\n\
snippet asen\n\
	asen(numero)\n\
# cos\n\
snippet cos\n\
	cos(numero)\n\
# acos\n\
snippet acos\n\
	acos(numero)\n\
# tan\n\
snippet tan\n\
	tan(tan)\n\
# atan\n\
snippet atan\n\
	atan(numero)\n\
# truncar\n\
snippet truncar\n\
	truncar(numero)\n\
# entero\n\
snippet entero\n\
	entero(numero)\n\
# real\n\
snippet real\n\
	real(numero)\n\
# limpiar\n\
snippet limpiar\n\
	limpiar()\n\
# redondear\n\
snippet redondear\n\
	redondear(numero, cantDecimales)\n\
# cadconcatenar\n\
snippet cadconcatenar\n\
	cadconcatenar(cad1, cad2)\n\
# cadinsertar\n\
snippet cadinsertar\n\
	cadinsertar(cad1, cad2, posicion)\n\
# cadremover\n\
snippet cadremover\n\
	cadremover(cad1, cad2, posicion)\n\
# cadreemplazar\n\
snippet cadreemplazar\n\
	cadreemplazar(cad1, cad2, posicion)\n\
# cadextraer\n\
snippet cadextraer\n\
	cadextraer(cad1, cad2, posicion)\n\
# cadbuscar\n\
snippet cadbuscar\n\
	cadbuscar(cad1, cad2, posicion)\n\
# ALGORITMO estructura básica\n\
snippet ALGORITMO\n\
	ALGORITMO ${1:nombreAlgoritmo}\n\
		VAR \n\
			ENTERO numero\n\
			REAL numero2\n\
		CONST \n\
			ENTERO numeroc := 25\n\
			REAL numerocc := 34.5\n\
		INICIO \n\
		//sentencias\n\
		FIN\n\
# ALGORITMO estructura básica con funciones\n\
snippet ALGORITMOF\n\
	ALGORITMO ${1:nombreAlgoritmo}\n\
		VAR \n\
			ENTERO numero\n\
			REAL numero2\n\
		CONST \n\
			ENTERO numeroc := 25\n\
			REAL numerocc := 34.5\n\
		FUNCION ${2:nombreFuncion}()\n\
			INICIO \n\
				escribirnl \"función\"\n\
			FIN FUNCION \n\
		INICIO \n\
		//sentencias\n\
		FIN\n\
# FUNCION sin parámetros ni retorno\n\
snippet FUNCION\n\
	FUNCION ${1:nombreFuncion}()\n\
		INICIO \n\
			//sentencias\n\
		FIN FUNCION \n\
# FUNCION con parámetros pero sin retorno\n\
snippet FUNCIONP\n\
	FUNCION ${1:nombreFuncion}(ENTERO param1, REAL param2)\n\
		INICIO \n\
			//sentencias\n\
		FIN FUNCION \n\
# FUNCION sin parámetros pero con retorno\n\
snippet FUNCIONR\n\
	FUNCION ENTERO ${1:nombreFuncion}(ENTERO param1, REAL param2)\n\
		INICIO \n\
			//sentencias\n\
			retorno numero\n\
		FIN FUNCION \n\
# para x desde 0 hasta n hacer ... fin para\n\
snippet para\n\
	para ${1:x} desde 0 hasta ${2:n} hacer\n\
		$1\n\
	fin para\n\
# para x desde 0 hasta n paso 1 hacer ... fin para\n\
snippet parap\n\
	para ${1:x} desde 0 hasta ${2:n} paso 1 hacer\n\
		$1\n\
	fin para\n\
# mientras (condicion) hacer fin mientras\n\
snippet mientras\n\
	mientras (i<j) hacer\n\
		//sentencias\n\
	fin mientras\n\
# repita ... hasta(condicion)\n\
snippet repita\n\
	repita\n\
		//sentencias\n\
	hasta(i<j)\n\
# si (VERDADERO) entonces ... fin si\n\
snippet si\n\
	si (VERDADERO) entonces\n\
		//sentencias\n\
	fin si\n\
# si (VERDADERO) entonces ... sino ... fin si\n\
snippet siS\n\
	si (VERDADERO) entonces\n\
		//sentencias\n\
	sino\n\
		//sentencias\n\
	fin si\n\
# seleccionar id caso 1 ... caso 2 ... fin seleccionar\n\
snippet seleccionar\n\
	seleccionar variable\n\
		caso 1\n\
			//sentencias\n\
		caso 2\n\
			//sentencias\n\
	fin seleccionar\n\
# seleccionar id caso 1 ... caso 2 ... sino ... fin seleccionar\n\
snippet seleccionarS\n\
	seleccionar variable\n\
		caso 1\n\
			//sentencias\n\
		caso 2\n\
			//sentencias\n\
		sino\n\
			//sentencias\n\
	fin seleccionar\n\
# leer variable \n\
snippet leer\n\
	leer variable\n\
# leer variable, variable1 \n\
snippet leerV\n\
	leer variable, variable1\n\
# escribir variable_o_expresion \n\
snippet escribir\n\
	escribir variable_o_expresion\n\
# escribir variable, variable1 \n\
snippet escribirV\n\
	escribir variable_o_expresion, variable_o_expresion1\n\
# escribirnl variable_o_expresion \n\
snippet escribirnl\n\
	escribirnl variable_o_expresion\n\
# escribirnl variable, variable1 \n\
snippet escribirnlV\n\
	escribirnl variable_o_expresion, variable_o_expresion1\n\
";
exports.scope = "nnumeral";

});
                (function() {
                    ace.require(["ace/snippets/nnumeral"], function(m) {
                        if (typeof module == "object") {
                            module.exports = m;
                        }
                    });
                })();
            