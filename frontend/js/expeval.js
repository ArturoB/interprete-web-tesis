

var astclasses = ast;
//actualInstuction

astclasses.Program.prototype.eval = function(cxt){
    astclasses.cicleDeep = 0;
    astclasses.exit = false;
    astclasses.totalVariables = 0;
    astclasses.actualLine = 0;
    astclasses.isFunctionPermited = true;
    astclasses.actualFunction = '';
    astclasses.cantInstruction = 0;
    cxt = cxt || {};
    astclasses.functions = this.functions;
    if(astclasses.lastInstruction == 0){
        astclasses.lastInstruction = -1;
        astclasses.validateFunctions();
    }
    //sentencias del programa

    for(var i=astclasses.getPosition();i<this.statements.length;i++){
        if(!(this.statements[i] instanceof astclasses.ConstantsCreation) 
            && !(this.statements[i] instanceof astclasses.VariablesCreation)){
            if(astclasses.debug && !astclasses.declaratedVariables){
                astclasses.declaratedVariables = true;
                astclasses.stop = true;
                entradaDatos();
                return;
            }
        }

        astclasses.isPos(this.statements[i]);
        this.statements[i].eval(cxt);
        if(astclasses.stop){
            entradaDatos();
            return;
        }
        astclasses.updatePosition();
        if(!(this.statements[i] instanceof astclasses.ConstantsCreation) 
            && !(this.statements[i] instanceof astclasses.VariablesCreation)){
            if(astclasses.debugMode(this.statements[i])){
                entradaDatos();
                return;
            }
        }
    }
    ejecucion.setValue(ejecucion.getValue()+'\nPrograma Finalizado Correctamente...');
    astclasses.lastInstruction = 0;
    astclasses.isRunning = false;
    entradaDatos();
    habilitarBotonesEjecucion();
    //if(!astclasses.stop)
     //   ejecucion.setValue(ejecucion.getValue()+'\nEjecución Finalizada');
};

astclasses.VariablesCreation.prototype.eval = function(cxt){
    if(!(astclasses.actualInstruction >= astclasses.lastInstruction) || astclasses.stop)
        return;
    cxt = cxt || {};
    for(var i=0;i<this.statements.length;i++){
        var tipo = this.statements[i][0].type;
        for (var j = 0; j < this.statements[i].length; j++) {
            if(cxt[this.statements[i][j].id])
                astclasses.endProgram('LA VARIABLE '
                    +this.statements[i][j].id+' NO PUEDE DECLARARSE NUEVAMENTE');
            this.statements[i][j].type = tipo;
            if(this.statements[i][j].isArray){
                if(this.statements[i][j].vectorDimensiones.length > 2)
                    astclasses.endProgram('LOS ARREGLOS '
                        +'NO PUEDEN TENER MÁS DE DOS DIMENSIONES');
            }
            /*else{
                var rightExp = this.statements[i][j].getType(cxt);
                var leftExp = tipo;
                if(tipo != rightExp){
                    if(astclasses.doCastDataType(rightExp) == 'ENTERO' && leftExp == 'CADENA'){
                    }
                    else if(rightExp == 'CARACTER' && leftExp == 'CADENA'){
                    }
                    else if(astclasses.doCastDataType(rightExp) == 'ENTERO' 
                        && astclasses.doCastDataType(leftExp) == 'ENTERO'){
                    }
                    else{
                        astclasses.endProgram('En declaración de variables se esperaba '
                            +leftExp+' pero se obtuvo '+rightExp);
                        return;
                    }
                }

            }*/
    
            cxt[this.statements[i][j].id] = {
                valor : (this.statements[i][j].isArray?[]:this.statements[i][j].eval(cxt)),
                tipo : tipo,
                vectorDimensiones: this.statements[i][j].vectorDimensiones,
                isArray: this.statements[i][j].isArray,
                var: true,
                index: astclasses.totalVariables++,

            };
            appendInTablaEjecucion(this.statements[i][j].id,this.statements[i][j].isArray?[]:this.statements[i][j].eval(cxt));
        }
        
    }
};

astclasses.VariableCreation.prototype.eval = function(cxt){
    if(!(astclasses.actualInstruction >= astclasses.lastInstruction) || astclasses.stop)
        return;
    cxt = cxt || {};
    if(this.defaultValue == null){
        switch (this.type){
            case "ENTERO":
            case "REAL":
                        return 0;
                        break;
            case "CADENA":
            case "CARACTER":
                        return '';
                        break;
            case "LOGICO":
                        return 'FALSO';
                        break;
        }
    }
    var rightExp = this.defaultValue.getType(cxt);
    var leftExp = this.type;
    if(leftExp != rightExp){
        if(astclasses.doCastDataType(rightExp) == 'ENTERO' && leftExp == 'CADENA'){
            
        }
        else if(rightExp == 'CARACTER' && leftExp == 'CADENA'){
        }
        else if(astclasses.doCastDataType(rightExp) == 'ENTERO' 
            && astclasses.doCastDataType(leftExp) == 'ENTERO'){
        }
        else{
            astclasses.endProgram('En declaración de variables se esperaba '
                +leftExp+' pero se obtuvo '+rightExp);
                return;
        }

    }

    return astclasses.doCast(this.defaultValue.eval(cxt), this.type);    
};

astclasses.ConstantsCreation.prototype.eval = function(cxt){
    if(!(astclasses.actualInstruction >= astclasses.lastInstruction) || astclasses.stop)
        return;
    cxt = cxt || {};
    for(var i=0;i<this.statements.length;i++){
        var tipo = this.statements[i][0].type;
        for (var j = 0; j < this.statements[i].length; j++) {
            if(cxt[this.statements[i][j].id])
                astclasses.endProgram('LA VARIABLE '
                    +this.statements[i][j].id+' NO PUEDE DECLARARSE NUEVAMENTE');
            this.statements[i][j].type = tipo;
            cxt[this.statements[i][j].id] = {
                valor : this.statements[i][j].eval(cxt),
                tipo : tipo,
                vectorDimensiones: [],
                isArray: false,
                var: false,

            };
        }
    }
};

astclasses.IfStruct.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    cxt = cxt || {};
    var statements = this.ifstmts;
    astclasses.updateStringp('-if');
    
    if(this.condition.getType(cxt) != 'LOGICO'){
        astclasses.endProgram('Condición debe ser LOGICA, pero se obtuvo '+this.condition.getType(cxt));
        return;
    }

    astclasses.isFunctionPermited = false;
    var condition = this.condition.eval(cxt);
    astclasses.isFunctionPermited = true;
    if(!astclasses.isStringParam('condition')){
        astclasses.setStringParam('condition', condition);

    }
    else{
        condition = astclasses.getStringParam('condition');
    }

    if(condition == "FALSO")
        statements = this.elsestmts;

    
    

    for(var i=astclasses.getPosition();i<statements.length;i++){
        astclasses.isPos(statements[i]);
        statements[i].eval(cxt);
        astclasses.updatePosition();
        if(astclasses.exit == true)
            break;
        if(astclasses.debugMode(statements[i])){
            return;
        }
    }
    astclasses.setStringParam('condition', '');
    astclasses.resetPosition();

};

astclasses.SwitchStruct.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    cxt = cxt || {};
    astclasses.updateStringp('-switch');
    var condition = this.id.eval(cxt);
    var sino = false;
    if(!astclasses.isStringParam('conditionj')){
        astclasses.setStringParam('condition', condition);//a veces es vacio e daña validación
        astclasses.setStringParam('conditionj', '25');//para poder validar
        astclasses.setStringParam('conditionElse', false);
    }
    else{
        condition = astclasses.getStringParam('condition');
        sino = astclasses.getStringParam('conditionElse');
    }

    for (var i = 0; i < this.cases.length && !sino; i++) {
        astclasses.isFunctionPermited = false;
        var caseValue = this.cases[i].exp.eval(cxt);
        astclasses.isFunctionPermited = true;
        if(caseValue == condition){
            for(var j=astclasses.getPosition();j<this.cases[i].stmts.length;j++){
                astclasses.isPos(this.cases[i].stmts[j]);
                this.cases[i].stmts[j].eval(cxt);
                astclasses.updatePosition();
                    if(astclasses.exit == true)
                        break;
                if(astclasses.debugMode(this.cases[i].stmts[j])){
                    return;
                }
            }
            astclasses.setStringParam('condition', condition);
            astclasses.setStringParam('conditionj', '');
            astclasses.setStringParam('conditionElse', false);
            astclasses.resetPosition();
            return;
        }
    }
    astclasses.setStringParam('conditionElse', true);
    for(var j=astclasses.getPosition();j<this.elseCase.length;j++){
        astclasses.isPos(this.elseCase[j]);
        this.elseCase[j].eval(cxt);
        astclasses.updatePosition();
        if(astclasses.exit == true)
            break;
        if(astclasses.debugMode(this.elseCase[j])){
            return;
        }
    }
    astclasses.setStringParam('condition', condition);
    astclasses.setStringParam('conditionj', '');
    astclasses.setStringParam('conditionElse', false);
    astclasses.resetPosition();
};

astclasses.ForStruct.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    astclasses.cicleDeep++;
    cxt = cxt || {};
    astclasses.updateStringp('-for');

    var statements = this.stmts;
    astclasses.isFunctionPermited = false;
    var initialA = this.initial.eval(cxt);
    astclasses.isFunctionPermited = true;
    if(!astclasses.isStringParam('initial')){
        astclasses.setStringParam('initial', initialA);
        astclasses.isFunctionPermited = false;
        this.final.eval(cxt);
        this.step.eval(cxt);
        astclasses.isFunctionPermited = true;
    }
    else{
        initialA = astclasses.getStringParam('initial');
    }

    if(!astclasses.variableExist(this.id.name, cxt, this.isArray))
        return;
    if(this.initial.getType(cxt) != 'ENTERO' 
        || this.final.getType(cxt) != 'ENTERO'
        || this.step.getType(cxt) != 'ENTERO'){
        astclasses.endProgram('Parámetros del ciclo para deben ser de tipo ENTERO');
        return;
    }

    for (var aux = initialA; aux != this.final.eval(cxt)+this.step.eval(cxt) && !astclasses.stop; aux = aux+this.step.eval(cxt)) {
        if(this.isArray){
            cxt[astclasses.getIdentifierName(this.id.name, cxt)].valor[astclasses.getDimension(cxt, this.id.name, this.id.vectorDimensiones)] = aux;
        }
        else cxt[astclasses.getIdentifierName(this.id.name, cxt)].valor = aux;

        if(cxt[astclasses.getIdentifierName(this.id.name, cxt)].index >= 0
            && cxt[astclasses.getIdentifierName(this.id.name, cxt)].var){
            updateInTablaEjecucion(cxt[astclasses.getIdentifierName(this.id.name, cxt)].index,
            cxt[astclasses.getIdentifierName(this.id.name, cxt)].valor.toString());
        }

        astclasses.setStringParam('initial', aux);

        astclasses.updateStringp('-for');
        for(var j=astclasses.getPosition();j<statements.length;j++){
            astclasses.isPos(statements[j]);
            statements[j].eval(cxt);
            if(astclasses.stop)
                return;
            astclasses.updatePosition();
            if(astclasses.exit == true)
                break;
            if(astclasses.debugMode(statements[j])){
                return;
            }
        }

        astclasses.resetPosition();
        if(astclasses.exit == true)
            break;
    }
    if(this.isArray){
        cxt[astclasses.getIdentifierName(this.id.name, cxt)].valor[astclasses.getDimension(cxt, this.id.name, this.id.vectorDimensiones)] = aux;
    }
    else cxt[astclasses.getIdentifierName(this.id.name, cxt)].valor = aux;

    if(cxt[astclasses.getIdentifierName(this.id.name, cxt)].index >= 0
     && cxt[astclasses.getIdentifierName(this.id.name, cxt)].var){
        updateInTablaEjecucion(cxt[astclasses.getIdentifierName(this.id.name, cxt)].index,
            cxt[astclasses.getIdentifierName(this.id.name, cxt)].valor.toString());
    }

    astclasses.setStringParam('initial', '');
    astclasses.resetPosition();
    astclasses.cicleDeep--;
    astclasses.exit = false;
};

astclasses.WhileStruct.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    astclasses.cicleDeep++;
    cxt = cxt || {};
    var statements = this.stmts;

    
    if(this.condition.getType(cxt) != 'LOGICO'){
        astclasses.endProgram('Condición debe ser LOGICA, pero se obtuvo '+this.condition.getType(cxt));
        return;
    }

    astclasses.updateStringp('-while');

    astclasses.isFunctionPermited = false;
    var condition = this.condition.eval(cxt);
    astclasses.isFunctionPermited = true;

    if(!astclasses.isStringParam('condition')){
        astclasses.setStringParam('condition', condition);
    }
    else{
        condition = astclasses.getStringParam('condition');
    }


    while(condition == "VERDADERO" && !astclasses.stop){
        astclasses.updateStringp('-while');
        for(var j=astclasses.getPosition();j<statements.length;j++){
            astclasses.isPos(statements[j]);
            statements[j].eval(cxt);
            if(astclasses.stop)
                return;
            astclasses.updatePosition();
            if(astclasses.exit == true)
                break;
            if(astclasses.debugMode(statements[j])){
                return;
            }
        }
        astclasses.resetPosition();
        if(astclasses.exit == true)
            break;
        condition = this.condition.eval(cxt);
        astclasses.setStringParam('condition', condition);

    }
    astclasses.resetPosition();
    astclasses.cicleDeep--;
    astclasses.exit = false;
};

astclasses.RepeatUntilStruct.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    astclasses.cicleDeep++;
    cxt = cxt || {};
    var statements = this.stmts;

    
    if(this.condition.getType(cxt) != 'LOGICO'){
        astclasses.endProgram('Condición debe ser LOGICA, pero se obtuvo '+this.condition.getType(cxt));
        return;
    }
    var condition = null;
    do{
        astclasses.updateStringp('-dowhile');
        for(var j=astclasses.getPosition();j<statements.length;j++){
            astclasses.isPos(statements[j]);
            statements[j].eval(cxt);
            if(astclasses.stop)
                return;
            astclasses.updatePosition();
            if(astclasses.exit == true)
                break;
            if(astclasses.debugMode(statements[j])){
                return;
            }
        }  
        astclasses.resetPosition();
        if(astclasses.exit == true)
            break;
        astclasses.isFunctionPermited = false;
        condition = this.condition.eval(cxt)
        astclasses.isFunctionPermited = true;
    }while(condition == "FALSO"  && !astclasses.stop);
    astclasses.cicleDeep--;
    astclasses.exit = false;
};

astclasses.Assignment.prototype.eval = function(cxt){
    if(!(astclasses.actualInstruction >= astclasses.lastInstruction) || astclasses.stop)
        return;
    if(!astclasses.variableExist(this.id, cxt, this.isArray))
        return;
    var rightExp = this.exp.getType(cxt);
    var leftExp = cxt[astclasses.getIdentifierName(this.id, cxt)].tipo;
    if(leftExp != rightExp){
        if(astclasses.doCastDataType(rightExp) == 'ENTERO' && leftExp == 'CADENA'){
            
        }
        else if(rightExp == 'CARACTER' && leftExp == 'CADENA'){
        }
        else if(astclasses.doCastDataType(rightExp) == 'ENTERO' 
            && astclasses.doCastDataType(leftExp) == 'ENTERO'){
        }
        else{
            astclasses.endProgram('Se esperaba '+leftExp+' pero se obtuvo '+rightExp);
                return;
        }

    }
    if(cxt[astclasses.getIdentifierName(this.id, cxt)].var){
        if(this.isArray){
            cxt[astclasses.getIdentifierName(this.id, cxt)].valor[astclasses.getDimension(cxt, this.id, this.vectorDimensiones)] = astclasses.doCast(this.exp.eval(cxt), leftExp);
        }
        else
            cxt[astclasses.getIdentifierName(this.id, cxt)].valor = astclasses.doCast(this.exp.eval(cxt), leftExp);
    }
    if(cxt[astclasses.getIdentifierName(this.id, cxt)].index >= 0
     && cxt[astclasses.getIdentifierName(this.id, cxt)].var){
        updateInTablaEjecucion(cxt[astclasses.getIdentifierName(this.id, cxt)].index,
            cxt[astclasses.getIdentifierName(this.id, cxt)].valor.toString());
    }

};

astclasses.Print.prototype.eval = function(cxt){
    if(!(astclasses.actualInstruction >= astclasses.lastInstruction) || astclasses.stop)
        return;
    //alert();
    astclasses.updateStringp('-print');
    for(var i=astclasses.getPosition();i<this.exps.length;i++){
        var print = this.exps[i].eval(cxt);
        var print = String(print).replace(/\\n/g,'\n')
                        .replace(/\\b/g,'\b')
                        .replace(/\\t/g,'\t')
                        .replace(/\\r/g,'\r')
                        .replace(/\\f/g,'\f')
                        .replace(/\\'/g,'\'')
                        .replace(/\\"/g,'\"');
        if(this.exps[i].getType(cxt) == ''){
            print = '';
        }
        if(astclasses.stop)
            return;
        ejecucion.setValue(ejecucion.getValue()+print);
        astclasses.updatePosition();
    }
    if(this.nl)
        ejecucion.setValue(ejecucion.getValue()+'\n');
    astclasses.resetPosition();
    //frenar para que sea fluido con la ayuda de astclasses.debugAutomatic
    //astclasses.debug = true;
    astclasses.cantInstruction = 500;
};

astclasses.Read.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    astclasses.updateStringp('-read');
    for(var i=astclasses.getPosition();i<this.exps.length;i++){
        /*astclasses.actualInstruction++;
        //alert(astclasses.actualInstruction +' '+ astclasses.lastInstruction);
        if(!(astclasses.actualInstruction >= astclasses.lastInstruction))
            continue;

        if(!(astclasses.actualInstruction == astclasses.lastInstruction)){
            if(!astclasses.variableExist(this.exps[i].name, cxt))
                return;
            astclasses.lastInstruction = astclasses.actualInstruction;
            astclasses.stop = true;
            //editores.setCxt(cxt);
            activarEntraDatos();
            return;
        }*/
        if(astclasses.getPosition() == astclasses.getPositionR()){
            if(!astclasses.variableExist(this.exps[i].name, cxt, this.exps[i].isArray))
                return;
            astclasses.updatePositionR();
            astclasses.stop = true;
            //editores.setCxt(cxt);
            activarEntraDatos();
            return;
            //return;
        }

        if(!astclasses.variableExist(this.exps[i].name, cxt, this.exps[i].isArray))
            return;

        //var aux = prompt('Ingrese valor de '+this.exps[i].name+': ','');
        var aux = getValorEjecucion();
            //alert(getValorEjecucion());
        aux = astclasses.doCast(aux, cxt[astclasses.getIdentifierName(this.exps[i].name, cxt)].tipo);
        //alert(Number(aux));

        if(cxt[astclasses.getIdentifierName(this.exps[i].name, cxt)].var){
            if(this.exps[i].isArray){
                cxt[astclasses.getIdentifierName(this.exps[i].name, cxt)].valor[astclasses.getDimension(cxt, this.exps[i].name, this.exps[i].vectorDimensiones)] = aux;
            }
            else
                cxt[astclasses.getIdentifierName(this.exps[i].name, cxt)].valor = aux;
        }

        if(cxt[astclasses.getIdentifierName(this.exps[i].name, cxt)].index >= 0
            && cxt[astclasses.getIdentifierName(this.exps[i].name, cxt)].var){
            updateInTablaEjecucion(cxt[astclasses.getIdentifierName(this.exps[i].name, cxt)].index,
            cxt[astclasses.getIdentifierName(this.exps[i].name, cxt)].valor.toString());
        }
        astclasses.updatePosition();
    }
    astclasses.finishRead = false;
    astclasses.resetPositionR();
    astclasses.resetPosition();
};

astclasses.AddExp.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    return this.e1.eval(cxt)+this.e2.eval(cxt);
};

//mejorar cadena+entero será cadena pero actualmente no
astclasses.AddExp.prototype.getType = function(cxt){
    if(astclasses.doCastDataType(this.e1.getType(cxt)) 
        == astclasses.doCastDataType(this.e2.getType(cxt)))
        return astclasses.getDomainType(this.e1.getType(cxt), this.e2.getType(cxt));
    return '';
}

astclasses.SubtractExp.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    return this.e1.eval(cxt)-this.e2.eval(cxt);
};

astclasses.SubtractExp.prototype.getType = function(cxt){
    if(astclasses.doCastDataType(this.e1.getType(cxt)) == 'ENTERO' 
        && astclasses.doCastDataType(this.e2.getType(cxt)) == 'ENTERO')
        return astclasses.getDomainType(this.e1.getType(cxt), this.e2.getType(cxt));
    return '';
}

astclasses.MultiplyExp.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    return this.e1.eval(cxt)*this.e2.eval(cxt);
}

astclasses.MultiplyExp.prototype.getType = function(cxt){
    if(astclasses.doCastDataType(this.e1.getType(cxt)) == 'ENTERO' 
        && astclasses.doCastDataType(this.e2.getType(cxt)) == 'ENTERO')
        return astclasses.getDomainType(this.e1.getType(cxt), this.e2.getType(cxt));
    return '';
}

astclasses.DivideExp.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    var e1 = this.e1.eval(cxt);
    var e2 = this.e2.eval(cxt);
    if(e2 == 0)
        astclasses.endProgram('DIVISIÓN ENTRE CERO NO ESTÁ PERMITIDA');
    return e1/e2;
};

astclasses.DivideExp.prototype.getType = function(cxt){
    if(astclasses.doCastDataType(this.e1.getType(cxt)) == 'ENTERO' 
        && astclasses.doCastDataType(this.e2.getType(cxt)) == 'ENTERO')
        return astclasses.getDomainType(this.e1.getType(cxt), this.e2.getType(cxt));
    return '';
}

astclasses.PowExp.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    return Math.pow(this.e1.eval(cxt), this.e2.eval(cxt));
};

astclasses.PowExp.prototype.getType = function(cxt){
    if(astclasses.doCastDataType(this.e1.getType(cxt)) == 'ENTERO' 
        && astclasses.doCastDataType(this.e2.getType(cxt)) == 'ENTERO')
        return astclasses.getDomainType(this.e1.getType(cxt), this.e2.getType(cxt));
    return '';
}

astclasses.ModExp.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    return this.e1.eval(cxt) % this.e2.eval(cxt);
};

astclasses.ModExp.prototype.getType = function(cxt){
    if(astclasses.doCastDataType(this.e1.getType(cxt)) == 'ENTERO' 
        && astclasses.doCastDataType(this.e2.getType(cxt)) == 'ENTERO')
        return astclasses.getDomainType(this.e1.getType(cxt), this.e2.getType(cxt));
    return '';
}

astclasses.OperatorR.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    switch(this.type){
        case '<':
                if(this.e1.eval(cxt) < this.e2.eval(cxt))
                    return "VERDADERO";
                else return "FALSO";
                break;
        case '>':
                if(this.e1.eval(cxt) > this.e2.eval(cxt))
                    return "VERDADERO";
                else return "FALSO";
                break;
        case '=':
                if(this.e1.eval(cxt) == this.e2.eval(cxt))
                    return "VERDADERO";
                else return "FALSO";
                break;
        case '<=':
                if(this.e1.eval(cxt) <= this.e2.eval(cxt))
                    return "VERDADERO";
                else return "FALSO";
                break;
        case '>=':
                if(this.e1.eval(cxt) >= this.e2.eval(cxt))
                    return "VERDADERO";
                else return "FALSO";
                break;
        case '<>':
                if(this.e1.eval(cxt) != this.e2.eval(cxt))
                    return "VERDADERO";
                else return "FALSO";
                break;
    }
};


astclasses.OperatorR.prototype.getType = function(cxt){
    if(astclasses.doCastDataType(this.e1.getType(cxt)) 
        == astclasses.doCastDataType(this.e2.getType(cxt)))
        return 'LOGICO';
    return '';
}

//Lógico
astclasses.OperatorL.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    switch(this.type){
        case 'Y':
                if(this.e1.eval(cxt) == "VERDADERO" && this.e2.eval(cxt) == "VERDADERO")
                    return "VERDADERO";
                else return "FALSO";
                break;
        case 'O':
                if(this.e1.eval(cxt) == "VERDADERO" || this.e2.eval(cxt) == "VERDADERO")
                    return "VERDADERO";
                else return "FALSO";
                break;
    }
};

astclasses.OperatorL.prototype.getType = function(cxt){
    if(this.e1.getType(cxt) == 'LOGICO' && this.e2.getType(cxt) == 'LOGICO')
        return 'LOGICO';
    return '';
}

//Negación
astclasses.OperatorN.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    if(this.e1.eval(cxt) == "FALSO")
        return "VERDADERO";
    else return "FALSO";
};

astclasses.OperatorN.prototype.getType = function(cxt){
    if(this.e1.getType(cxt) == 'LOGICO')
        return 'LOGICO';
    return '';
}

astclasses.Integer.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    return this.value;
};

astclasses.Integer.prototype.getType = function(cxt){
    return this.type;
}

astclasses.String.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    return this.value;
};

astclasses.String.prototype.getType = function(cxt){
    return this.type;
}

astclasses.Boolean.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    return this.value;
};

astclasses.Boolean.prototype.getType = function(cxt){
    return 'LOGICO';
}

astclasses.Identifier.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    var value;
    if(!astclasses.variableExist(this.name, cxt, this.isArray))
        return;
    if(this.isArray){
        value = cxt[astclasses.getIdentifierName(this.name,cxt)].valor[astclasses.getDimension(cxt, this.name, this.vectorDimensiones)];
        if(value == null){
            astclasses.endProgram('No ha sido inicializada la variable: '+this.name);
            return '';
        }
    }
    else
        value = cxt[astclasses.getIdentifierName(this.name,cxt)].valor;
    return astclasses.doCast(value, cxt[astclasses.getIdentifierName(this.name,cxt)].tipo);
};

astclasses.Identifier.prototype.getType = function(cxt){
    if(!astclasses.variableExist(this.name, cxt, this.isArray))
        return;
    var tipo = cxt[astclasses.getIdentifierName(this.name,cxt)].tipo;
    return tipo;
}

/*
funcionCeroParametros    = limpiar
*/
astclasses.Function0.prototype.eval = function(cxt){
    if(!(astclasses.actualInstruction >= astclasses.lastInstruction) || astclasses.stop)
        return;
    switch(this.name){
        case 'limpiar':
            ejecucion.setValue('');
            break;
    }
};

astclasses.Function0.prototype.getType = function(cxt){
    return '';
}

/*
funcionUnParametro    = aleatorio|raiz|log|ln|exp|abs|sen|asen|cos|acos|tan|atan|truncar
*/
astclasses.Function1.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    switch(this.name){
        case 'aleatorio':
            return Math.round(Math.random()*this.exp1.eval(cxt));
            break;
        case 'raiz':
            return Math.sqrt(this.exp1.eval(cxt));
            break;
        case 'log':
            return Math.log10(this.exp1.eval(cxt));
            break;
        case 'ln':
            return Math.log(this.exp1.eval(cxt));
            break;
        case 'abs':
            return Math.abs(this.exp1.eval(cxt));
            break;
        case 'sen':
            return Math.sin(this.exp1.eval(cxt));
            break;
        case 'asen':
            return Math.asin(this.exp1.eval(cxt));
            break;
        case 'cos':
            return Math.cos(this.exp1.eval(cxt));
            break;
        case 'acos':
            return Math.acos(this.exp1.eval(cxt));
            break;
        case 'tan':
            return Math.tan(this.exp1.eval(cxt));
            break;
        case 'atan':
            return Math.atan(this.exp1.eval(cxt));
            break;
        case 'truncar':
            return Math.trunc(this.exp1.eval(cxt));
            break;
        case 'entero':
            return Math.round(this.exp1.eval(cxt));
            break;
        case 'real':
            return parseFloat(this.exp1.eval(cxt));
            break;
    }
};

astclasses.Function1.prototype.getType = function(cxt){
    switch(this.name){
        case 'aleatorio':
        case 'entero':
            return 'ENTERO';
        case 'truncar':
            return 'ENTERO';
    }
    return 'REAL';
}

/*
funcionDosParametros = redondear|cadconcatenar
*/
astclasses.Function2.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    switch(this.name){
        case 'redondear':
            var decimals = Math.pow(10, this.exp2.eval(cxt));
            return Math.round(this.exp1.eval(cxt)*decimals)/(decimals);
            break;
        case 'cadconcatenar':
            return this.exp1.eval(cxt)+''+this.exp2.eval(cxt);
            break;
    }
};

astclasses.Function2.prototype.getType = function(cxt){
    switch(this.name){
        case 'redondear':
            return 'ENTERO';
        case 'cadconcatenar':
            return 'CADENA';
    }
    
}

/*
funcionDosParametros = cadinsertar|cadremover|cadreemplazar|cadextraer|cadbuscar
exp1: cadena a la que se realizará el proceso
exp2: cadena a insertar, remover o reemplazar
exp3: posición de inicio
return: retorna el resultado de la operación (exp1 nunca se altera)
*/
astclasses.Function3.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    switch(this.name){
        case 'cadinsertar'://inserta una cadena en otra
            var result = this.exp1.eval(cxt).slice(0, this.exp3.eval(cxt))
             + this.exp2.eval(cxt) + this.exp1.eval(cxt).slice(this.exp3.eval(cxt));
            return result;
            break;
        case 'cadremover'://remueve una cadena de otra cadena
            var result = this.exp1.eval(cxt).slice(0, this.exp3.eval(cxt))
             + this.exp1.eval(cxt).slice(this.exp3.eval(cxt)).replace(this.exp2.eval(cxt), '');
            return result;
            break;
        case 'cadreemplazar':
            var result = this.exp1.eval(cxt).slice(0, this.exp3.eval(cxt))
             + this.exp2.eval(cxt);
            return result;
            break;
        case 'cadextraer'://igual a remover
            var result = this.exp1.eval(cxt).slice(0, this.exp3.eval(cxt))
             + this.exp1.eval(cxt).slice(this.exp3.eval(cxt)).replace(this.exp2.eval(cxt), '');
            return result;
            break;
        case 'cadbuscar'://busca una cadena y devuelve si fue encontrado o no
            var result = this.exp1.eval(cxt).slice(this.exp3.eval(cxt)).search(this.exp2.eval(cxt));
            return (result<0?'FALSO':'VERDADERO');
            break;
    }
};

astclasses.Function3.prototype.getType = function(cxt){
    switch(this.name){
        case 'cadbuscar':
            return 'LOGICO';
            break;
    }
    return 'CADENA'
    
}

astclasses.Exit.prototype.eval = function(cxt){
    if(!(astclasses.actualInstruction >= astclasses.lastInstruction) || astclasses.stop)
        return;
    if(astclasses.cicleDeep > 0)
        astclasses.exit = true;
};

astclasses.Function.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;

    astclasses.updateStringp('-f');

    //alert();
    for (var i = astclasses.getPosition(); i < this.stmts.length; i++) {
        astclasses.isPos(this.stmts[i]);
        this.stmts[i].eval(cxt);
        astclasses.updatePosition();
        if(astclasses.stop)
            return;

        if(astclasses.debugMode(this.stmts[i])){
            return;
        }
    }
    astclasses.resetPosition();
    var retorno = null;
    if(this.retorno){
        if(this.retorno.getType(cxt) != this.dataType){
            astclasses.endProgram('Condición debe ser '+this.dataType
                +', pero se obtuvo '+this.retorno.getType(cxt));
            return;
        }
        astclasses.isFunctionPermited = false;
        retorno = this.retorno.eval(cxt);
        astclasses.isFunctionPermited = true;
    }
    return retorno;
};

astclasses.FunctionCall.prototype.eval = function(cxt){
    if(astclasses.stop)
        return;
    var existe = false;
    for (var i = 0; i < astclasses.functions.length; i++) {
        if(astclasses.functions[i].name == this.name){
            if(!astclasses.isFunctionPermited){
                astclasses.endProgram('No se permite la llamada de '+
                        this.name
                            +', como parámetro de algún ciclo, estructura '+
                            'condicional\n\t, retorno de función o posición de un arreglo');
            }
            existe = true;

            //verifico cantidad de parámetros
            if(astclasses.functions[i].parameters && this.parameters){
                if(astclasses.functions[i].parameters.length > this.parameters.length)
                    astclasses.endProgram('Faltan parámetros en llamada a la función '+this.name);
                else if(astclasses.functions[i].parameters.length < this.parameters.length)
                    astclasses.endProgram('Sobran parámetros en llamada a la función '+this.name);

            }
            else if(!astclasses.functions[i].parameters && this.parameters){
                astclasses.endProgram('La función '+this.name+' no debe recibir parámetros');

            }
            else if(!this.parameters && astclasses.functions[i].parameters){
                astclasses.endProgram('Faltan parámetros en llamada a la función '+this.name);
                return;

            }



            /*declaración de parámetros de la función tomando functionDeep para
                saber que invocación es esta
            */
            astclasses.functionDeep++;
            if(astclasses.functionDeep > 100){
                astclasses.endProgram('¡Pila de funciones excedida!');
            }
            astclasses.updateStringp('-f');
            /*if(!astclasses.isStringParam('exist')){
                astclasses.setStringParam('exist', true);

            }
            else{
                condition = astclasses.getStringParam('condition');
            }*/
            if(astclasses.functions[i].parameters && !astclasses.isStringParam('exist')){
                for (var j = 0; j < astclasses.functions[i].parameters.length; j++) {
                    var parameter = astclasses.functions[i].parameters[j];
                    cxt[astclasses.functions[i].name+''+astclasses.functionDeep+'-'+parameter.id] = {
                        //valor : (parameter.eval(cxt)),
                        tipo : parameter.type,
                        vectorDimensiones: parameter.vectorDimensiones,
                        isArray: parameter.isArray,
                        var:true,
                    };
                }
            }


            var lastFunction = astclasses.actualFunction;
            astclasses.actualFunction = this.name+''+astclasses.functionDeep;
            
            //asigno valor a las variables de la función
            //izquierda de asignación con nuevo ambito, derecha con viejo ambito
            astclasses.isFunctionPermited = false;
            for (var j = 0; j < astclasses.functions[i].parameters.length 
                && !astclasses.isStringParam('exist'); j++) {
                
                astclasses.actualFunction = lastFunction;

                //falta código de asignación válida. En retorno también
                if(astclasses.functions[i].parameters[j].type != this.parameters[j].getType(cxt)){
                    astclasses.endProgram('Se esperaba '+
                        astclasses.functions[i].parameters[j].type
                            +', pero se obtuvo '+this.parameters[j].getType(cxt));

                }

                var rightExp = this.parameters[j].getType(cxt);
                var leftExp = astclasses.functions[i].parameters[j].type;
                if(leftExp != rightExp){
                    if(astclasses.doCastDataType(rightExp) == 'ENTERO' && leftExp == 'CADENA'){
                    }
                    else if(rightExp == 'CARACTER' && leftExp == 'CADENA'){
                    }
                    else if(astclasses.doCastDataType(rightExp) == 'ENTERO' 
                        && astclasses.doCastDataType(leftExp) == 'ENTERO'){
                    }
                    else{
                        astclasses.endProgram('Se esperaba '+leftExp+' pero se obtuvo '+rightExp);
                        return;
                    }
                }


                var value = astclasses.doCast(this.parameters[j].eval(cxt), leftExp);

                //no usé Assignment por cuestión de recursividad
                lastFunction = astclasses.actualFunction;
                astclasses.actualFunction = this.name+''+astclasses.functionDeep;

            
                cxt[astclasses.
                    getIdentifierName(astclasses.functions[i].parameters[j].id, cxt)].valor = value
                         
            }
            astclasses.isFunctionPermited = true;
            if(!astclasses.isStringParam('exist')){
                astclasses.setStringParam('exist', true);
            }


            var retorno = astclasses.functions[i].eval(cxt);

            astclasses.functionDeep--;

            astclasses.actualFunction = lastFunction;
            if(astclasses.stop)
                return;
            astclasses.setStringParam('exist', false);
            astclasses.resetPosition();

            if(retorno)
                return retorno;
        }
    }
    if(!existe)
        astclasses.endProgram('No existe la función '+this.name);
};



astclasses.FunctionCall.prototype.getType = function(cxt){
    for (var i = 0; i < astclasses.functions.length; i++) {
        if(astclasses.functions[i].name == this.name){
            var tipo = astclasses.functions[i].dataType;

            return tipo;

        }
    }
    return '';    
}

/**
    Internamente vectores y matrices siempre serán vectores.
    Por tanto, la función trae la posición del vector que se necesita
*/
astclasses.getDimension = function(cxt, id, vectorDimensiones){
    var dimension = 0;
    if(vectorDimensiones.length != cxt[astclasses.getIdentifierName(id,cxt)].vectorDimensiones.length){
            astclasses.endProgram('ERROR EN DIMENSIONES, SE ESPERABAN '
                +cxt[astclasses.getIdentifierName(id,cxt)].vectorDimensiones.length);
    }
    if(vectorDimensiones.length < 2){
        astclasses.isFunctionPermited = false;
        var dimension1R = cxt[astclasses.getIdentifierName(id,cxt)].vectorDimensiones[0].eval(cxt);
        var dimension1U = vectorDimensiones[0].eval(cxt);
        astclasses.isFunctionPermited = true;
        if(dimension1R <= dimension1U)
            astclasses.endProgram('SE EXCEDIÓ EL TAMAÑO MÁXIMO DEL ARREGLO ');
        dimension += vectorDimensiones[0].eval(cxt);
    }
    else{
        astclasses.isFunctionPermited = false;
        var dimension1R = cxt[astclasses.getIdentifierName(id,cxt)].vectorDimensiones[0].eval(cxt);
        var dimension2R = cxt[astclasses.getIdentifierName(id,cxt)].vectorDimensiones[1].eval(cxt);
        var dimension1U = vectorDimensiones[0].eval(cxt);
        var dimension2U = vectorDimensiones[1].eval(cxt);
        astclasses.isFunctionPermited = true;
        if(dimension1R <= dimension1U || dimension2R <= dimension2U)
            astclasses.endProgram('SE EXCEDIÓ EL TAMAÑO MÁXIMO DEL ARREGLO ');
        dimension += cxt[astclasses.getIdentifierName(id,cxt)].vectorDimensiones[1].eval(cxt)*vectorDimensiones[0].eval(cxt);
        dimension += vectorDimensiones[1].eval(cxt);

    }
    
    return dimension;

}

/**
    Trae el nombre del indentificador buscado
    Toma en cuenta la función donde nos encontramos
*/
astclasses.getIdentifierName = function(name, cxt){
    var finalName = name;
        //alert();
    if(astclasses.actualFunction != ''){
        if(cxt[astclasses.actualFunction+'-'+name])
            finalName = astclasses.actualFunction+'-'+name;
    }
    
    return finalName;

}

/*
    Según el tipo de dato hace el cast.
    Para poder asignar reales a enteros (ejemplo de uso)
*/
astclasses.doCast = function(value, type){
    //verificar que sea número o retornar 0
        switch (type){
            case "ENTERO":
                        var r = (Math.round(value));
                        if(isNaN(r))
                            return 0;
                        return r;
                        break;
            case "REAL":
                        var r = Number(value);
                        if(isNaN(r))
                            return 0;
                        return r;
                        break;
            case "CADENA":
                        return String(value);
                        break;
            case "CARACTER":
                        var r = String(value);
                        if(r.length > 1)
                            return '';
                        return r;
                        break;
            case "LOGICO":
                        if(value === 'VERDADERO' || value === 'FALSO')
                            return value;
                        var r = (Math.round(value));
                        if(isNaN(r))
                            return 'FALSO';
                        return (r<=0?'FALSO':'VERDADERO');
                        break;
        }
}

/**
    Casa tipos de datos parecidos entre si. Entero y Real sirven
    para operaciones aritméticas
**/
astclasses.doCastDataType = function(type){
    var tipo = type;
    if(tipo == 'CARACTER')
        tipo = 'CADENA';
    if(tipo == 'REAL')
        tipo = 'ENTERO';
    return tipo;
}

/**
    entre Real y entero será resultante un Real
    entre caracter y cadena el resultado será una cadena
**/
astclasses.getDomainType = function(type1, type2){
    if(type1 == 'REAL' || type2 == 'REAL')
        return 'REAL';
    if(type1 == 'CADENA' || type2 == 'CADENA')
        return 'CADENA';
    return type1;
}


/**
    Función de error personalizado
**/
astclasses.showError = function(message){
    ejecucion.setValue(ejecucion.getValue()+
        '\nERROR '+message+'\n');
}

/**
    Verifica que una variable exista
**/
astclasses.variableExist = function(name, cxt, isArray = false){
    existe = false;
        //alert();
    if(astclasses.actualFunction != ''){
        if(cxt[astclasses.actualFunction+'-'+name]){
            if(cxt[astclasses.actualFunction+'-'+name].isArray == isArray)
                existe = true;
        }
    }
    if(cxt[name])
        if(cxt[name].isArray == isArray)
            existe = true;

    /*var nombre = name;
    if(name.split('-')[1].length > 1)
        nombre = name.split('-')[1];*/
    if(!existe){
        astclasses.endProgram('No existe la variable '+name+' o error en dimensiones');
        return false;
    }
    return true;

}

/**
    Finaliza el programa e informa del proceso
**/
astclasses.endProgram = function(message){
    if(astclasses.stop)
        return;
    var linea = '';
    if(astclasses.actualLine > 0)
        linea = '\nLínea: '+(astclasses.actualLine+1);
    astclasses.showError(message+linea);
    astclasses.showError('Programa terminado...');
    astclasses.stop = true;
    astclasses.lastInstruction = 0;
    astclasses.isRunning = false;
    habilitarBotonesEjecucion();

}


/**
    Actualiza sentencia donde se encuentra la ejecución actualmente
**/
astclasses.updatePosition = function(){
    if(astclasses.stop)
        return;
    astclasses.position[astclasses.stringp.toString()]++;
    //alert(astclasses.position[astclasses.stringp.toString()]);
}

/**
    Obtiene posición donde nos encontramos actualmente
**/
astclasses.getPosition = function(){
    //alert(astclasses.position[astclasses.stringp.toString()]);

    //alert(astclasses.stringp.toString()+' '+astclasses.position[astclasses.stringp.toString()]);
    return astclasses.position[astclasses.stringp.toString()];
}

astclasses.resetPosition = function(deleteI = true){
    if(astclasses.stop)
        return;
    astclasses.position[astclasses.stringp.toString()] = 0;
    //delete astclasses.stringp[astclasses.stringp.length-1];
    if(deleteI)
        astclasses.stringp.splice(astclasses.stringp.length-1, 1);
    //alert('resert '+astclasses.stringp.toString()+' '+astclasses.position[astclasses.stringp.toString()]);
    
    //astclasses.stringp.splice(astclasses.stringp.length, 1);
}

astclasses.updateStringp = function(string){
    astclasses.stringp.push(string);
    if(!astclasses.position[astclasses.stringp.toString()])
        astclasses.position[astclasses.stringp.toString()] = 0;
    if(string == "-read")
    if(!astclasses.position[astclasses.stringp.toString()+'read'])
        astclasses.position[astclasses.stringp.toString()+'read'] = 0;
}

astclasses.isStringParam = function(param){
    if(astclasses.position[astclasses.stringp.toString()+param] == null)
        return false;
    if(astclasses.position[astclasses.stringp.toString()+param] == '')
        return false;
    return true;
}

astclasses.setStringParam = function(param, value){
    if(astclasses.stop)
        return;
    astclasses.position[astclasses.stringp.toString()+param] = value;
}

astclasses.getStringParam = function(param){
    return astclasses.position[astclasses.stringp.toString()+param];
}



astclasses.updatePositionR = function(){
    if(astclasses.stop)
        return;
    astclasses.position[astclasses.stringp.toString()+'read']++;
}

astclasses.getPositionR = function(){
    return astclasses.position[astclasses.stringp.toString()+'read'];
}

astclasses.resetPositionR = function(){
    if(astclasses.stop)
        return;
    astclasses.position[astclasses.stringp.toString()+'read'] = 0;
}

astclasses.debugMode = function(exp = {pos:false}){
    if(astclasses.debug && !entradaDatosActivo){
        if(exp.pos){
            astclasses.actualLine = exp.pos-1;
             breakPoints = editores.getBreakPoints();
             for (var i = 0; i < breakPoints.length; i++) {
                 if(breakPoints[i].row == astclasses.actualLine){
                    astclasses.stop = true;
                    astclasses.breakPointsStop = true;
                    return true;
                 }
                    
             }
         }
         if(astclasses.cantInstruction < 250)
            astclasses.cantInstruction++;
        else{
            astclasses.stop = true;
            return true;
        }
             return false;
             

    }
            
 

    

    //Todo esto era lo original
    /*if(astclasses.finishRead){
        if(astclasses.debug && !entradaDatosActivo)
            astclasses.stop = true;
    }
    else{
        astclasses.finishRead = true;
        return false;
    }*/
    if(astclasses.debugAutomatic && astclasses.functionDeep < 101 && !entradaDatosActivo){
        if(astclasses.cantInstruction < 500)
            astclasses.cantInstruction++;
        else{
            astclasses.stop = true;
            return true;
        }
    }
    return astclasses.debug;
}

astclasses.isPos = function(exp){
    if(exp.pos){
        astclasses.actualLine = exp.pos-1;
        if(astclasses.debug && !astclasses.debugAutomatic)
            editores.marcarLinea(exp.pos-1);
    }
}

astclasses.validateFunctions = function(){
    for (var i = 0; i < astclasses.functions.length; i++) {
        for (var j = i+1; j < astclasses.functions.length; j++) {
            if(astclasses.functions[i].name == astclasses.functions[j].name){
                astclasses.endProgram('La función '+astclasses.functions[j].name
                    +" no puede declararse más de una vez");
                break;
            }
        }

    }

}




if (typeof(module) !== 'undefined') { module.exports = astclasses; }