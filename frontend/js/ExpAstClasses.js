
var ast = {

    actualInstruction: 0,//instrucción actual (inservible casi)
    isRunning: false,//para saber si hay un programa ejecutándose
    totalVariables: 0,//cantidad total de variables (para tabla de paso a paso)
    lastInstruction: 0,//última insturcción de lectura (casi inservible)
    stop: false,//para frenar ejecución
    cicleDeep: 0,//cantidad de ciclos
    functionDeep: 0,//cantidad de funciones llamadas
    exit: false,//para el comando 'abandona'
    functions: null,//funciones del programa
    actualFunction: '',//función actual en ejecución
    position:[],//posición en valores
    stringp: [],//posición en string donde nos encontramos
    isFunctionPermited: true,//para evitar disparar error si se usa alguna función
    debug: false,//para modo paso a paso
    debugAutomatic: true,//para ejecución normal y poder eliminar limitación de un hilo
    cantInstruction: 0, //cantidad de instrucciones ejecutadas sin parar
    finishRead: true, //profundidad de lectura
    actualLine:0,//línea actual de ejecución
    declaratedVariables:false,//para saber si ya la declaración de variables pasó (para paso a paso)
    breakPointsStop: false,


    Program: function Program(stmts, functions) {
        this.statements = stmts;
        this.functions = functions;
    },

    VariablesCreation: function VariablesCreation(stmts) {
        this.statements = stmts;
    },

    VariableCreation: function VariableCreation(type, id, defaultValue = null, isArray = false, vectorDimensiones = []) {
        this.id = id;
        this.type = type;
        this.vectorDimensiones = vectorDimensiones;
        this.isArray = isArray;
        this.defaultValue = defaultValue;
    },

    ConstantsCreation: function ConstantsCreation(stmts) {
        this.statements = stmts;
    },

    IfStruct: function IfStruct(condition, ifstmts = [], elsestmts = []) {
        this.ifstmts = ifstmts;
        this.elsestmts = elsestmts;
        this.condition = condition;
    },

    SwitchStruct: function SwitchStruct(id, cases = [], elseCase = []) {
        this.id = id;
        this.cases = cases;
        this.elseCase = elseCase;
    },

    SwitchCaseStruct: function SwitchCaseStruct(exp, stmts) {
        this.exp = exp;
        this.stmts = stmts;
    },

    ForStruct: function ForStruct(id, initial, final, stmts, step = new ast.Integer(1)) {
        this.id = id;
        this.initial = initial;
        this.final = final;
        this.stmts = stmts;
        this.step = step;
    },

    WhileStruct: function WhileStruct(condition, stmts) {
        this.condition = condition;
        this.stmts = stmts;
    },

    RepeatUntilStruct: function RepeatUntilStruct(condition, stmts) {
        this.condition = condition;
        this.stmts = stmts;
    },

    Assignment: function Assignment(id, exp, isArray = false, vectorDimensiones = []) {
        this.id = id;
        this.exp = exp;
        this.isArray = isArray;
        this.vectorDimensiones = vectorDimensiones;
    },

    Print: function Print(exps, nl = false) {
        this.exps = exps;
        this.nl = nl;
    },

    Read: function Read(exps) {
        this.exps = exps;
    },

    AddExp: function AddExp(e1, e2) {
        this.e1 = e1;
        this.e2 = e2;
    },
    SubtractExp: function SubtractExp(e1, e2) {
        this.e1 = e1;
        this.e2 = e2;
    },
    MultiplyExp: function MultiplyExp(e1, e2) {
        this.e1 = e1;
        this.e2 = e2;
    },
    DivideExp: function DivideExp(e1, e2) {
        this.e1 = e1;
        this.e2 = e2;
    },
    PowExp: function PowExp(e1, e2) {
        this.e1 = e1;
        this.e2 = e2;
    },
    ModExp: function ModExp(e1, e2) {
        this.e1 = e1;
        this.e2 = e2;
    },
    OperatorR: function OperatorR(e1, e2, type) {
        this.e1 = e1;
        this.e2 = e2;
        this.type = type;
    },
    OperatorL: function OperatorL(e1, e2, type) {
        this.e1 = e1;
        this.e2 = e2;
        this.type = type;
    },
    OperatorN: function OperatorN(e1) {
        this.e1 = e1;
    },
    Integer: function Integer(i, type = 'ENTERO') {
        this.value = i;
        this.type = type;
    },
    String: function String(i, type) {
        this.value = i;
        this.type = type;
    },
    Boolean: function Boolean(i) {
        this.value = i;
    },
    /*
    Vector de longitud de la cantidad de dimensiones.
    Cada valor del vector dimensiones corresponde al tamaño de la dimensión.
    Ejemplo: mat[1][2] vectorDimensiones = [1, 2]
    */
    Identifier: function Identifier(id, isArray = false, vectorDimensiones = []) {
        this.name = id;
        this.value = null;
        this.isArray = isArray;
        this.vectorDimensiones = vectorDimensiones;
    },
    //Funciones del sistema
    Function0: function Function0(name) {
        this.name = name;
    },
    Function1: function Function1(name, exp1) {
        this.name = name;
        this.exp1 = exp1;
    },
    Function2: function Function2(name, exp1, exp2) {
        this.name = name;
        this.exp1 = exp1;
        this.exp2 = exp2;
    },
    Function3: function Function3(name, exp1, exp2, exp3) {
        this.name = name;
        this.exp1 = exp1;
        this.exp2 = exp2;
        this.exp3 = exp3;
    },
    Exit: function Exit() {
    },
    //Declaración y llamada de funciones
    Function: function Function(name, stmts, parameters = [], dataType = null, retorno = null) {
        this.name = name;
        this.stmts = stmts;
        this.parameters = parameters;
        this.retorno = retorno;
        this.dataType = dataType;
    },
    FunctionCall: function FunctionCall(name, parameters = []) {
        this.name = name;
        this.parameters = parameters;
    },

};
if (typeof(module) !== 'undefined') {
    module.exports = ast;
}
