grammar pseudo;

options 
{ 
language = CSharp2; 
output=AST;
}


//Uso esto solo para los nodos imaginarios usados en el AST
tokens 
{
PROGRAMA;	//Nodo programa
LISTA_VARS;	//Nodo para la lista de variables declaradas en un programa
LISTA_CONSTS;	//Nodo para la lista de constantes declaradas en un programa
SENTENCIAS;	//Nodo para las sentencias que componen el programa
DEFVAR;		//Nodo para la definicion de una lista de variables (ej. ENTERO a,b,c[4],d[2][3])
DEFCONST;	//Nodo para la definicion de una lista de constantes (ej. ENTERO PI=3, F=4)
VARSIMPLE;	//Nodo de una variable simple del ej. serian a o b
CONSTSIMPLE;	//Nodo de una constante del ej. serian PI o F
ONE_DIM_ARRAY;	//Nodo de una variable con una dimension del ej. seria c
TWO_DIM_ARRAY;	//Nodo de una variable con dos dimensiones del ej. seria d
NEGATIVO;	//Nodo para la operacion unaria de los numeros negativos (ej -1 > 2 alli el negativo es el operador de -1 )
}

@lexer::namespace {
    �_Numeral
}

@parser::namespace {
    �_Numeral
}


OLOGICO : 'O';
YLOGICO : 'Y';
INICIO_PROG : 'INICIO'; 
ALGORITMO : 'ALGORITMO' ;
INICIO_DECL_VAR : 'VAR' ;
INICIO_DECL_CONST : 'CONST';
TIPO_ENTERO : 'ENTERO' ;
TIPO_REAL : 'REAL' ;
TIPO_CADENA : 'CADENA' ;
TIPO_CARACTER : 'CARACTER' ;
TIPO_LOGICO : 'LOGICO' ;
ASIGNACION : ':=' ;
SEPARADOR_LISTA : ',' ;
LEER : 'leer' ;
ESCRIBIR : 'escribir' ;
ESCRIBIRNL : 'escribirnl' ;
SI : 'si' ;
ENTONCES : 'entonces' ;
SINO : 'sino' ;
FINSI : 'fin si' ;
SELECCIONAR : 'seleccionar' ;
CASO : 'caso' ;
FINSELECCIONAR : 'fin seleccionar' ;
PARA : 'para' ;
DESDE : 'desde' ;
HASTA : 'hasta' ;
PASO : 'paso' ;
HACER : 'hacer' ;
FINPARA : 'fin para' ;
MIENTRAS : 'mientras' ; 
FINMIENTRAS : 'fin mientras' ;
REPITA : 'repita' ;
FUN_LIMPIAR : 'limpiar' ;
FUN_RAIZ : 'raiz' ;
FUN_LOG : 'log' ;
FUN_LN : 'ln' ;
FUN_ABS : 'abs' ;
FUN_TRUNCAR : 'truncar' ;
FUN_EXPONENCIAL : 'exp' ;
FUN_SENO : 'sen' ;
FUN_ASENO : 'asen' ;
FUN_COSENO : 'cos' ;
FUN_ACOSENO : 'acos' ;
FUN_TANGENTE : 'tan' ;
FUN_ATANGENTE : 'atan' ;
FUN_CONVENTERO : 'entero' ;
FUN_CONVREAL : 'real' ;
FUN_REDONDEAR : 'redondear' ;
FUN_POSICIONAREN : 'posicionaren' ;
FUN_CADCONCATENAR : 'cadconcatenar' ;
FUN_CADINSERTAR : 'cadinsertar' ;
FUN_CADREMOVER : 'cadremover' ;
FUN_CADEXTRAER : 'cadextraer' ;	
FUN_CADBUSCAR 	: 'cadbuscar';
FUN_CADREEMPLAZAR : 'cadreemplazar' ;
FIN_PROG : 'FIN' ;
SUMA  : '+'   ;
RESTA :  '-'   ;
DIVISION :  '/';
MULTIPLICACION :  '*' ;
IGUAL           : '='   ;
NO_IGUAL        : '<>' ;
MENOR           : '<'   ;
MENOR_IGUAL     : '<=' ;
MAYOR_IGUAL     : '>=' ;
MAYOR           : '>'   ;
PARENT_IZQ      : '('   ;
PARENT_DER      : ')'   ;
MODULO :  'mod'   ;
CORCHETE_IZQ :  '[';
CORCHETE_DER :  ']';
NEGACION :  '!';
ABANDONA :  'abandona';

LOGICO	:	'VERDADERO'
	|	'FALSO'
	;
 
IDENTIFICADOR  	:	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    		;

ENTERO :	'0'..'9'+
       ;

REAL
    :   ('0'..'9')+ '.' ('0'..'9')* EXPONENT?
    |   '.' ('0'..'9')+ EXPONENT?
    |   ('0'..'9')+ EXPONENT
    ;

COMENTARIO
    :   '//' ~('\n'|'\r')* {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

NUEVALINEA : '\r'?'\n'
	   ;

ESPACIO_BLANCO  :   ( ' '
        | '\t'
	| NUEVALINEA  //este deberia sera /r?/n pero al incio de la linea TODO:arreglar
        ) {$channel=HIDDEN;}
        ;

CADENA
    :  '"' ( ESC_SEQ | ~('\\'|'"'|'\n'|'\n\r')  )* '"' {Text = Text.Substring(1, Text.Length - 2);}
    ;
    
CARACTER:  '\'' ( ESC_SEQ | ~('\''|'\\') ) '\'' {Text = Text.Substring(1, Text.Length - 2);}
    ;

fragment
EXPONENT : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;

fragment
HEX_DIGIT : ('0'..'9'|'a'..'f'|'A'..'F') ;

fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    |   UNICODE_ESC
    |   OCTAL_ESC
    ;

fragment
OCTAL_ESC
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UNICODE_ESC
    :   '\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
    ;



/*
 AST INFO:  
****Nodo Programa**** posee como su info de ubicacion, la del token INICIO_PROG y tiene como hijos:
	- Nombre del programa
	- Lista de variables declaradas
	- Lista de constantes declaradas
	- sentencias del programa
*/
programa 
	:	encabezado (declvariables)? (declconstantes)? INICIO_PROG terminador bloquesentencias FIN_PROG
		-> ^(PROGRAMA encabezado declvariables? declconstantes? bloquesentencias) //declvariables declconstantes)// bloquesentencias)
		  
		
	;
	
encabezado 
	:	(NUEVALINEA)* ALGORITMO IDENTIFICADOR terminador
		-> IDENTIFICADOR
	;
	
terminador 
	:	(NUEVALINEA)+	-> //No retorna AST
	;

declvariables
	:	INICIO_DECL_VAR terminador listadeclsvars
		-> listadeclsvars 
	;
	
listadeclsvars 
	:	declvars terminador (declvars terminador)*
		-> ^(DEFVAR declvars+)
	;

declvars:	tipodedato listavars
		-> ^(tipodedato listavars)
	;
	
listavars
	:	decl_var (SEPARADOR_LISTA decl_var )*
		-> decl_var+
	;	
	
decl_var:	IDENTIFICADOR(ASIGNACION expresion)?
		-> ^(VARSIMPLE IDENTIFICADOR expresion?)
		| IDENTIFICADOR CORCHETE_IZQ fila=ENTERO CORCHETE_DER 
		  ( CORCHETE_IZQ columna=ENTERO CORCHETE_DER -> ^(TWO_DIM_ARRAY IDENTIFICADOR $fila $columna)
		  |                                          -> ^(ONE_DIM_ARRAY IDENTIFICADOR $fila)
		  )
		;


declconstantes
	:	INICIO_DECL_CONST terminador listadeclsconsts
		-> listadeclsconsts
	;

listadeclsconsts 
	:	declconsts terminador (declconsts terminador)*
		-> ^(DEFCONST declconsts+)
	;
	
declconsts
	:	tipodedato listaconsts
		-> ^(tipodedato listaconsts)
	;
	
listaconsts
	:	decl_const (SEPARADOR_LISTA decl_const )*
		-> decl_const+
	;

//TODO:Es una expresion??? era una simple		
decl_const
	:	IDENTIFICADOR ASIGNACION expresion 
		-> ^(CONSTSIMPLE IDENTIFICADOR expresion)
	;

tipodedato
	:	TIPO_ENTERO | TIPO_REAL | TIPO_CADENA | TIPO_CARACTER | TIPO_LOGICO
	;
	
bloquesentencias
	:	sentencia+
		-> ^(SENTENCIAS sentencia+)
	;

sentencia 
	: 	instruccion terminador
		-> instruccion
	;
	
instruccion
	: 	sentexpresion
	|	sententrada
	|	sentsalida
	|	sentcondicional
	|	sentcondicionalmultiple
	|	sentciclopara
	| 	sentciclomientras
	| 	sentciclohasta
	|	sentabandona
	;

//sustituir backtrack por syntatic predicates??	
sentexpresion 
options {backtrack=true;
	 memoize = true;
	 }
	:	IDENTIFICADOR ASIGNACION  expresion
		-> ^(ASIGNACION ^(VARSIMPLE IDENTIFICADOR) expresion)
	| 	IDENTIFICADOR CORCHETE_IZQ fila=expresion CORCHETE_DER ASIGNACION  valor=expresion
		-> ^(ASIGNACION ^(ONE_DIM_ARRAY IDENTIFICADOR $fila) $valor)
	| 	IDENTIFICADOR CORCHETE_IZQ fila=expresion CORCHETE_DER CORCHETE_IZQ columna=expresion CORCHETE_DER ASIGNACION  valor=expresion
		-> ^(ASIGNACION ^(TWO_DIM_ARRAY IDENTIFICADOR $fila $columna) $valor)
	|	expresion
		-> expresion
	;
	
sententrada
	:	LEER variable (SEPARADOR_LISTA variable)*
		-> ^(LEER variable)+
	;
	
sentsalida
	:	ESCRIBIR (tiposalida) (SEPARADOR_LISTA tiposalida)*
		-> ^(ESCRIBIR tiposalida+)
	| 	ESCRIBIRNL ((tiposalida) (SEPARADOR_LISTA tiposalida)*)?
		-> ^(ESCRIBIRNL tiposalida*)
	;

tiposalida
	:	expresion
		-> expresion
	;
	
sentcondicional
	:	SI sentexpresion ENTONCES terminador verdadero=bloquesentencias (SINO terminador falso=bloquesentencias)? FINSI
		-> ^(SI sentexpresion $verdadero $falso?)
	;

sentcondicionalmultiple
	:	SELECCIONAR sentexpresion terminador casos (SINO terminador bloquesentencias)? FINSELECCIONAR
		-> ^(SELECCIONAR sentexpresion casos ^(SINO bloquesentencias)? )
	;

casos	:	caso+
	;

caso 	:	CASO ENTERO terminador bloquesentencias
		-> ^(CASO ENTERO bloquesentencias)
	;


//TODO muy importante: debido a la ineficiencia del antlr para generar automaticamente el walker manual, debo repetir
//el nodo de variable dos veces luego del bloque de sentencias ( y hacer el para en la generacion poco optimo igualmente,
// esto debe ser corregido con un walker manual en un futur, se deja asi para el primer release, en posteriores debe ser corregido.

sentciclopara
	:	PARA var=variable DESDE inicio=expresion HASTA condicion=expresion (PASO paso=expresion)? HACER terminador bloquesentencias FINPARA
		-> ^(PARA $var $inicio ($paso)? bloquesentencias $var $var $var $condicion)
	;

sentciclomientras
	:	MIENTRAS sentexpresion HACER terminador bloquesentencias FINMIENTRAS
		-> ^(MIENTRAS sentexpresion bloquesentencias)
	;
	
sentciclohasta
	:	REPITA terminador bloquesentencias HASTA sentexpresion
		-> ^(REPITA bloquesentencias sentexpresion)
	;

sentabandona
	:	ABANDONA
		-> ^(ABANDONA)
	;


expresion 
	: 	expr_o
	;

expr_o	:	expr_y ( OLOGICO^  expr_y)*
	;
	
expr_y 	:	expr_igualdad ( YLOGICO^  expr_igualdad)*
	;
	
expr_igualdad
	:	expr_relacional ( ( IGUAL^ | NO_IGUAL^ ) expr_relacional)*
	;
	
expr_relacional
	:	expr_suma (( MENOR^ | MENOR_IGUAL^ | MAYOR_IGUAL^ | MAYOR^) expr_suma)*
	;

expr_suma
	:	expr_multi (( SUMA^ | RESTA^ ) expr_multi)*
	;
	
expr_multi
	:	expr_unaria (( MULTIPLICACION^ | DIVISION^ | MODULO^) expr_unaria)*
	;
	

expr_unaria
	: 	  RESTA factor -> ^(NEGATIVO factor)
	  	| NEGACION factor -> ^(NEGACION factor)
	  	| factor -> factor
	; 

factor	:	
	  variable
    	| PARENT_IZQ! expresion PARENT_DER!  
    	| llamada_funcion_std
    	| constanteSinSigno
    	;

constanteSinSigno
    : numeroSinSigno
    | CADENA
    | LOGICO
    | CARACTER
    ;
    
numeroSinSigno
	:	ENTERO
	|	REAL
	;


variable:	IDENTIFICADOR
		-> ^(VARSIMPLE IDENTIFICADOR)
	|	IDENTIFICADOR CORCHETE_IZQ fila=expresion  CORCHETE_DER 
		  ( CORCHETE_IZQ columna=expresion CORCHETE_DER -> ^(TWO_DIM_ARRAY IDENTIFICADOR $fila $columna)
		     |                                          -> ^(ONE_DIM_ARRAY IDENTIFICADOR $fila)
		  ) 
	;

llamada_funcion_std
	:	
		funcion_std_cero  PARENT_IZQ PARENT_DER
		-> ^(funcion_std_cero)	
	|	funcion_std_uno  PARENT_IZQ expresion PARENT_DER
		-> ^(funcion_std_uno expresion)
	|	funcion_std_dos  PARENT_IZQ parametro1=expresion SEPARADOR_LISTA parametro2=expresion PARENT_DER
		-> ^(funcion_std_dos $parametro1 $parametro2)
	|	fcad=funcion_std_cadenas  PARENT_IZQ var1=variable SEPARADOR_LISTA parametro2=expresion SEPARADOR_LISTA parametro3=expresion  PARENT_DER
		-> {fcad.Tree.ToString().Equals("cadextraer") || fcad.Tree.ToString().Equals("cadbuscar")}?
								^(funcion_std_cadenas $var1 $parametro2 $parametro3)
		->  						^(funcion_std_cadenas $var1 $parametro2 $parametro3 $var1)
		
//el ultimo var1 solo debe colocarse si la funcion no es o extraer(cad,inicio,fin)
	;
	
funcion_std_cero
	:
		FUN_LIMPIAR
	;
		
funcion_std_uno
	:	FUN_RAIZ
	| 	FUN_LOG
	|	FUN_LN
	|	FUN_ABS
	|	FUN_TRUNCAR
	|	FUN_EXPONENCIAL
	|	FUN_SENO
	|	FUN_ASENO
	|	FUN_COSENO
	|	FUN_ACOSENO
	|	FUN_TANGENTE
	|	FUN_ATANGENTE
	|	FUN_CONVENTERO
	|	FUN_CONVREAL
	;

funcion_std_dos
	:	FUN_REDONDEAR
	|	FUN_POSICIONAREN
	|	FUN_CADCONCATENAR
	;
	
funcion_std_cadenas
	:	FUN_CADINSERTAR
	|	FUN_CADREMOVER
	|	FUN_CADREEMPLAZAR
	|	FUN_CADEXTRAER
	|	FUN_CADBUSCAR
	;	
