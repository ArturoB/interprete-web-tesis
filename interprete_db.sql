-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-06-2018 a las 01:32:16
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `interprete_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aula`
--

CREATE TABLE `aula` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `codigo` char(6) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `semestre_id` int(11) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  `fecha_borrado` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aula`
--

INSERT INTO `aula` (`id`, `nombre`, `codigo`, `usuario_id`, `semestre_id`, `borrado`, `fecha_borrado`) VALUES
(1, 'informática', '2c6494', 1, 1, 0, NULL),
(7, 'civil', '2c6497', 1, 1, 0, NULL),
(15, 'nuevo', NULL, 11, 1, 0, NULL),
(17, 'AulaProfesor', NULL, 3, 2, 0, NULL),
(20, 'prueba', NULL, 12, 6, 0, NULL),
(36, 'semestre', NULL, 12, 2, 0, NULL),
(41, 'prueba', '2c649c', 1, 2, 0, NULL),
(44, 'pr', '0bd181', 1, 2, 0, NULL),
(49, 'aula3', '1a2d1a', 1, 2, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

CREATE TABLE `carrera` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `carrera`
--

INSERT INTO `carrera` (`id`, `nombre`) VALUES
(3, 'Civil'),
(2, 'Electrónica'),
(4, 'Industrial'),
(1, 'Informática'),
(5, 'Mecánica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrega`
--

CREATE TABLE `entrega` (
  `id` int(11) NOT NULL,
  `proyecto_id` int(11) NOT NULL,
  `usuario_aula_id` int(11) NOT NULL,
  `codigo` mediumtext,
  `nota` float DEFAULT NULL,
  `comentarios` mediumtext,
  `ip` varchar(15) DEFAULT NULL,
  `crc` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `entrega`
--

INSERT INTO `entrega` (`id`, `proyecto_id`, `usuario_aula_id`, `codigo`, `nota`, `comentarios`, `ip`, `crc`) VALUES
(13, 16, 1, 'ALGORITMO conCrc\n    INICIO\n        escribirnl \"Hola mundo\"\n    FIN', 10, '', '', 1413672589),
(19, 30, 17, 'ALGORITMO miPrograma\n    INICIO\n        escribirnl \"Hola mundo\"\n    FIN', 15, 'Quedó bien', '::1', NULL),
(20, 31, 1, 'ALGORITMO miPrograma\n    VAR \n    ENTERO aux, vector[10], i, j\n    INICIO\n        para i desde 0 hasta 10 hacer\n            escribirnl \"posición \",(i+1),\": \"\n            vector[i] := aleatorio(5000)\n        fin para\n        escribirnl \"Vector\"\n        para i desde 0 hasta 9 hacer\n            para j desde (i+1) hasta 10 hacer\n                si (vector[j] > vector[i]) entonces\n                    aux := vector[i]\n                    vector[i] := vector[j]\n                    vector[j] := aux\n                fin si\n            fin para\n        fin para\n        para i desde 0 hasta 10 hacer\n            escribirnl \"posición \",(i+1),\": \",vector[i]\n        fin para\n        mientras (aleatorio(10000)<>0) hacer\n            1+1\n        fin mientras\n        escribirnl \"ahora si\"\n    FIN', 20, 'Muy bien!!!!', '::1', NULL),
(21, 25, 28, 'ALGORITMO miPrograma INICIO escribirnl \"Hola mundo\" FIN', 4, '', '::1', 4115062685),
(22, 31, 29, 'Búrbuja', NULL, NULL, '::1', NULL),
(23, 16, 29, 'dfsdfsdfsd', NULL, NULL, '::1', NULL),
(24, 25, 30, 'sdfdsfds', 234, 'regresaste ja\n', '::1', NULL),
(25, 26, 28, 'Prueba de ip en pc remota', NULL, NULL, '192.168.23.2', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametro`
--

CREATE TABLE `parametro` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `valor` varchar(45) DEFAULT NULL,
  `tipo_valor` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `parametro`
--

INSERT INTO `parametro` (`id`, `nombre`, `descripcion`, `valor`, `tipo_valor`) VALUES
(1, 'SEMESTRE_EN_CURSO', 'semestre en curso actualmente', '2', 'number'),
(7, 'CANTIDAD_MAXIMA_SCRIPTS', 'número máximo de scripts por estudiante', '20', 'number'),
(8, 'TAMAÑO_MAXIMO_SCRIPT_KB', 'tamaño máximo de cada script', '30', 'number'),
(9, 'PERMITIR_REGISTRO_EXTERNO', 'registro de usuarios no unet', '0', 'bool');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `id` int(11) NOT NULL,
  `aula_id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `valor` float NOT NULL,
  `enunciado` mediumtext,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `clave` char(40) DEFAULT NULL,
  `validacion_ip` tinyint(1) NOT NULL,
  `ip_rango_inicial` varchar(15) DEFAULT NULL,
  `ip_rango_final` varchar(15) DEFAULT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  `fecha_borrado` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proyecto`
--

INSERT INTO `proyecto` (`id`, `aula_id`, `nombre`, `valor`, `enunciado`, `fecha_inicio`, `fecha_fin`, `clave`, `validacion_ip`, `ip_rango_inicial`, `ip_rango_final`, `borrado`, `fecha_borrado`) VALUES
(16, 1, 'inicio1', 15, 'Parcial ', '2018-05-30 13:34:00', '2020-06-29 18:34:00', '8927bd748f26a7258a01e318a7e1e7585458a228', 0, '0.0.0.0', '255.255.255.255', 0, '2018-06-04 20:35:05'),
(25, 7, 'no', 0, '', '2018-05-30 21:39:00', '2018-06-30 22:39:00', '', 0, '0.0.0.0', '255.255.255.255', 0, NULL),
(26, 7, 'nuevo', 34, '', '2018-06-03 00:12:00', '2018-06-12 00:12:00', '', 0, '0.0.0.0', '255.255.255.255', 0, NULL),
(30, 17, 'Primer Proyecto', 15, 'Ordenar un vector con el método de la burbuja', '2018-06-04 19:56:00', '2018-10-04 19:56:00', NULL, 0, '0.0.0.0', '255.255.255.255', 0, NULL),
(31, 1, 'burbuja', 0, 'Método de la burbuja', '2018-06-06 01:02:00', '2018-06-28 01:02:00', '', 0, '0.0.0.0', '255.255.255.255', 0, NULL),
(41, 49, 'r', 0, '', '2018-06-13 18:46:00', '2018-06-13 18:46:00', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 0, '0.0.0.0', '255.255.255.255', 0, NULL),
(42, 49, 'dfd', 10, '', '2018-06-13 18:46:00', '2018-06-13 18:46:00', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 0, '0.0.0.0', '255.255.255.255', 0, NULL),
(43, 1, 'prueba pdf', 0, 'Mi enunciado', '2018-06-14 01:00:00', '2018-06-14 01:00:00', '', 0, '0.0.0.0', '255.255.255.255', 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`) VALUES
(1, 'ADMINISTRADOR'),
(3, 'DOCENTE'),
(2, 'ESTUDIANTE'),
(4, 'EXTERNO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `script`
--

CREATE TABLE `script` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `codigo` mediumtext,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `script`
--

INSERT INTO `script` (`id`, `nombre`, `codigo`, `usuario_id`) VALUES
(45, 'Tab1', '\n          ALGORITMO suma_num\n    VAR\n        ENTERO num, suma:=0, cont:=1\n    INICIO\n        repita\n            escribir \"Deme un número: \"\n            leer num\n            si ( num < 0 ) entonces\n                suma := suma * 2\n                abandona\n            fin si\n            suma := suma + num\n            cont := cont + 1\n        hasta (cont=200)\n        escribirnl \"la suma de los números es: \", suma\n    FIN', 1),
(46, 'prueba matriz', 'ALGORITMO EjemploMatriz\n    VAR\n        ENTERO MatCodigo[3][2]\n        ENTERO i , j\n    INICIO\n        MatCodigo [0][0] := 1\n        MatCodigo [0][1] := 2\n        MatCodigo [1][0] := 3\n        MatCodigo [1][1] := 4\n        MatCodigo [2][0] := 5\n        MatCodigo [2][1] := 6\n        i := 0\n        seleccionar i\n            caso 1\n                //sentencias\n        escribirnl \"ja hola\"\n        escribirnl \"ja hola\"\n        escribirnl \"ja hola\"\n        escribirnl \"ja hola\"\n            caso 2\n                //sentencias\n        escribirnl \"ja hola\"\n        escribirnl \"ja hola\"\n        escribirnl \"ja hola\"\n        escribirnl \"ja hola\"\n            sino\n        i :=1\n        escribirnl i\n        leer i\n                //sentencias\n        fin seleccionar\n        i:=0\n        si (i >0) entonces\n        escribirnl \"ja hola\"\n        escribirnl \"ja hola\"\n        escribirnl \"ja hola\"\n        escribirnl \"ja hola\"\n        sino\n        i :=4\n        escribirnl i\n        leer i\n        fin si\n        para i desde 0 hasta 2 hacer\n            para j desde 0 hasta 2 hacer\n                escribir \"ingrese pos \", i,\" \",j\n                leer MatCodigo[j][i]\n                escribir MatCodigo[j][i], i,\" \",j\n            fin para\n        fin para\n        mientras (5*i<>25) hacer\n            //sentencias\n            i := 5\n            leer i;\n            escribirnl \"numero fue \",i\n        fin mientras\n        escribirnl \"Fin\"\n    FIN', 1),
(47, 'Tab1', 'ALGORITMO Lab_Computacion\nVAR\n	CADENA sala, codigo_com\n	CARACTER estado, resp\n	ENTERO i, cbycr_sala, ci_sala, salas_mmitad_ci:=0, total_com_sala\nCONST\n	ENTERO cant_salas := 2\nINICIO\n	para i desde 1 hasta cant_salas hacer\n		cbycr_sala := 0\n		ci_sala := 0\n		escribir \"Nombre de la sala: \"\n		leer sala\n		repita\n			escribir \"Codigo del computador: \"\n			leer codigo_com\n			repita\n				escribir \"Estado (B-Bueno, R-Regular, I-Malo:) \"\n				leer estado\n				escribir estado\n			hasta (estado=\'B\' O estado=\'R\' O estado = \'I\')\n			si (estado<>\'I\') entonces\n				cbycr_sala := cbycr_sala + 1\n			sino \n				ci_sala := ci_sala + 1\n			fin si		\n\n			escribir \"hay mas computadores? \"\n			leer resp\n			escribir resp\n		hasta (resp=\'N\' O resp=\'n\')	\n\n		escribirnl \"El numero total entre computadores buenos y recuperables es: \", cbycr_sala	\n\n		total_com_sala := ci_sala + cbycr_sala\n		si(ci_sala < total_com_sala/2.0) entonces\n			salas_mmitad_ci := salas_mmitad_ci + 1\n		fin si\n\n		si(cbycr_sala*100.0/total_com_sala<75) entonces\n			escribirnl \"la situacion de la sala \", sala, \" es mejorable\"\n		sino\n			escribirnl \"la situacion de la sala \", sala, \" es critica\"	\n		fin si\n	fin para\n\n	escribirnl \"Hay \", salas_mmitad_ci , \" salas con mas de la mitad de los computadores inservibles\"\nFIN', 1),
(48, 'Tab1', '\n           //Mi primer programa\n           /*Estructura básica de un programa*/\n           /*\n           Esto es un comentario\n           */\n           ALGORITMO miPrograma\n            VAR\n              ENTERO entero1 := 1, x:=0\n              REAL real2 :=1.2, real1\n              CADENA cadena := \"Cadena\\n\", cadena3 :=\"\\\\ ha \\b\"\n              CARACTER caracter\n              ENTERO primero :=4 , segundo :=34, vector[10]\n              ENTERO matriz [3][4*2], vector1[4]\n            CONST\n              LOGICO tercero := VERDADERO, cuarto := FALSO\n            FUNCION ENTERO hla(ENTERO numero)\n              INICIO\n                si (numero < 5) entonces \n                  numero:= numero+1\n                escribirnl \"numero: \", numero\n                  hla(numero)\n                fin si\n                retorno 5\n              FIN FUNCION\n            FUNCION funcion1()\n              INICIO\n                escribir \"mi funcion\"\n              FIN FUNCION\n           INICIO\n            limpiar()\n            escribirnl cadena, hla(-10)\n            escribir redondear(raiz(91), 2),\" \",abs(-181),\" \",\"Ingrese un valor: \"\n            escribir cadinsertar(\"holaadios\", \" nombre \", 4)\n            escribir cadconcatenar(\"hola\", \" mundo\")\n            escribir cadbuscar(\"hola\", \"ho\", 0)\n            escribir \"\\nIngrese valor:\"\n            leer matriz[1][1]\n            vector[1] := 6\n            escribirnl matriz[1][1], \' \', vector[1]\n            escribirnl -12+25+entero1, \' \', \"hola mundo \", !(3<4)\n            para x desde 0 hasta 1+1 hacer\n              escribirnl 12 mod primero\n              primero := primero+1\n            fin para\n            mientras (5 < primero) hacer\n              escribirnl primero\n              primero := primero-1\n            fin mientras\n            primero := 0\n            repita\n                seleccionar primero\n                    caso 1\n                        escribirnl \"1\"\n                    caso 3\n                        escribirnl \"3\"\n                    sino\n                        escribirnl \"sino\"\n                fin seleccionar\n              primero := primero+1\n            hasta (primero=5)\n            si (4<5)entonces\n              escribirnl VERDADERO\n            sino \n              escribirnl \"caso contrario\"\n            fin si\n            escribirnl tercero\n            //leer primero, segundo\n            escribir primero + segundo+10\n            caracter := \'d\'\n            cadena := \"hola mundo\"\n          FIN\n          ', 1),
(49, 'Tab1', '\n           //Mi primer programa\n           /*Estructura básica de un programa*/\n           /*\n           Esto es un comentario\n           */\n           ALGORITMO miPrograma\n            VAR\n              ENTERO entero1 := 1, x:=0\n              REAL real2 :=1.2, real1\n              CADENA cadena := \"Cadena\\n\", cadena3 :=\"\\\\ ha \\b\"\n              CARACTER caracter\n              ENTERO primero :=-300 , segundo :=34, vector[10]\n              ENTERO matriz [3][4*2], vector1[4]\n            CONST\n              LOGICO tercero := VERDADERO, cuarto := FALSO\n            FUNCION ENTERO hla(ENTERO numero)\n              INICIO\n                si (numero < 5) entonces \n                  numero:= numero+1\n                  hla(numero)\n                  leer primero\n                fin si\n                escribirnl \"numero: \", numero\n                retorno primero\n              FIN FUNCION\n            FUNCION funcion1()\n              INICIO\n                escribir \"mi funcion\"\n              FIN FUNCION\n           INICIO\n            limpiar()\n            //escribirnl cadena, hla(4)\n            escribir redondear(raiz(91), 2),\" \",abs(-181),\" \",\"Ingrese un valor: \"\n            escribir cadinsertar(\"holaadios\", \" nombre \", 4)\n            escribir cadconcatenar(\"hola\", \" mundo\")\n            escribir cadbuscar(\"hola\", \"ho\", 0)\n            escribir hla(0)\n            leer matriz[1][1]\n            vector[1] := 6\n            escribirnl matriz[1][1], \' \', vector[1]\n            escribirnl -12+25+entero1, \' \', \"hola mundo \", !(3<4)\n            para x desde 0 hasta 1+1 hacer\n              escribirnl 12 mod primero\n              primero := primero+1\n            fin para\n            mientras (5 < primero) hacer\n              escribirnl primero\n              primero := primero-1\n            fin mientras\n            primero := 0\n            repita\n                seleccionar primero\n                    caso 1\n                        escribirnl \"1\"\n                    caso 3\n                        escribirnl \"3\"\n                    sino\n                        escribirnl \"sino\"\n                fin seleccionar\n              primero := primero+1\n            hasta (primero=5)\n            si (4<5)entonces\n              escribirnl VERDADERO\n            sino \n              escribirnl \"caso contrario\"\n            fin si\n            escribirnl tercero\n            //leer primero, segundo\n            escribir primero + segundo\n            caracter := \'d\'\n            cadena := \"hola mundo\"\n          FIN\n          ', 1),
(51, 'programa', 'ALGORITMO miPrograma\n    INICIO\n        escribirnl \"Hola mundo\"\n    FIN', 8),
(52, 'ff', 'ALGORITMO miPrograma\n    INICIO\n        escribirnl \"Hola mundo\"\n    FIN', 8),
(84, 'programa', 'ALGORITMO miPrograma\n    INICIO\n        escribirnl \"Hola mundo\"\n    FIN', 17),
(86, 'nuevo', 'ALGORITMO miPrograma\n    INICIO\n        escribirnl \"Hola mundo\"\n    FIN', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semestre`
--

CREATE TABLE `semestre` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `semestre`
--

INSERT INTO `semestre` (`id`, `nombre`) VALUES
(1, '2018-1'),
(2, '2018-2'),
(6, '2018-3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `alias` varchar(45) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `cedula` varchar(45) NOT NULL,
  `descripcion` varchar(125) DEFAULT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` char(40) NOT NULL,
  `semestre_id` int(11) DEFAULT NULL,
  `carrera_id` int(11) DEFAULT NULL,
  `token_reseteo_clave` char(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `rol_id`, `alias`, `nombre`, `cedula`, `descripcion`, `correo`, `clave`, `semestre_id`, `carrera_id`, `token_reseteo_clave`) VALUES
(1, 1, 'arturo.bernal', 'Arturo', '19925695', 'mi usuario', 'arturo.bernal@unet.edu.ve', 'b1517460ec7535761b72f908615026dd144726eb', 1, 1, NULL),
(3, 3, 'lochoa', 'Luis Ochoa', '9482485', 'Profesor', 'lochoa@unet.edu.ve', 'aac4629b546e55d0f7ebc550b9f3b499fad88c5a', 1, 1, NULL),
(7, 2, 'estudiante1', 'estudiante', '2', NULL, 'estudiante@gmail.com', 'b1517460ec7535761b72f908615026dd144726eb', 1, 1, NULL),
(8, 2, 'estudiante', NULL, '3', NULL, 'estudiante1@gmail.com', 'b1517460ec7535761b72f908615026dd144726eb', 1, 1, NULL),
(9, 2, 'arturor.bernalh', 'Arturo', '19925696', '', 'arturor.bernalh@gmail.com', '9253998f1851a78164f158fc43e50d6f1747c1b9', 1, 1, NULL),
(10, 2, 'usuio', 'usuario', '123', '', 'usuio@gmail.com', '8927bd748f26a7258a01e318a7e1e7585458a228', 1, 2, NULL),
(11, 3, 'prueba', 'prueba', 'prueba', NULL, 'prueba@gmail.com', 'b1517460ec7535761b72f908615026dd144726eb', 1, 1, NULL),
(12, 1, 'admin', 'admin', 'admin', NULL, 'admin@admin.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, 1, NULL),
(15, 2, 'admidn', 'Arturo', '19925697', '', 'admidn@adminadmin.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 6, 5, NULL),
(16, 2, 'arturobernal', 'Arturo', '19925694', '', 'arturobernal@gmail1.com', 'b1517460ec7535761b72f908615026dd144726eb', 6, 4, NULL),
(17, 2, '19925699', '19925699', '19925699', NULL, '19925699@gmail.com', '41a0434b9d7f50948d0c4481ef5accdd2ad8e5de', 6, 3, NULL),
(18, 1, 'leydy.moreno', 'Leydy', '24743325', NULL, 'leydy.moreno@unet.edu.ve', '593fd2cb0854e8e96fd34c7947755eb75f3b41f2', 2, 1, NULL),
(19, 2, 'lskfjds', 'dlskfjsdl', '5849343897', '', 'lskfjds@gmail.com', '8927bd748f26a7258a01e318a7e1e7585458a228', 2, 3, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_aula`
--

CREATE TABLE `usuario_aula` (
  `id` int(11) NOT NULL,
  `aula_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  `fecha_borrado` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario_aula`
--

INSERT INTO `usuario_aula` (`id`, `aula_id`, `usuario_id`, `borrado`, `fecha_borrado`) VALUES
(1, 1, 7, 0, NULL),
(5, 7, 10, 0, NULL),
(17, 17, 8, 1, '2018-06-06 14:27:57'),
(20, 20, 1, 1, '2018-06-04 22:42:15'),
(21, 1, 15, 1, '2018-06-06 01:08:11'),
(25, 1, 3, 1, '2018-06-06 01:08:07'),
(26, 1, 8, 0, NULL),
(27, 1, 16, 1, '2018-06-08 01:54:53'),
(28, 7, 7, 0, NULL),
(29, 1, 17, 0, NULL),
(30, 7, 17, 0, NULL),
(31, 1, 1, 1, '2018-06-08 02:23:14'),
(39, 44, 19, 0, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo_UNIQUE` (`codigo`),
  ADD KEY `fk_aula_usuario1_idx` (`usuario_id`),
  ADD KEY `fk_aula_semestre1_idx` (`semestre_id`);

--
-- Indices de la tabla `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

--
-- Indices de la tabla `entrega`
--
ALTER TABLE `entrega`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_aula_proyecto_proyecto1_idx` (`proyecto_id`),
  ADD KEY `fk_usuario_aula_proyecto_usuario_aula1_idx` (`usuario_aula_id`);

--
-- Indices de la tabla `parametro`
--
ALTER TABLE `parametro`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_proyecto_aula1_idx` (`aula_id`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

--
-- Indices de la tabla `script`
--
ALTER TABLE `script`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_script_usuario1_idx` (`usuario_id`);

--
-- Indices de la tabla `semestre`
--
ALTER TABLE `semestre`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cedula_UNIQUE` (`cedula`),
  ADD UNIQUE KEY `correo_UNIQUE` (`correo`),
  ADD KEY `fk_usuario_rol1_idx` (`rol_id`),
  ADD KEY `fk_usuario_semestre1_idx` (`semestre_id`),
  ADD KEY `fk_usuario_carrera1_idx` (`carrera_id`);

--
-- Indices de la tabla `usuario_aula`
--
ALTER TABLE `usuario_aula`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_aula_aula1_idx` (`aula_id`),
  ADD KEY `fk_usuario_aula_usuario1_idx` (`usuario_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aula`
--
ALTER TABLE `aula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `carrera`
--
ALTER TABLE `carrera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `entrega`
--
ALTER TABLE `entrega`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `parametro`
--
ALTER TABLE `parametro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `script`
--
ALTER TABLE `script`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT de la tabla `semestre`
--
ALTER TABLE `semestre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `usuario_aula`
--
ALTER TABLE `usuario_aula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aula`
--
ALTER TABLE `aula`
  ADD CONSTRAINT `fk_aula_semestre1` FOREIGN KEY (`semestre_id`) REFERENCES `semestre` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_aula_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `entrega`
--
ALTER TABLE `entrega`
  ADD CONSTRAINT `fk_usuario_aula_proyecto_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_usuario_aula_proyecto_usuario_aula1` FOREIGN KEY (`usuario_aula_id`) REFERENCES `usuario_aula` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `fk_proyecto_aula1` FOREIGN KEY (`aula_id`) REFERENCES `aula` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `script`
--
ALTER TABLE `script`
  ADD CONSTRAINT `fk_script_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_carrera1` FOREIGN KEY (`carrera_id`) REFERENCES `carrera` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_rol1` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_semestre1` FOREIGN KEY (`semestre_id`) REFERENCES `semestre` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario_aula`
--
ALTER TABLE `usuario_aula`
  ADD CONSTRAINT `fk_usuario_aula_aula1` FOREIGN KEY (`aula_id`) REFERENCES `aula` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_usuario_aula_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
